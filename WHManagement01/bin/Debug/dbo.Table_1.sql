﻿CREATE TABLE [dbo].[USER]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [UserName] NVARCHAR(100) NOT NULL, 
    [Password] NVARCHAR(100) NOT NULL, 
    [CreateData] DATETIME NOT NULL, 
    [UpdateData] DATETIME NOT NULL, 
    [Notes] NVARCHAR(2000) NULL, 
    [SyncStatus] NCHAR(10) NOT NULL, 
    [IdBranch] INT NOT NULL, 
    [IdClient] INT NOT NULL
)
