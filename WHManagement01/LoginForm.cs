﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
//using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Collections.Specialized;

namespace WHManagement01
{
    public partial class LoginForm : Form
    {
        CultureInfo culture;
        ResourceManager LocRM;
        public LoginForm()
        {
            InitializeComponent();
            //Utils.initDefaultUrl();
            updateLanguage(Utils.getLanguage());
            isLogin = false;       //for close form handle
            //loadBranchList();
            loadLastLoginSuccess();
        }

        private bool isLogin = false;
        private async void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            string whId = txtWHIdLogin.Text;
            string clientId = txtClientId.Text;

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(whId) && !string.IsNullOrEmpty(clientId))
            {

                int result = await Utils.callApiPostLogin(username, password, whId, clientId);
                if (result == 1)
                {
                    isLogin = true;
                    MainForm mainForm = new MainForm();
                    this.Close();
                    mainForm.Show();
                }
                else if (result == -1)
                {
                    MessageBox.Show(LocRM.GetString("errorNoLoginLink", culture), LocRM.GetString("errorLogin", culture), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (result == 2)
                {
                    MessageBox.Show(LocRM.GetString("errorMessageLogin", culture), LocRM.GetString("errorLogin", culture), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            //isLogin = true;
            //MainForm mainForm = new MainForm();
            //this.Close();
            //mainForm.Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isLogin)
            {
                System.Windows.Forms.Application.Exit();
            }
        }

        private void loadLastLoginSuccess()
        {
            string warehouseId = "";
            string clientId = "";
            string username = "";
            Utils.getLastLoginSuccess(out warehouseId, out clientId, out username);
            if (!string.IsNullOrEmpty(username))
            {
                txtUsername.Text = username;
            }
            else
            {
                this.ActiveControl = txtUsername;
            }
            if (!string.IsNullOrEmpty(clientId))
            {
                txtClientId.Text = clientId;
            }
            else
            {
                this.ActiveControl = txtClientId;
            }
            if (!string.IsNullOrEmpty(warehouseId))
            {
                txtWHIdLogin.Text = warehouseId;
                this.ActiveControl = txtPassword;
            }
            else
            {
                this.ActiveControl = txtWHIdLogin;
            }
        }

        public void updateLanguage(String language)
        {
            culture = CultureInfo.CreateSpecificCulture(language);
            LocRM = new ResourceManager("WHManagement01.Language", typeof(LoginForm).Assembly);

            lbLoginForm.Text = LocRM.GetString("titleLoginForm", culture);
            lbWHLogin.Text = LocRM.GetString("whId", culture);
            lbClientId.Text = LocRM.GetString("clientId", culture);
            lbUsername.Text = LocRM.GetString("username", culture);
            lbPassword.Text = LocRM.GetString("password", culture);
            btnLogin.Text = LocRM.GetString("login", culture);
            btnCancel.Text = LocRM.GetString("cancel", culture);
        }
    }
}
