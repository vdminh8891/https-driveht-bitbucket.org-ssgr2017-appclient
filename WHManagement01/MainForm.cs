﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace WHManagement01
{
    public partial class MainForm : Form
    {
        #region VARIABLE REGION
        CultureInfo culture;
        ResourceManager LocRM;

        private string currentPaletExportId;
        private string currentExportId;
        private string currentImportId;
        private string currentUserId;
        private string currentWHId;
        private string currentTruckId;
        private string currentPaletId;
        private string currentDetailImportId;
        private string currentDetailExportId;

        private int actionStatus = 0;
        private const int ADDNEW_STATUS = 0;
        private const int UPDATE_STATUS = 1;
        private const int DELETE_STATUS = 2;

        private const string HEAD_OFFICE = "Trụ sở chính";
        private const string BRANCH = "Chi nhánh";
        private const string PRODUCE = "Sản xuất";
        private const string BUY = "Mua";

        private List<string> listArea = new List<string> { "Miền Nam", "Miền Trung", "Miền Bắc" };                              //Khu vực
        Dictionary<string, string> listRole = new Dictionary<string, string> { { BRANCH, "0" }, { HEAD_OFFICE, "1" } };         //head office or branch
        Dictionary<string, string> listTypeProduct = new Dictionary<string, string> { { PRODUCE, "0" }, { BUY, "1" } };         //PRODUCE OR BUY

        static XmlReader xmlFile;
        static XmlReader xmlFile2;
        DataTable tempDataTableDetailImportAndPalet;
        DataTable tempDataTableDetailExportAndPalet;
        #endregion

        #region CONSTRUCTOR REGION
        public MainForm()
        {
            InitializeComponent();
            updateLanguage(Utils.getLanguage());
            tabMain.TabPages.Remove(tabMain.TabPages["tabBranchManage"]);
            focusDefaulTab();
        }
        #endregion

        #region FORM EVENT REGION
        private void dataGridViewPaletExportManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 0){
                currentPaletExportId = dataGridViewPaletExportManage.CurrentRow.Cells[1].Value.ToString();
                txtSerialPaletExportManage.Text = dataGridViewPaletExportManage.CurrentRow.Cells[3].Value.ToString();

                string whId = dataGridViewPaletExportManage.CurrentRow.Cells[2].Value.ToString();
                cbCurentWhPaletExport.SelectedIndex = cbCurentWhPaletExport.FindString(Utils.findWhNameByWhId(whId));
                DateTime parsedDate;
                parsedDate = new DateTime();
                if (DateTime.TryParseExact(dataGridViewPaletExportManage.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
                {
                    dtpBeginDatePaletExport.Value = parsedDate;
                }
                parsedDate = new DateTime();
                if (DateTime.TryParseExact(dataGridViewPaletExportManage.CurrentRow.Cells[5].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
                {
                    dtpEndDatePaletExport.Value = parsedDate;
                }
                txtStatusPaletExport.Text = dataGridViewPaletExportManage.CurrentRow.Cells[6].Value.ToString();

                //actionStatus = UPDATE_STATUS;
            }
        }
        private void subTabImportManage_Selected(object sender, TabControlEventArgs e)
        {

        }
        private async void tabMain_Selected(object sender, TabControlEventArgs e)
        {
            if (tabMain.SelectedTab == tabMain.TabPages["tabWHManage"])
            {
                btnDelete.Enabled = true;
                btnNew.Enabled = true;
                btnSave.Enabled = true;
                loadComboBoxArea(ref cbAreaWHManage);
                loadComboBoxIsHO(ref cbIsHOWhManage);

                int result = await Utils.callApiGetWarehouse();
                switch (result)
                {
                    case 1: //success case
                        LoadTabWarehouseManage();
                        break;
                    case -1: //null url
                        break;
                    case -2: // loi reponse null
                        break;
                    case -3: //result not a number
                        break;
                }
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbTruckManage"])
            {
                btnDelete.Enabled = true;
                btnNew.Enabled = true;
                btnSave.Enabled = true;
                loadComboBoxWH(ref cbWhTruckManage);

                int result = await Utils.callApiGetTruck();
                switch (result)
                {
                    case 1: //success case
                        LoadTabTruckManage();
                        break;
                    case -1: //null url
                        break;
                    case -2: // loi reponse null
                        break;
                    case -3: //result not a number
                        break;
                }
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabPaletManage"])
            {
                btnDelete.Enabled = false;
                btnNew.Enabled = false;
                btnSave.Enabled = true;
                currentPaletId = "";
                loadComboBoxWH(ref cbWhExImportManage);             //Kho nguồn
                loadComboBoxWH(ref cbWhCurrentImportManage);        //Kho hiện tại import
                loadComboBoxWH(ref cbWhDetailTabImportManage);      //Kho hiện tại import - tab khác

                int result = await Utils.callApiGetPalet();
                switch (result)
                {
                    case 1: //success case
                        LoadTabPaletManage();
                        break;
                    case -1: //null url
                        break;
                    case -2: // loi reponse null
                        break;
                    case -3: //result not a number
                        break;
                }
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabImportManagement"])
            {
                btnDelete.Enabled = false;
                btnNew.Enabled = false;
                btnSave.Enabled = false;
                loadComboBoxWH(ref cbWhExImportManage);             //Kho nguồn
                loadComboBoxWH(ref cbWhCurrentImportManage);        //Kho hiện tại import
                loadComboBoxWH(ref cbWhDetailTabImportManage);      //Kho hiện tại import - tab khác
                loadComboBoxProduct(ref cbProduceImportManage);

                int result = await Utils.callApiGetImport();
                switch (result)
                {
                    case 1: //success case
                        LoadTabImportManage();
                        break;
                    case -1: //null url
                        break;
                    case -2: // loi reponse null
                        break;
                    case -3: //result not a number
                        break;
                }
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabExportManagement"])
            {
                btnDelete.Enabled = false;
                btnNew.Enabled = false;
                btnSave.Enabled = false;
                loadComboBoxWH(ref cbCurrentWhExportManage);             //Kho nguồn
                loadComboBoxWH(ref cbImWhExportManage);        //Kho hiện tại export
                loadComboBoxWH(ref cbCurentWhPaletExport);      //Kho hiện tại export - tab khác
                loadComboBoxProduct(ref cbTypeProductExportManage);

                int result = await Utils.callApiGetExport();
                switch (result)
                {
                    case 1: //success case
                        LoadTabExportManage();
                        LoadGridViewPaletInExport();
                        break;
                    case -1: //null url
                        break;
                    case -2: // loi reponse null
                        break;
                    case -3: //result not a number
                        break;
                }
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbUserManage"])
            {
                btnDelete.Enabled = true;
                btnNew.Enabled = true;
                btnSave.Enabled = true;
                loadComboBoxWH(ref cbWhUserManage);

                int result = await Utils.callApiGetUser();
                switch (result)
                {
                    case 1: //success case
                        LoadTabUserManage();
                        break;
                    case -1: //null url
                        MessageBox.Show(LocRM.GetString("errorNoGetUserLink", culture), LocRM.GetString("callApiError", culture), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case -2: // loi reponse null

                        break;
                    case -3: //result not a number

                        break;
                }
            }
        }
        private void dataGridViewDetailImportAndPalet_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            currentDetailImportId = dataGridViewDetailImportAndPalet.CurrentRow.Cells[0].Value.ToString();
            txtSerialPaletImportManage.Text = dataGridViewDetailImportAndPalet.CurrentRow.Cells[2].Value.ToString();
            txtLincensePlateImportManage.Text = dataGridViewDetailImportAndPalet.CurrentRow.Cells[3].Value.ToString();

            DateTime parsedDate;
            if (DateTime.TryParseExact(dataGridViewDetailImportAndPalet.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpCreateDateSubTabImportManage.Value = parsedDate;
            }
            txtNoteDetailImportImportManage.Text = dataGridViewDetailImportAndPalet.CurrentRow.Cells[5].Value.ToString();
            currentPaletId = dataGridViewDetailImportAndPalet.CurrentRow.Cells[6].Value.ToString();
            string whId = dataGridViewDetailImportAndPalet.CurrentRow.Cells[7].Value.ToString();
            cbWhDetailTabImportManage.SelectedIndex = cbWhDetailTabImportManage.FindString(Utils.findWhNameByWhId(whId));
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewDetailImportAndPalet.CurrentRow.Cells[9].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpBeginDatePaletImportManage.Value = parsedDate;
            }
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewDetailImportAndPalet.CurrentRow.Cells[10].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpEndDatePaletImportManage.Value = parsedDate;
            }

            txtStatusPlateImportManage.Text = dataGridViewDetailImportAndPalet.CurrentRow.Cells[11].Value.ToString();

            actionStatus = UPDATE_STATUS;
        }
        private void dataGridViewDetailExportAndPalet_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            currentDetailExportId = dataGridViewDetailExportAndPalet.CurrentRow.Cells[0].Value.ToString();
            txtSerialPaletExportManage.Text = dataGridViewDetailExportAndPalet.CurrentRow.Cells[2].Value.ToString();
            txtLicensePlateExportManage.Text = dataGridViewDetailExportAndPalet.CurrentRow.Cells[3].Value.ToString();

            DateTime parsedDate;
            if (DateTime.TryParseExact(dataGridViewDetailExportAndPalet.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpCreateDateSubTabExport.Value = parsedDate;
            }
            txtNoteSubTabExport.Text = dataGridViewDetailExportAndPalet.CurrentRow.Cells[5].Value.ToString();
            currentPaletId = dataGridViewDetailExportAndPalet.CurrentRow.Cells[6].Value.ToString();
            string whId = dataGridViewDetailExportAndPalet.CurrentRow.Cells[7].Value.ToString();
            cbCurentWhPaletExport.SelectedIndex = cbCurentWhPaletExport.FindString(Utils.findWhNameByWhId(whId));
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewDetailExportAndPalet.CurrentRow.Cells[9].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpBeginDatePaletExport.Value = parsedDate;
            }
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewDetailExportAndPalet.CurrentRow.Cells[10].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpEndDatePaletExport.Value = parsedDate;
            }

            txtStatusPaletExport.Text = dataGridViewDetailExportAndPalet.CurrentRow.Cells[11].Value.ToString();

            actionStatus = UPDATE_STATUS;
        }
        private void btnAddNewDetailImportPalet_Click(object sender, EventArgs e)
        {
            addNewDetailImportAndPalet();
        }

        private void btnUpdateDetailImportPalet_Click(object sender, EventArgs e)
        {
            updateDetailImportAndPalet();
        }
        private void btnAddNewDetailExport_Click(object sender, EventArgs e)
        {
            addNewDetailExportAndPalet();
        }

        private void btnUpdateDetailExport_Click(object sender, EventArgs e)
        {
            updateDetailExportAndPalet();
        }

        private void btnAddNewImport_Click(object sender, EventArgs e)
        {
            addNewImport();
        }

        private void btnUpdateImport_Click(object sender, EventArgs e)
        {
            updateImport();
        }
        private void btnAddNewExport_Click(object sender, EventArgs e)
        {
            addNewExport();
        }

        private void btnUpdateExport_Click(object sender, EventArgs e)
        {
            updateExport();
        }
        private void btnNew_Click(object sender, EventArgs e)
        {
            //if (tabMain.SelectedTab == tabMain.TabPages["tabBranchManage"])
            //{
            //    addNewBranch();
            //}
            if (tabMain.SelectedTab == tabMain.TabPages["tabWHManage"])
            {
                addNewWarehouse();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbTruckManage"])
            {
                addNewTruck();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabPaletManage"])
            {

            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabImportManagement"])
            {
                //ko xu ly
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabExportManagement"])
            {

            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbUserManage"])
            {
                addNewUser();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (tabMain.SelectedTab == tabMain.TabPages["tabBranchManage"])
            //{
            //    updateBranch();
            //}
            if (tabMain.SelectedTab == tabMain.TabPages["tabWHManage"])
            {
                updateWarehouse();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbTruckManage"])
            {
                updateTruck();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabPaletManage"])
            {
                updatePalet();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabImportManagement"])
            {
                //ko xu ly
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabExportManagement"])
            {

            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbUserManage"])
            {
                updateUser();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //if (tabMain.SelectedTab == tabMain.TabPages["tabBranchManage"])
            //{
            //    deleteBranch();
            //}
            if (tabMain.SelectedTab == tabMain.TabPages["tabWHManage"])
            {
                deleteWarehouse();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbTruckManage"])
            {
                deleteTruck();
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabPaletManage"])
            {

            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabImportManagement"])
            {
                //ko xu ly
            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tabExportManagement"])
            {

            }
            else if (tabMain.SelectedTab == tabMain.TabPages["tbUserManage"])
            {
                deleteUser();
            }
        }
        private void dataGridViewListExport_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DateTime parsedDate;
            currentExportId = dataGridViewListExport.CurrentRow.Cells[0].Value.ToString();
            txtNameExportExportManage.Text = dataGridViewListExport.CurrentRow.Cells[3].Value.ToString();
            string whId = dataGridViewListExport.CurrentRow.Cells[1].Value.ToString();
            cbCurrentWhExportManage.SelectedIndex = cbCurrentWhExportManage.FindString(Utils.findWhNameByWhId(whId));
            whId = dataGridViewListExport.CurrentRow.Cells[2].Value.ToString();
            cbImWhExportManage.SelectedIndex = cbImWhExportManage.FindString(Utils.findWhNameByWhId(whId));
            if (DateTime.TryParseExact(dataGridViewListExport.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpCreateDateExportManage.Value = parsedDate;
            }
            txtSerialExportManage.Text = dataGridViewListExport.CurrentRow.Cells[5].Value.ToString();
            //cbProduceImportManage.SelectedIndex = cbProduceImportManage.FindString(dataGridViewImportImportManage.CurrentRow.Cells[6].Value.ToString());
            if (dataGridViewListExport.CurrentRow.Cells[6].Value.ToString().Equals("0"))
            {
                cbTypeProductExportManage.SelectedIndex = cbTypeProductExportManage.FindString(PRODUCE);
            }
            else
            {
                cbTypeProductExportManage.SelectedIndex = cbTypeProductExportManage.FindString(BUY);
            }
            txtQuantityPaletExportManage.Text = dataGridViewListExport.CurrentRow.Cells[7].Value.ToString();
            txtNoteExportManage.Text = dataGridViewListExport.CurrentRow.Cells[8].Value.ToString();
            txtCreateUserExportManage.Text = dataGridViewListExport.CurrentRow.Cells[10].Value.ToString();
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewListExport.CurrentRow.Cells[11].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpUpdateDateExportManage.Value = parsedDate;
            }
            //txtUsernameImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[12].Value.ToString();
            txtNameBelongExport.Text = dataGridViewListExport.CurrentRow.Cells[3].Value.ToString();
            actionStatus = UPDATE_STATUS;
            tempDataTableDetailExportAndPalet = new DataTable();
            tempDataTableDetailExportAndPalet = Utils.getListDetailExportLocalByExportId(currentExportId);
            if (tempDataTableDetailExportAndPalet == null)
            {
                return;
            }
            loadDataGridViewDetailExportAndPalet(tempDataTableDetailExportAndPalet);
        }

        private void dataGridViewImportImportManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DateTime parsedDate;
            currentImportId = dataGridViewImportImportManage.CurrentRow.Cells[0].Value.ToString();
            txtNameImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[3].Value.ToString();
            string whId = dataGridViewImportImportManage.CurrentRow.Cells[1].Value.ToString();
            cbWhCurrentImportManage.SelectedIndex = cbWhCurrentImportManage.FindString(Utils.findWhNameByWhId(whId));
            whId = dataGridViewImportImportManage.CurrentRow.Cells[2].Value.ToString();
            cbWhExImportManage.SelectedIndex = cbWhExImportManage.FindString(Utils.findWhNameByWhId(whId));
            if (DateTime.TryParseExact(dataGridViewImportImportManage.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpCreateDateImportManage.Value = parsedDate;
            }
            txtSerialImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[5].Value.ToString();
            //cbProduceImportManage.SelectedIndex = cbProduceImportManage.FindString(dataGridViewImportImportManage.CurrentRow.Cells[6].Value.ToString());
            if (dataGridViewImportImportManage.CurrentRow.Cells[6].Value.ToString().Equals("0"))
            {
                cbProduceImportManage.SelectedIndex = cbProduceImportManage.FindString(PRODUCE);
            }
            else
            {
                cbProduceImportManage.SelectedIndex = cbProduceImportManage.FindString(BUY);
            }
            txtQuantityImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[7].Value.ToString();
            txtNoteImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[8].Value.ToString();
            txtUsernameImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[10].Value.ToString();
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewImportImportManage.CurrentRow.Cells[11].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpUpdateDateImportManage.Value = parsedDate;
            }
            //txtUsernameImportManage.Text = dataGridViewImportImportManage.CurrentRow.Cells[12].Value.ToString();
            txtNameBelongImport.Text = dataGridViewImportImportManage.CurrentRow.Cells[3].Value.ToString();
            actionStatus = UPDATE_STATUS;
            tempDataTableDetailImportAndPalet = new DataTable();
            tempDataTableDetailImportAndPalet = Utils.getListDetailImportLocalByImportId(currentImportId);
            if (tempDataTableDetailImportAndPalet == null)
            {
                return;
            }
            loadDataGridViewDetailImportAndPalet(tempDataTableDetailImportAndPalet);

            //cbWhCurrentImportManage.SelectedIndex = cbWhCurrentImportManage.FindString(Utils.findWhNameByWhId(Utils.CURRENT_ID_WAREHOUSE));
        }

        private void dataGridViewTruckManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DateTime parsedDate;
            currentTruckId = dataGridViewTruckManage.CurrentRow.Cells[0].Value.ToString();
            txtLicensePlateTruckManages.Text = dataGridViewTruckManage.CurrentRow.Cells[1].Value.ToString();
            string whId = dataGridViewTruckManage.CurrentRow.Cells[2].Value.ToString();
            cbWhTruckManage.SelectedIndex = cbWhTruckManage.FindString(Utils.findWhNameByWhId(whId));
            if (DateTime.TryParseExact(dataGridViewTruckManage.CurrentRow.Cells[3].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpBeginDateTruckManage.Value = parsedDate;
            }
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewTruckManage.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpEndDateTruckManage.Value = parsedDate;
            }

            txtStatusTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[5].Value.ToString();
            txtOwnerTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[6].Value.ToString();
            txtAddressTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[7].Value.ToString();
            txtPhoneTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[8].Value.ToString();
            txtIdCardTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[9].Value.ToString();
            txtNoteTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[10].Value.ToString();
            txtDriverTruckManage.Text = dataGridViewTruckManage.CurrentRow.Cells[12].Value.ToString();

            actionStatus = UPDATE_STATUS;
        }

        private void dataGridViewUserManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DateTime parsedDate;
            currentUserId = dataGridViewUserManage.CurrentRow.Cells[0].Value.ToString();
            txtUsernameUserManage.Text = dataGridViewUserManage.CurrentRow.Cells[1].Value.ToString();
            txtPasswordUserManage.Text = dataGridViewUserManage.CurrentRow.Cells[2].Value.ToString();
            if (DateTime.TryParseExact(dataGridViewUserManage.CurrentRow.Cells[3].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpCreateDate.Value = parsedDate;
            }
            parsedDate = new DateTime();
            if (DateTime.TryParseExact(dataGridViewUserManage.CurrentRow.Cells[4].Value.ToString(), "MM/dd/yyyy HH:mm:ss", null, DateTimeStyles.None, out parsedDate))
            {
                dtpUpdateDate.Value = parsedDate;
            }
            txtNoteUserManage.Text = dataGridViewUserManage.CurrentRow.Cells[5].Value.ToString();
            string whId = dataGridViewUserManage.CurrentRow.Cells[7].Value.ToString();
            cbWhUserManage.SelectedIndex = cbWhUserManage.FindString(Utils.findWhNameByWhId(whId));
            txtClientUserManage.Text = dataGridViewUserManage.CurrentRow.Cells[8].Value.ToString();


            txtUsernameUserManage.ReadOnly = true;
            dtpCreateDate.Enabled = false;
            dtpUpdateDate.Enabled = false;
            actionStatus = UPDATE_STATUS;
        }

        private void dataGridViewWHManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            currentWHId = dataGridViewWHManage.CurrentRow.Cells[0].Value.ToString();

            txtWhIdWhManage.Text = dataGridViewWHManage.CurrentRow.Cells[0].Value.ToString();
            txtNameWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[1].Value.ToString();
            txtAddressWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[2].Value.ToString();
            txtPhoneWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[3].Value.ToString();
            txtNoteWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[4].Value.ToString();
            txtPalletWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[6].Value.ToString();
            cbAreaWHManage.SelectedIndex = cbWhUserManage.FindString(dataGridViewWHManage.CurrentRow.Cells[7].Value.ToString());
            txtLinkWSWHManage.Text = dataGridViewWHManage.CurrentRow.Cells[8].Value.ToString();
            if (dataGridViewWHManage.CurrentRow.Cells[9].Value.ToString().Equals("1"))
            {
                cbIsHOWhManage.SelectedIndex = cbIsHOWhManage.FindString(HEAD_OFFICE);
            }
            else
            {
                cbIsHOWhManage.SelectedIndex = cbIsHOWhManage.FindString(BRANCH);
            }
            txtWhIdWhManage.ReadOnly = true;
            actionStatus = UPDATE_STATUS;

        }

        private void dataGridViewBranchManage_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //currentBranchId = dataGridViewBranchManage.CurrentRow.Cells[0].Value.ToString();

            //txtBranchNameBranchManage.Text = dataGridViewBranchManage.CurrentRow.Cells[1].Value.ToString();
            //txtAddressBranchManage.Text = dataGridViewBranchManage.CurrentRow.Cells[2].Value.ToString();
            //txtAreaBranchManage.Text = dataGridViewBranchManage.CurrentRow.Cells[3].Value.ToString();
            //txtPhoneBranchManage.Text = dataGridViewBranchManage.CurrentRow.Cells[4].Value.ToString();
            //txtNoteBranchManage.Text = dataGridViewBranchManage.CurrentRow.Cells[5].Value.ToString();

            //actionStatus = UPDATE_STATUS;
        }

        private void txtPalletWHManage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region IMPORT REGION
        private void loadDataGridViewDetailImportAndPalet(DataTable dt)
        {
            dataGridViewDetailImportAndPalet.DataSource = dt;
            dataGridViewDetailImportAndPalet.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewDetailImportAndPalet.Columns[1].HeaderText = LocRM.GetString("importCode", culture);
            dataGridViewDetailImportAndPalet.Columns[2].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewDetailImportAndPalet.Columns[3].HeaderText = LocRM.GetString("numberPlate", culture);
            dataGridViewDetailImportAndPalet.Columns[4].HeaderText = LocRM.GetString("createDate", culture);
            dataGridViewDetailImportAndPalet.Columns[5].HeaderText = LocRM.GetString("note", culture);
            dataGridViewDetailImportAndPalet.Columns[6].HeaderText = LocRM.GetString("id", culture);
            dataGridViewDetailImportAndPalet.Columns[7].HeaderText = LocRM.GetString("whId", culture);
            dataGridViewDetailImportAndPalet.Columns[8].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewDetailImportAndPalet.Columns[9].HeaderText = LocRM.GetString("beginDate", culture);
            dataGridViewDetailImportAndPalet.Columns[10].HeaderText = LocRM.GetString("endDate", culture);
            dataGridViewDetailImportAndPalet.Columns[11].HeaderText = LocRM.GetString("status", culture);

            dataGridViewDetailImportAndPalet.Columns[0].Visible = false;
            dataGridViewDetailImportAndPalet.Columns[6].Visible = false;
            dataGridViewDetailImportAndPalet.Columns[8].Visible = false;
            foreach (DataGridViewRow row in dataGridViewDetailImportAndPalet.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }

        private async void updateDetailImportAndPalet()
        {
            string serialPalet = txtSerialPaletImportManage.Text;
            string licensePlateDetailImport = txtLincensePlateImportManage.Text;
            string createDateDetailImport = dtpCreateDateSubTabImportManage.Value.Date.ToString();
            string noteDetailImport = txtNoteDetailImportImportManage.Text;
            //currentImportId

            string whId = Utils.CURRENT_ID_WAREHOUSE;
            string beginDate = dtpBeginDatePaletImportManage.Value.Date.ToString();
            string endDate = dtpEndDatePaletImportManage.Value.Date.ToString();
            string status = txtStatusPlateImportManage.Text;

            if (string.IsNullOrEmpty(currentImportId))
            {
                MessageBox.Show(LocRM.GetString("plsChoseImport", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!string.IsNullOrEmpty(currentDetailImportId) && !string.IsNullOrEmpty(currentPaletId) && actionStatus == UPDATE_STATUS)
            {
                if (serialPalet == "" || licensePlateDetailImport == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serialPalet == "") message += "\n" + "  " + "- " + lbSerialDetailImportImportManage.Text;
                    if (licensePlateDetailImport == "") message += "\n" + "  " + "- " + lbPlateDetailImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateDetailImport", culture) + ": " + "[" + serialPalet + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateDetailImport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result1 = await Utils.callApiPostUpdateDetailImport(currentDetailImportId, currentImportId, serialPalet, licensePlateDetailImport, createDateDetailImport, noteDetailImport);
                        int result2 = await Utils.callApiPostUpdatePalet(currentPaletId, whId, serialPalet, beginDate, endDate, status);
                        if (result1 == 1 && result2 == 1)
                        {
                            if (tempDataTableDetailImportAndPalet != null)
                            {
                                foreach (DataRow dr in tempDataTableDetailImportAndPalet.Rows)
                                {
                                    if (dr["IdPalet"].ToString().Equals(currentPaletId) && dr["IdDetailImport"].ToString().Equals(currentDetailImportId))
                                    {
                                        dr["IdImport"] = currentImportId;
                                        dr["SerialPalet"] = serialPalet;
                                        dr["LicensePlate"] = licensePlateDetailImport;
                                        dr["Date"] = createDateDetailImport;
                                        dr["Notes"] = noteDetailImport;
                                        dr["IdWareHouse"] = whId;
                                        dr["Serial"] = serialPalet;
                                        dr["BeginDate"] = beginDate;
                                        dr["EndDate"] = endDate;
                                        dr["Status"] = status;
                                    }
                                }
                                loadDataGridViewDetailImportAndPalet(tempDataTableDetailImportAndPalet);
                            }
                        }

                    }
                }
            }

        }
        private async void addNewDetailImportAndPalet()
        {
            string serialPalet = txtSerialPaletImportManage.Text;
            string licensePlateDetailImport = txtLincensePlateImportManage.Text;
            string createDateDetailImport = dtpCreateDateSubTabImportManage.Value.Date.ToString();
            string noteDetailImport = txtNoteDetailImportImportManage.Text;
            //currentImportId

            string whId = Utils.CURRENT_ID_WAREHOUSE;
            string beginDate = dtpBeginDatePaletImportManage.Value.Date.ToString();
            string endDate = dtpEndDatePaletImportManage.Value.Date.ToString();
            string status = txtStatusPlateImportManage.Text;

            if (string.IsNullOrEmpty(currentImportId))
            {
                MessageBox.Show(LocRM.GetString("plsChoseImport", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (actionStatus != ADDNEW_STATUS && currentDetailImportId != null || currentPaletId != null)
            {
                txtSerialPaletImportManage.Text = "";
                dtpBeginDatePaletImportManage.Value = DateTime.Now;
                dtpEndDatePaletImportManage.Value = DateTime.Now;
                dtpCreateDateSubTabImportManage.Value = DateTime.Now;
                txtLincensePlateImportManage.Text = "";
                txtNoteDetailImportImportManage.Text = "";
                txtStatusPlateImportManage.Text = "";
                currentDetailImportId = null;
                currentPaletId = null;
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (serialPalet == "" || licensePlateDetailImport == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serialPalet == "") message += "\n" + "  " + "- " + lbSerialDetailImportImportManage.Text;
                    if (licensePlateDetailImport == "") message += "\n" + "  " + "- " + lbPlateDetailImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createDetailImportPalet", culture) + " " + LocRM.GetString("importCode", culture) + " " + currentImportId + ": "
                                                                        + "\n" + "- " + lbSerialDetailImportImportManage.Text + ": " + serialPalet
                                                                        + "\n" + "- " + lbPlateDetailImportImportManage.Text + ": " + licensePlateDetailImport
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createDetailImportPalet", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result1 = await Utils.callApiPostAddNewDetailImport(currentImportId, serialPalet, licensePlateDetailImport, createDateDetailImport, noteDetailImport);
                        int result2 = await Utils.callApiPostAddNewPalet(whId, serialPalet, beginDate, endDate, status);
                        if (result1 == 1 && result2 == 1)
                        {
                            if (tempDataTableDetailImportAndPalet == null)
                            {
                                tempDataTableDetailImportAndPalet = new DataTable();
                                tempDataTableDetailImportAndPalet.Columns.Add("IdPalet");
                                tempDataTableDetailImportAndPalet.Columns.Add("IdImport");
                                tempDataTableDetailImportAndPalet.Columns.Add("SerialPalet");
                                tempDataTableDetailImportAndPalet.Columns.Add("LicensePlate");
                                tempDataTableDetailImportAndPalet.Columns.Add("Date");
                                tempDataTableDetailImportAndPalet.Columns.Add("Notes");
                                tempDataTableDetailImportAndPalet.Columns.Add("IdDetailImport");
                                tempDataTableDetailImportAndPalet.Columns.Add("IdWareHouse");
                                tempDataTableDetailImportAndPalet.Columns.Add("Serial");
                                tempDataTableDetailImportAndPalet.Columns.Add("BeginDate");
                                tempDataTableDetailImportAndPalet.Columns.Add("EndDate");
                                tempDataTableDetailImportAndPalet.Columns.Add("Status");
                            }
                            DataRow dr = tempDataTableDetailImportAndPalet.NewRow();
                            dr["IdPalet"] = Utils.idNewPalet;
                            dr["IdImport"] = currentImportId;
                            dr["SerialPalet"] = serialPalet;
                            dr["LicensePlate"] = licensePlateDetailImport;
                            dr["Date"] = createDateDetailImport;
                            dr["Notes"] = noteDetailImport;
                            dr["IdDetailImport"] = Utils.idNewDetailImport;
                            dr["IdWareHouse"] = whId;
                            dr["Serial"] = serialPalet;
                            dr["BeginDate"] = beginDate;
                            dr["EndDate"] = endDate;
                            dr["Status"] = status;
                            tempDataTableDetailImportAndPalet.Rows.Add(dr);
                            loadDataGridViewDetailImportAndPalet(tempDataTableDetailImportAndPalet);
                        }
                    }
                }
            }
        }

        private async void updateImport()
        {
            string nameImport = txtNameImportManage.Text;
            string sourceWhId = cbWhExImportManage.SelectedValue.ToString();
            string currentWhIdImport = cbWhCurrentImportManage.SelectedValue.ToString();
            string serial = txtSerialImportManage.Text;
            string createDate = dtpCreateDateImportManage.Value.Date.ToString();
            string updateDate = dtpUpdateDateImportManage.Value.Date.ToString();
            string typeProduct = cbProduceImportManage.SelectedValue.ToString();
            string quantityPalet = txtQuantityImportManage.Text;
            string note = txtNoteImportManage.Text;
            string username = txtUsernameImportManage.Text;
            //string userId = txtUserIdImportManage.Text;

            if (!string.IsNullOrEmpty(currentImportId) && actionStatus == UPDATE_STATUS)
            {
                if (nameImport == "" || serial == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (nameImport == "") message += "\n" + "  " + "- " + lbNameImportManage.Text;
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateImport", culture) + ": " + "[" + nameImport + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateImport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdateImport(currentImportId, currentWhIdImport, sourceWhId, nameImport, createDate, serial, typeProduct, quantityPalet, note, username, DateTime.Now.ToString());
                        switch (result)
                        {
                            case 1:
                                LoadTabImportManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    // }
                }
            }
        }

        private async void addNewImport()
        {
            string nameImport = txtNameImportManage.Text;
            string sourceWhId = cbWhExImportManage.SelectedValue.ToString();
            string currentWhIdImport = cbWhCurrentImportManage.SelectedValue.ToString();
            string serial = txtSerialImportManage.Text;
            string createDate = dtpCreateDateImportManage.Value.Date.ToString();
            string updateDate = dtpUpdateDateImportManage.Value.Date.ToString();
            string typeProduct = cbProduceImportManage.SelectedValue.ToString();
            string quantityPalet = "0";
            string note = txtNoteImportManage.Text;
            string username = txtUsernameImportManage.Text;
            //string userId = Utils.CURRENT_ID_USER;
            //string userId = "1";

            currentImportId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtNameImportManage.Text = "";
                dtpCreateDateImportManage.Value = DateTime.Now;
                dtpUpdateDateImportManage.Value = DateTime.Now;
                txtNoteImportManage.Text = "";
                txtQuantityImportManage.Text = "0";
                cbWhCurrentImportManage.SelectedIndex = cbWhCurrentImportManage.FindString(Utils.findWhNameByWhId(Utils.CURRENT_ID_WAREHOUSE));
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (nameImport == "" || serial == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialImportImportManage.Text;
                    if (nameImport == "") message += "\n" + "  " + "- " + lbNameImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createImport", culture) + ": "
                                                                        + "\n" + "- " + lbNameImportManage.Text + ": " + nameImport
                                                                        + "\n" + "- " + lbSerialImportImportManage.Text + ": " + serial
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createImport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewImport(Utils.CURRENT_ID_WAREHOUSE, sourceWhId, nameImport, DateTime.Now.ToString(), serial, typeProduct, quantityPalet, note, username, DateTime.Now.ToString());
                        switch (result)
                        {
                            case 1:
                                LoadTabImportManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }

        private void LoadTabImportManage()
        {
            //load datagridview list user
            if (!File.Exists(@"Import.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"Import.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            dataGridViewImportImportManage.DataSource = ds.Tables["Item"];
            //dataGridViewWHManage.DataSource = null;
            //dataGridViewWHManage.DataSource = Utils.entityWH.WAREHOUSEs.ToList();
            dataGridViewImportImportManage.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewImportImportManage.Columns[1].HeaderText = LocRM.GetString("currentWh", culture);
            dataGridViewImportImportManage.Columns[2].HeaderText = LocRM.GetString("sourceWh", culture);
            dataGridViewImportImportManage.Columns[3].HeaderText = LocRM.GetString("importName", culture);
            dataGridViewImportImportManage.Columns[4].HeaderText = LocRM.GetString("createDate", culture);
            dataGridViewImportImportManage.Columns[5].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewImportImportManage.Columns[6].HeaderText = LocRM.GetString("goodType", culture);
            dataGridViewImportImportManage.Columns[7].HeaderText = LocRM.GetString("quantityPallet", culture);
            dataGridViewImportImportManage.Columns[8].HeaderText = LocRM.GetString("note", culture);
            dataGridViewImportImportManage.Columns[9].HeaderText = LocRM.GetString("syncStatus", culture);
            dataGridViewImportImportManage.Columns[10].HeaderText = LocRM.GetString("createdUser", culture);
            dataGridViewImportImportManage.Columns[11].HeaderText = LocRM.GetString("updateData", culture);
            //dataGridViewImportImportManage.Columns[12].HeaderText = LocRM.GetString("createdUser", culture);

            dataGridViewImportImportManage.Columns[0].Visible = false;
            dataGridViewImportImportManage.Columns[9].Visible = false;
            //dataGridViewImportImportManage.Columns[10].Visible = false;
            foreach (DataGridViewRow row in dataGridViewPaletManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
            cbWhCurrentImportManage.SelectedIndex = cbWhCurrentImportManage.FindString(Utils.findWhNameByWhId(Utils.CURRENT_ID_WAREHOUSE));
            //txtQuantityImportManage.Text = "0";

        }
        #endregion

        #region EXPORT REGION
        private void loadDataGridViewDetailExportAndPalet(DataTable dt)
        {
            dataGridViewDetailExportAndPalet.DataSource = dt;
            dataGridViewDetailExportAndPalet.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewDetailExportAndPalet.Columns[1].HeaderText = LocRM.GetString("importCode", culture);
            dataGridViewDetailExportAndPalet.Columns[2].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewDetailExportAndPalet.Columns[3].HeaderText = LocRM.GetString("numberPlate", culture);
            dataGridViewDetailExportAndPalet.Columns[4].HeaderText = LocRM.GetString("createDate", culture);
            dataGridViewDetailExportAndPalet.Columns[5].HeaderText = LocRM.GetString("note", culture);
            dataGridViewDetailExportAndPalet.Columns[6].HeaderText = LocRM.GetString("id", culture);
            dataGridViewDetailExportAndPalet.Columns[7].HeaderText = LocRM.GetString("whId", culture);
            dataGridViewDetailExportAndPalet.Columns[8].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewDetailExportAndPalet.Columns[9].HeaderText = LocRM.GetString("beginDate", culture);
            dataGridViewDetailExportAndPalet.Columns[10].HeaderText = LocRM.GetString("endDate", culture);
            dataGridViewDetailExportAndPalet.Columns[11].HeaderText = LocRM.GetString("status", culture);

            dataGridViewDetailExportAndPalet.Columns[0].Visible = false;
            dataGridViewDetailExportAndPalet.Columns[6].Visible = false;
            dataGridViewDetailExportAndPalet.Columns[8].Visible = false;
            foreach (DataGridViewRow row in dataGridViewDetailExportAndPalet.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }

        private async void updateDetailExportAndPalet()
        {
            string serialPalet = txtSerialPaletExportManage.Text;
            string licensePlateDetailExport = txtLicensePlateExportManage.Text;
            string createDateDetailExport = dtpCreateDateSubTabExport.Value.Date.ToString();
            string noteDetailExport = txtNoteSubTabExport.Text;

            string whId = Utils.CURRENT_ID_WAREHOUSE;
            string beginDate = dtpBeginDatePaletImportManage.Value.Date.ToString();
            string endDate = dtpEndDatePaletImportManage.Value.Date.ToString();
            string status = txtStatusPlateImportManage.Text;

            if (string.IsNullOrEmpty(currentExportId))
            {
                MessageBox.Show(LocRM.GetString("plsChoseExport", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!string.IsNullOrEmpty(currentDetailImportId) && !string.IsNullOrEmpty(currentPaletId) && actionStatus == UPDATE_STATUS)
            {
                if (serialPalet == "" || licensePlateDetailExport == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serialPalet == "") message += "\n" + "  " + "- " + lbSerialDetailImportImportManage.Text;
                    if (licensePlateDetailExport == "") message += "\n" + "  " + "- " + lbPlateDetailImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateDetailExport", culture) + ": " + "[" + serialPalet + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateDetailExport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result1 = await Utils.callApiPostUpdateDetailExport(currentDetailExportId, currentExportId, serialPalet, licensePlateDetailExport, createDateDetailExport, noteDetailExport);
                        //int result2 = await Utils.callApiPostUpdatePalet(currentPaletId, whId, serialPalet, beginDate, endDate, status);
                        if (result1 == 1)
                        {
                            if (tempDataTableDetailExportAndPalet != null)
                            {
                                foreach (DataRow dr in tempDataTableDetailExportAndPalet.Rows)
                                {
                                    if (dr["IdPalet"].ToString().Equals(currentPaletId) && dr["IdDetailExport"].ToString().Equals(currentDetailExportId))
                                    {
                                        dr["IdExport"] = currentExportId;
                                        dr["SerialPalet"] = serialPalet;
                                        dr["LicensePlate"] = licensePlateDetailExport;
                                        dr["Date"] = createDateDetailExport;
                                        dr["Notes"] = noteDetailExport;
                                        dr["IdWareHouse"] = whId;
                                        dr["Serial"] = serialPalet;
                                        dr["BeginDate"] = beginDate;
                                        dr["EndDate"] = endDate;
                                        dr["Status"] = status;
                                    }
                                }
                                loadDataGridViewDetailExportAndPalet(tempDataTableDetailExportAndPalet);
                            }
                        }

                    }
                }
            }

        }
        private async void addNewDetailExportAndPalet()
        {
            string serialPalet = txtSerialPaletExportManage.Text;
            string licensePlateDetailExport = txtLicensePlateExportManage.Text;
            string createDateDetailExport = dtpCreateDateSubTabExport.Value.Date.ToString();
            string noteDetailExport = txtNoteSubTabExport.Text;
            //currentImportId

            string whId = Utils.CURRENT_ID_WAREHOUSE;
            string beginDate = dtpBeginDatePaletExport.Value.Date.ToString();
            string endDate = dtpEndDatePaletExport.Value.Date.ToString();
            string status = txtStatusPaletExport.Text;

            if (string.IsNullOrEmpty(currentExportId))
            {
                MessageBox.Show(LocRM.GetString("plsChoseExport", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (string.IsNullOrEmpty(currentPaletExportId))
            {
                MessageBox.Show("vui lòng chọn hàng hóa xuất kho", LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (actionStatus != ADDNEW_STATUS && currentDetailImportId != null || currentPaletId != null)
            {
                txtSerialPaletExportManage.Text = "";
                dtpBeginDatePaletExport.Value = DateTime.Now;
                dtpEndDatePaletExport.Value = DateTime.Now;
                dtpCreateDateSubTabExport.Value = DateTime.Now;
                txtLicensePlateExportManage.Text = "";
                txtNoteSubTabExport.Text = "";
                txtStatusPaletExport.Text = "";
                currentDetailExportId = null;
                currentPaletId = null;
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (serialPalet == "" || licensePlateDetailExport == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serialPalet == "") message += "\n" + "  " + "- " + lbSerialDetailImportImportManage.Text;
                    if (licensePlateDetailExport == "") message += "\n" + "  " + "- " + lbPlateDetailImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createDetailExportPalet", culture) + " " + LocRM.GetString("importCode", culture) + " " + currentImportId + ": "
                                                                        + "\n" + "- " + lbSerialDetailImportImportManage.Text + ": " + serialPalet
                                                                        + "\n" + "- " + lbPlateDetailImportImportManage.Text + ": " + licensePlateDetailExport
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createDetailExportPalet", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result1 = await Utils.callApiPostAddNewDetailExport(currentExportId, serialPalet, licensePlateDetailExport, createDateDetailExport, noteDetailExport);
                        //int result2 = await Utils.callApiPostAddNewPalet(whId, serialPalet, beginDate, endDate, status);
                        if (result1 == 1)
                        {
                            if (tempDataTableDetailExportAndPalet == null)
                            {
                                tempDataTableDetailExportAndPalet = new DataTable();
                                tempDataTableDetailExportAndPalet.Columns.Add("IdPalet");
                                tempDataTableDetailExportAndPalet.Columns.Add("IdExport");
                                tempDataTableDetailExportAndPalet.Columns.Add("SerialPalet");
                                tempDataTableDetailExportAndPalet.Columns.Add("LicensePlate");
                                tempDataTableDetailExportAndPalet.Columns.Add("Date");
                                tempDataTableDetailExportAndPalet.Columns.Add("Notes");
                                tempDataTableDetailExportAndPalet.Columns.Add("IdDetailImport");
                                tempDataTableDetailExportAndPalet.Columns.Add("IdWareHouse");
                                tempDataTableDetailExportAndPalet.Columns.Add("Serial");
                                tempDataTableDetailExportAndPalet.Columns.Add("BeginDate");
                                tempDataTableDetailExportAndPalet.Columns.Add("EndDate");
                                tempDataTableDetailExportAndPalet.Columns.Add("Status");
                            }
                            DataRow dr = tempDataTableDetailExportAndPalet.NewRow();
                            dr["IdPalet"] = Utils.idNewPalet;
                            dr["IdExport"] = currentExportId;
                            dr["SerialPalet"] = serialPalet;
                            dr["LicensePlate"] = licensePlateDetailExport;
                            dr["Date"] = createDateDetailExport;
                            dr["Notes"] = noteDetailExport;
                            dr["IdDetailImport"] = Utils.idNewDetailExport;
                            dr["IdWareHouse"] = whId;
                            dr["Serial"] = serialPalet;
                            dr["BeginDate"] = beginDate;
                            dr["EndDate"] = endDate;
                            dr["Status"] = status;
                            tempDataTableDetailExportAndPalet.Rows.Add(dr);
                            loadDataGridViewDetailExportAndPalet(tempDataTableDetailExportAndPalet);
                        }
                    }
                }
            }
        }
        private async void updateExport()
        {
            string nameExport = txtNameExportExportManage.Text;
            string imWhId = cbImWhExportManage.SelectedValue.ToString();
            string currentWhIdImport = cbCurrentWhExportManage.SelectedValue.ToString();
            string serial = txtSerialExportManage.Text;
            string createDate = dtpCreateDateExportManage.Value.Date.ToString();
            string updateDate = dtpUpdateDateExportManage.Value.Date.ToString();
            string typeProduct = cbTypeProductExportManage.SelectedValue.ToString();
            string quantityPalet = "0";
            string note = txtNoteExportManage.Text;
            string username = txtCreateUserExportManage.Text;

            if (!string.IsNullOrEmpty(currentExportId) && actionStatus == UPDATE_STATUS)
            {
                if (nameExport == "" || serial == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (nameExport == "") message += "\n" + "  " + "- " + lbNameExportManage.Text;
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialImportImportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateExport", culture) + ": " + "[" + nameExport + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateExport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdateExport(currentExportId, currentWhIdImport, imWhId, nameExport, createDate, serial, typeProduct, quantityPalet, note, username, DateTime.Now.ToString());
                        switch (result)
                        {
                            case 1:
                                LoadTabExportManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    // }
                }
            }
        }
        private async void addNewExport()
        {
            string nameExport = txtNameExportExportManage.Text;
            string imWhId = cbImWhExportManage.SelectedValue.ToString();
            string currentWhIdImport = cbCurrentWhExportManage.SelectedValue.ToString();
            string serial = txtSerialExportManage.Text;
            string createDate = dtpCreateDateExportManage.Value.Date.ToString();
            string updateDate = dtpUpdateDateExportManage.Value.Date.ToString();
            string typeProduct = cbTypeProductExportManage.SelectedValue.ToString();
            string quantityPalet = "0";
            string note = txtNoteExportManage.Text;
            string username = txtCreateUserExportManage.Text;
            //string userId = Utils.CURRENT_ID_USER;
            //string userId = "1";

            currentExportId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtNameExportExportManage.Text = "";
                dtpCreateDateExportManage.Value = DateTime.Now;
                dtpUpdateDateExportManage.Value = DateTime.Now;
                txtNoteExportManage.Text = "";
                txtSerialExportManage.Text = "";
                txtQuantityPaletExportManage.Text = "0";
                cbCurrentWhExportManage.SelectedIndex = cbCurrentWhExportManage.FindString(Utils.findWhNameByWhId(Utils.CURRENT_ID_WAREHOUSE));
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (nameExport == "" || serial == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialImportImportManage.Text;
                    if (nameExport == "") message += "\n" + "  " + "- " + lbNameExportManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createExport", culture) + ": "
                                                                        + "\n" + "- " + lbNameExportManage.Text + ": " + nameExport
                                                                        + "\n" + "- " + lbSerialImportImportManage.Text + ": " + serial
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createExport", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewExport(Utils.CURRENT_ID_WAREHOUSE, imWhId, nameExport, DateTime.Now.ToString(), serial, typeProduct, quantityPalet, note, username, DateTime.Now.ToString());
                        switch (result)
                        {
                            case 1:
                                LoadTabExportManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }
        private void LoadGridViewPaletInExport()
        {
            //load datagridview list user
            if (!File.Exists(@"PaletForExport.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"PaletForExport.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            //ds.Tables["Item"].Columns.Add("Xuất");
            dataGridViewPaletExportManage.DataSource = ds.Tables["Item"];
            //dataGridViewWHManage.DataSource = null;
            //dataGridViewWHManage.DataSource = Utils.entityWH.WAREHOUSEs.ToList();
            dataGridViewPaletExportManage.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewPaletExportManage.Columns[1].HeaderText = LocRM.GetString("wh", culture);
            dataGridViewPaletExportManage.Columns[2].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewPaletExportManage.Columns[3].HeaderText = LocRM.GetString("beginDate", culture);
            dataGridViewPaletExportManage.Columns[4].HeaderText = LocRM.GetString("endDate", culture);
            dataGridViewPaletExportManage.Columns[5].HeaderText = LocRM.GetString("status", culture);
            dataGridViewPaletExportManage.Columns[6].HeaderText = LocRM.GetString("syncStatus", culture);


            dataGridViewPaletExportManage.Columns[0].Visible = false;
            dataGridViewPaletExportManage.Columns[6].Visible = false;
            foreach (DataGridViewRow row in dataGridViewPaletExportManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }


            DataGridViewButtonColumn addButtonColumn = new DataGridViewButtonColumn();
            addButtonColumn.Name = "Xuất kho";
            addButtonColumn.Text = "Xuất kho";
            addButtonColumn.UseColumnTextForButtonValue = true;
            if (dataGridViewPaletExportManage.Columns["button"] == null)
            {
                dataGridViewPaletExportManage.Columns.Insert(0, addButtonColumn);
            }
        }
        private void LoadTabExportManage()
        {
            //load datagridview list user
            if (!File.Exists(@"Export.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"Export.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            dataGridViewListExport.DataSource = ds.Tables["Item"];
            //dataGridViewWHManage.DataSource = null;
            //dataGridViewWHManage.DataSource = Utils.entityWH.WAREHOUSEs.ToList();
            dataGridViewListExport.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewListExport.Columns[1].HeaderText = LocRM.GetString("currentWh", culture);
            dataGridViewListExport.Columns[2].HeaderText = LocRM.GetString("sourceWh", culture);
            dataGridViewListExport.Columns[3].HeaderText = LocRM.GetString("importName", culture);
            dataGridViewListExport.Columns[4].HeaderText = LocRM.GetString("createDate", culture);
            dataGridViewListExport.Columns[5].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewListExport.Columns[6].HeaderText = LocRM.GetString("goodType", culture);
            dataGridViewListExport.Columns[7].HeaderText = LocRM.GetString("quantityPallet", culture);
            dataGridViewListExport.Columns[8].HeaderText = LocRM.GetString("note", culture);
            dataGridViewListExport.Columns[9].HeaderText = LocRM.GetString("syncStatus", culture);
            dataGridViewListExport.Columns[10].HeaderText = LocRM.GetString("createdUser", culture);
            dataGridViewListExport.Columns[11].HeaderText = LocRM.GetString("updateData", culture);

            dataGridViewListExport.Columns[0].Visible = false;
            dataGridViewListExport.Columns[9].Visible = false;
            //dataGridViewImportImportManage.Columns[10].Visible = false;
            foreach (DataGridViewRow row in dataGridViewListExport.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
            cbCurrentWhExportManage.SelectedIndex = cbCurrentWhExportManage.FindString(Utils.findWhNameByWhId(Utils.CURRENT_ID_WAREHOUSE));
            //txtQuantityImportManage.Text = "0";

        }
        #endregion

        #region PALET REGION
        private async void updatePalet()
        {
            string whId = cbWhPaletManage.SelectedValue.ToString();
            string serial = txtSerialPaletManage.Text;
            string beginDate = dtpBeginDateTruckManage.Value.Date.ToString();
            string endDate = dtpEndDateTruckManage.Value.Date.ToString();
            string status = txtStatusPaletManage.Text;

            if (!string.IsNullOrEmpty(currentPaletId) && actionStatus == UPDATE_STATUS)
            {
                if (serial == "" || status == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialPaletManage.Text;
                    if (status == "") message += "\n" + "  " + "- " + lbStatusPaletManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updatePalet", culture) + ": " + "[" + serial + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updatePalet", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdatePalet(currentPaletId, whId, serial, beginDate, endDate, status);
                        switch (result)
                        {
                            case 1:
                                LoadTabPaletManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    // }
                }
            }
        }

        private async void deletePalet()
        {
            string whId = cbWhPaletManage.SelectedValue.ToString();
            string serial = txtSerialPaletManage.Text;
            string beginDate = dtpBeginDateTruckManage.Value.Date.ToString();
            string endDate = dtpEndDateTruckManage.Value.Date.ToString();
            string status = txtStatusPaletManage.Text;


            if (currentPaletId != null && actionStatus == UPDATE_STATUS)
            {
                string message = LocRM.GetString("deletePalet", culture) + ": " + "[" + serial + "]"
                                                                        + "\n" + "- " + lbWhPaletManage.Text + ": " + cbWhPaletManage.FindString(Utils.findWhNameByWhId(whId))
                                                                        + "\n" + "- " + lbStatusPaletManage.Text + ": " + status
                                                                        + " ?";
                DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("deletePalet", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    int result = await Utils.callApiPostDeletePalet(currentPaletId);
                    switch (result)
                    {
                        case 1:
                            LoadTabPaletManage();
                            break;
                        case -1:
                            //no link
                            break;

                        case 2:
                            // error
                            break;

                        default:
                            //show result
                            break;
                    }
                }
            }
        }

        private async void addNewPalet()
        {
            string whId = cbWhPaletManage.SelectedValue.ToString();
            string serial = txtSerialPaletManage.Text;
            string beginDate = dtpBeginDateTruckManage.Value.Date.ToString();
            string endDate = dtpEndDateTruckManage.Value.Date.ToString();
            string status = txtStatusPaletManage.Text;

            currentPaletId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtSerialPaletManage.Text = "";
                dtpBeginDatePaletManage.Value = DateTime.Now;
                dtpEndDatePaletManage.Value = DateTime.Now;
                txtStatusPaletManage.Text = "";
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (status == "" || serial == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (serial == "") message += "\n" + "  " + "- " + lbSerialPaletManage.Text;
                    if (status == "") message += "\n" + "  " + "- " + lbStatusPaletManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createPalet", culture) + ": "
                                                                        + "\n" + "- " + lbSerialPaletManage.Text + ": " + serial
                                                                        + "\n" + "- " + lbBeginDatePaletManage.Text + ": " + beginDate
                                                                        + "\n" + "- " + lbEndDatePaletManage.Text + ": " + endDate
                                                                        + "\n" + "- " + lbStatusPaletManage.Text + ": " + status
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createPalet", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewPalet(whId, serial, beginDate, endDate, status);
                        switch (result)
                        {
                            case 1:
                                LoadTabPaletManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }

        private void LoadTabPaletManage()
        {
            //load datagridview list user
            if (!File.Exists(@"Palet.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"Palet.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            dataGridViewPaletManage.DataSource = ds.Tables["Item"];
            //dataGridViewWHManage.DataSource = null;
            //dataGridViewWHManage.DataSource = Utils.entityWH.WAREHOUSEs.ToList();
            dataGridViewPaletManage.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewPaletManage.Columns[1].HeaderText = LocRM.GetString("wh", culture);
            dataGridViewPaletManage.Columns[2].HeaderText = LocRM.GetString("serial", culture);
            dataGridViewPaletManage.Columns[3].HeaderText = LocRM.GetString("beginDate", culture);
            dataGridViewPaletManage.Columns[4].HeaderText = LocRM.GetString("endDate", culture);
            dataGridViewPaletManage.Columns[5].HeaderText = LocRM.GetString("status", culture);
            dataGridViewPaletManage.Columns[6].HeaderText = LocRM.GetString("syncStatus", culture);


            dataGridViewPaletManage.Columns[0].Visible = false;
            dataGridViewPaletManage.Columns[6].Visible = false;
            foreach (DataGridViewRow row in dataGridViewPaletManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }
        #endregion

        #region TRUCK REGION
        private async void updateTruck()
        {
            string licensePlate = txtLicensePlateTruckManages.Text;
            string whId = cbWhTruckManage.SelectedValue.ToString();
            string beginDate = dtpBeginDateTruckManage.Value.Date.ToString();
            string endDate = dtpEndDateTruckManage.Value.Date.ToString();
            string status = txtStatusTruckManage.Text;
            string owner = txtOwnerTruckManage.Text;
            string address = txtAddressTruckManage.Text;
            string phone = txtPhoneTruckManage.Text;
            string cmnd = txtIdCardTruckManage.Text;
            string driver = txtDriverTruckManage.Text;
            string note = txtNoteTruckManage.Text;

            if (!string.IsNullOrEmpty(currentTruckId) && actionStatus == UPDATE_STATUS)
            {
                if (licensePlate == "" || owner == "" || phone == "" || cmnd == "" || driver == "" || address == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (licensePlate == "") message += "\n" + "  " + "- " + lbLicensePlateTruckManage.Text;
                    if (owner == "") message += "\n" + "  " + "- " + lbOwnerTruckManage.Text;
                    if (phone == "") message += "\n" + "  " + "- " + lbPhoneTruck.Text;
                    if (cmnd == "") message += "\n" + "  " + "- " + lbIdCardTruckManage.Text;
                    if (driver == "") message += "\n" + "  " + "- " + lbDriverTruckManage.Text;
                    if (address == "") message += "\n" + "  " + "- " + lbAddressTruck.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateTruck", culture) + ": " + "[" + licensePlate + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateTruck", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdateTruck(currentTruckId, licensePlate, whId, beginDate, endDate, status, owner, address, phone, cmnd, note, driver);
                        switch (result)
                        {
                            case 1:
                                LoadTabTruckManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    // }
                }
            }
        }

        private async void deleteTruck()
        {
            //string whId = txtWhIdWhManage.Text.ToString();
            string licensePlate = txtLicensePlateTruckManages.Text;
            string drivewName = txtDriverTruckManage.Text;
            string ownerName = txtOwnerTruckManage.Text;


            if (currentTruckId != null && actionStatus == UPDATE_STATUS)
            {
                string message = LocRM.GetString("deleteTruck", culture) + ": " + "[" + licensePlate + "]"
                                                                        + "\n" + "- " + lbDriverTruckManage.Text + ": " + drivewName
                                                                        + "\n" + "- " + lbOwnerTruckManage.Text + ": " + ownerName
                                                                        + "\n" + "- " + lbWhTruckManage.Text + ": " + cbWhTruckManage.Text
                                                                        + " ?";
                DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("deleteTruck", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    int result = await Utils.callApiPostDeleteTruck(currentTruckId);
                    switch (result)
                    {
                        case 1:
                            LoadTabTruckManage();
                            break;
                        case -1:
                            //no link
                            break;

                        case 2:
                            // error
                            break;

                        default:
                            //show result
                            break;
                    }
                }
            }
        }

        private async void addNewTruck()
        {
            string licensePlate = txtLicensePlateTruckManages.Text;
            string whId = cbWhTruckManage.SelectedValue.ToString();
            string beginDate = dtpBeginDateTruckManage.Value.Date.ToString();
            string endDate = dtpEndDateTruckManage.Value.Date.ToString();
            string status = txtStatusTruckManage.Text;
            string owner = txtOwnerTruckManage.Text;
            string address = txtAddressTruckManage.Text;
            string phone = txtPhoneTruckManage.Text;
            string cmnd = txtIdCardTruckManage.Text;
            string driver = txtDriverTruckManage.Text;
            string note = txtNoteTruckManage.Text;

            currentTruckId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtLicensePlateTruckManages.Text = "";
                dtpBeginDateTruckManage.Value = DateTime.Now;
                dtpEndDateTruckManage.Value = DateTime.Now;
                txtStatusTruckManage.Text = "";
                txtOwnerTruckManage.Text = "";
                txtAddressTruckManage.Text = "";
                txtPhoneTruckManage.Text = "";
                txtIdCardTruckManage.Text = "";
                txtDriverTruckManage.Text = "";
                txtNoteTruckManage.Text = "";
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (licensePlate == "" || owner == "" || phone == "" || cmnd == "" || driver == "" || address == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (licensePlate == "") message += "\n" + "  " + "- " + lbLicensePlateTruckManage.Text;
                    if (owner == "") message += "\n" + "  " + "- " + lbOwnerTruckManage.Text;
                    if (phone == "") message += "\n" + "  " + "- " + lbPhoneTruck.Text;
                    if (cmnd == "") message += "\n" + "  " + "- " + lbIdCardTruckManage.Text;
                    if (driver == "") message += "\n" + "  " + "- " + lbDriverTruckManage.Text;
                    if (address == "") message += "\n" + "  " + "- " + lbAddressTruck.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createTruck", culture) + ": "
                                                                        + "\n" + "- " + lbLicensePlateTruckManage.Text + ": " + licensePlate
                                                                        + "\n" + "- " + lbDriverTruckManage.Text + ": " + driver
                                                                        + "\n" + "- " + lbOwnerTruckManage.Text + ": " + owner
                                                                        + "\n" + "- " + lbPhoneTruck.Text + ": " + phone
                                                                        + "\n" + "- " + lbIdCardTruckManage.Text + ": " + cmnd
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createTruck", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewTruck(licensePlate, whId, beginDate, endDate, status, owner, address, phone, cmnd, note, driver);
                        switch (result)
                        {
                            case 1:
                                LoadTabTruckManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }

        private void LoadTabTruckManage()
        {
            //load datagridview list user
            if (!File.Exists(@"Truck.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"Truck.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            dataGridViewTruckManage.DataSource = ds.Tables["Item"];
            //dataGridViewWHManage.DataSource = null;
            //dataGridViewWHManage.DataSource = Utils.entityWH.WAREHOUSEs.ToList();
            dataGridViewTruckManage.Columns[0].HeaderText = LocRM.GetString("id", culture);
            dataGridViewTruckManage.Columns[1].HeaderText = LocRM.GetString("numberPlate", culture);
            dataGridViewTruckManage.Columns[2].HeaderText = LocRM.GetString("whId", culture);
            dataGridViewTruckManage.Columns[3].HeaderText = LocRM.GetString("beginDate", culture);
            dataGridViewTruckManage.Columns[4].HeaderText = LocRM.GetString("endDate", culture);
            dataGridViewTruckManage.Columns[5].HeaderText = LocRM.GetString("truckStatus", culture);
            dataGridViewTruckManage.Columns[6].HeaderText = LocRM.GetString("truckOwner", culture);
            dataGridViewTruckManage.Columns[7].HeaderText = LocRM.GetString("address", culture);
            dataGridViewTruckManage.Columns[8].HeaderText = LocRM.GetString("phone", culture);
            dataGridViewTruckManage.Columns[9].HeaderText = LocRM.GetString("idCard", culture);
            dataGridViewTruckManage.Columns[10].HeaderText = LocRM.GetString("note", culture);
            dataGridViewTruckManage.Columns[11].HeaderText = LocRM.GetString("syncStatus", culture);
            dataGridViewTruckManage.Columns[12].HeaderText = LocRM.GetString("driverName", culture);

            //dataGridViewTruckManage.Columns[0].Visible = false;
            dataGridViewTruckManage.Columns[11].Visible = false;
            foreach (DataGridViewRow row in dataGridViewTruckManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }
        #endregion

        #region WAREHOUSE REGION
        private async void updateWarehouse()
        {
            string whName = txtNameWHManage.Text;
            string addressWH = txtAddressWHManage.Text;
            string phoneWH = txtPhoneWHManage.Text;
            string noteWH = txtNoteWHManage.Text;
            string quantityPallet = txtPalletWHManage.Text;
            string area = cbAreaWHManage.Text.ToString();
            string linkWS = txtLinkWSWHManage.Text;
            string isHeadOffice = cbIsHOWhManage.SelectedValue.ToString();

            if (!string.IsNullOrEmpty(currentWHId) && txtNameWHManage.ReadOnly)
            {
                if (addressWH == "" || phoneWH == "" || area == "" || linkWS == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (addressWH == "") message += "\n" + "  " + "- " + lbAddressWHManage.Text;
                    if (phoneWH == "") message += "\n" + "  " + "- " + lbPhoneWHManage.Text;
                    if (area == "") message += "\n" + "  " + "- " + lbAreaWHManage.Text;
                    if (linkWS == "") message += "\n" + "  " + "- " + lbLinkWSWHManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (branchId.Equals(currentWH.IdBranch) && addressWH.Equals(currentWH.Address) && phoneWH.Equals(currentWH.Phone) && noteWH.Equals(currentWH.Note) && quantityPallet.Equals(currentWH.QuantityPalet+""))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateWarehouse", culture) + ": " + "[" + whName + "]";
                    // if (!branchId.Equals(currentWH.IdBranch)) message += "\n" + "- " + lbBranchWHManage.Text + ": " + cbBranchWHManage.SelectedText.ToString();
                    //if (!addressWH.Equals(currentWH.Address)) message += "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text;
                    // if (!phoneWH.Equals(currentWH.Phone)) message += "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text;
                    //if (!noteWH.Equals(currentWH.Note)) message += "\n" + "- " + lbNoteWHManage + ": " + txtNoteWHManage.Text;
                    // if (!quantityPallet.Equals(currentWH.QuantityPalet + "")) message += "\n" + "- " + lbPalletWHManage + ": " + txtPalletWHManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateWarehouse", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdateWH(currentWHId, whName, addressWH, phoneWH, noteWH, quantityPallet, area, linkWS, isHeadOffice);
                        switch (result)
                        {
                            case 1:
                                LoadTabWarehouseManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    // }
                }
            }
        }

        private async void deleteWarehouse()
        {
            string whId = txtWhIdWhManage.Text.ToString();
            string whName = txtNameWHManage.Text;
            string addressWH = txtAddressWHManage.Text;
            string phoneWH = txtPhoneWHManage.Text;
            string noteWH = txtNoteWHManage.Text;
            string quantityPallet = txtPalletWHManage.Text;


            if (currentWHId != null && actionStatus == UPDATE_STATUS)
            {
                string message = LocRM.GetString("deleteWarehouse", culture) + ": " + "[" + whName + "]"
                                                                        + "\n" + "- " + lbWhIdManage.Text + ": " + txtWhIdWhManage.Text.ToString()
                                                                        + "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text
                                                                        + "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text
                                                                        + "\n" + "- " + lbPalletWHManage.Text + ": " + txtPalletWHManage.Text
                                                                        + " ?";
                DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("deleteWarehouse", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    int result = await Utils.callApiPostDeleteWarehouse(currentWHId);
                    switch (result)
                    {
                        case 1:
                            LoadTabWarehouseManage();
                            break;
                        case -1:
                            //no link
                            break;

                        case 2:
                            // error
                            break;

                        default:
                            //show result
                            break;
                    }
                }
            }
        }

        private async void addNewWarehouse()
        {
            string whName = txtNameWHManage.Text;
            string addressWH = txtAddressWHManage.Text;
            string phoneWH = txtPhoneWHManage.Text;
            string noteWH = txtNoteWHManage.Text;
            //string quantityPallet = txtPalletWHManage.Text;
            string quantityPallet = "0";
            string area = cbAreaWHManage.Text;
            string linkws = txtLinkWSWHManage.Text;
            string isHeadOffice = cbIsHOWhManage.SelectedValue.ToString();

            currentWHId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtWhIdWhManage.Text = "";
                txtWhIdWhManage.ReadOnly = true;
                txtNameWHManage.Text = "";
                txtAddressWHManage.Text = "";
                txtPhoneWHManage.Text = "";
                txtNoteWHManage.Text = "";
                txtPalletWHManage.Text = "";
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (whName == "" || addressWH == "" || phoneWH == "" || linkws == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (whName == "") message += "\n" + "  " + "- " + lbNameWHManage.Text;
                    if (addressWH == "") message += "\n" + "  " + "- " + lbAddressWHManage.Text;
                    if (phoneWH == "") message += "\n" + "  " + "- " + lbPhoneWHManage.Text;
                    if (linkws == "") message += "\n" + "  " + "- " + lbLinkWSWHManage.Text;
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createNewWarehouse", culture) + ": "
                                                                        + "\n" + "- " + lbNameWHManage.Text + ": " + txtNameWHManage.Text
                                                                        + "\n" + "- " + lbAddressWHManage.Text + ": " + txtAddressWHManage.Text
                                                                        + "\n" + "- " + lbPhoneWHManage.Text + ": " + txtPhoneWHManage.Text
                                                                        + "\n" + "- " + lbLinkWSWHManage.Text + ": " + txtLinkWSWHManage.Text
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createNewWarehouse", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewWarehouse(whName, addressWH, phoneWH, noteWH, quantityPallet, area, linkws, isHeadOffice);
                        switch (result)
                        {
                            case 1:
                                LoadTabWarehouseManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }

        private void LoadTabWarehouseManage()
        {
            //load datagridview list user
            if (!File.Exists(@"Warehouse.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"Warehouse.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            dataGridViewWHManage.DataSource = ds.Tables["Item"];

            dataGridViewWHManage.Columns[0].HeaderText = LocRM.GetString("whId", culture);
            dataGridViewWHManage.Columns[1].HeaderText = LocRM.GetString("whName", culture);
            dataGridViewWHManage.Columns[2].HeaderText = LocRM.GetString("address", culture);
            dataGridViewWHManage.Columns[3].HeaderText = LocRM.GetString("phone", culture);
            dataGridViewWHManage.Columns[4].HeaderText = LocRM.GetString("note", culture);
            dataGridViewWHManage.Columns[5].HeaderText = LocRM.GetString("syncStatus", culture);
            dataGridViewWHManage.Columns[6].HeaderText = LocRM.GetString("quantityPallet", culture);
            dataGridViewWHManage.Columns[7].HeaderText = LocRM.GetString("area", culture);
            dataGridViewWHManage.Columns[8].HeaderText = LocRM.GetString("linkWS", culture);
            dataGridViewWHManage.Columns[9].HeaderText = LocRM.GetString("isHO", culture);


            dataGridViewWHManage.Columns[5].Visible = false;
            foreach (DataGridViewRow row in dataGridViewWHManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }

            txtWhIdWhManage.ReadOnly = true;
        }
        #endregion

        #region USER REGION
        private async void updateUser()
        {
            string username = txtUsernameUserManage.Text;
            string password = txtPasswordUserManage.Text;
            string clientId = txtClientUserManage.Text;
            string note = txtNoteUserManage.Text;
            string whId = cbWhUserManage.SelectedValue.ToString();

            if (!string.IsNullOrEmpty(currentUserId) && txtUsernameUserManage.ReadOnly)
            {
                if (password == "" || clientId == "" || whId == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (password == "") message += "\n" + "  " + "- " + LocRM.GetString("password", culture);
                    if (clientId == "") message += "\n" + "  " + "- " + LocRM.GetString("clientId", culture);
                    if (whId == "") message += "\n" + "  " + "- " + LocRM.GetString("wh", culture);
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //if (password.Equals(currentUser.Password) && clientId.Equals(currentUser.IdClient) && branchId.Equals(currentUser.IdBranch) && note.Equals(currentUser.Notes))
                    //{
                    //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //}
                    //else
                    //{
                    string message = LocRM.GetString("updateUser", culture) + ": " + "[" + username + "]";
                    //if (!password.Equals(currentUser.Password)) message += "\n" + "- " + LocRM.GetString("password", culture) + ": " + txtPasswordUserManage.Text;
                    // if (!clientId.Equals(currentUser.IdClient)) message += "\n" + "- " + LocRM.GetString("clientId", culture) + ": " + txtCLientUserManage.Text;
                    //if (!branchId.Equals(currentUser.IdBranch)) message += "\n" + "- " + LocRM.GetString("branch", culture) + ": " + cbBranchUserManage.Text;
                    //if (!note.Equals(currentUser.Notes)) message += "\n" + "- " + LocRM.GetString("note", culture) + ": " + txtNoteUserManage.Text;
                    message += " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateUser", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostUpdateUser(currentUserId, username, password, whId, clientId, note);
                        switch (result)
                        {
                            case 1:
                                LoadTabUserManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }

                    }
                    //}
                }
            }
        }

        private async void deleteUser()
        {
            string username = txtUsernameUserManage.Text;
            string password = txtPasswordUserManage.Text;
            string clientId = txtClientUserManage.Text;

            if (currentUserId != null && actionStatus == UPDATE_STATUS)
            {


                string message = LocRM.GetString("deleteUser", culture) + ": " + "[" + username + "]"
                                                                        + "\n" + "- " + LocRM.GetString("user", culture) + ": " + txtUsernameUserManage.Text
                                                                        + "\n" + "- " + LocRM.GetString("clientId", culture) + ": " + txtClientUserManage.Text
                                                                        + "\n" + "- " + lbWHUserManage.Text + ": " + cbWhUserManage.SelectedText.ToString()
                                                                        + " ?";
                DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("deleteUser", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    int result = await Utils.callApiPostDeleteUser(currentUserId);
                    switch (result)
                    {
                        case 1:
                            LoadTabUserManage();
                            break;
                        case -1:
                            //no link
                            break;

                        case 2:
                            // error
                            break;

                        default:
                            //show result
                            break;
                    }
                }
            }
        }

        private async void addNewUser()
        {
            string username = txtUsernameUserManage.Text;
            string password = txtPasswordUserManage.Text;
            string clientId = txtClientUserManage.Text;
            string note = txtNoteUserManage.Text;
            string whId = cbWhUserManage.SelectedValue.ToString();

            currentUserId = null;
            if (actionStatus != ADDNEW_STATUS)
            {
                txtUsernameUserManage.ReadOnly = false;
                dtpCreateDate.Value = DateTime.Now;
                dtpUpdateDate.Value = DateTime.Now;
                dtpCreateDate.Enabled = false;
                dtpUpdateDate.Enabled = false;
                txtUsernameUserManage.Text = "";
                txtPasswordUserManage.Text = "";
                txtClientUserManage.Text = "";
                txtNoteUserManage.Text = "";
                actionStatus = ADDNEW_STATUS;
            }
            else
            {
                if (username == "" || password == "" || clientId == "")
                {
                    string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
                    if (username == "") message += "\n" + "  " + "- " + LocRM.GetString("username", culture);
                    if (password == "") message += "\n" + "  " + "- " + LocRM.GetString("password", culture);
                    if (clientId == "") message += "\n" + "  " + "- " + LocRM.GetString("clientId", culture);
                    MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string message = LocRM.GetString("createNewUser", culture) + ": "
                                                                        + "\n" + "- " + LocRM.GetString("user", culture) + ": " + txtUsernameUserManage.Text
                                                                        + "\n" + "- " + LocRM.GetString("password", culture) + ": " + txtPasswordUserManage.Text
                                                                        + "\n" + "- " + LocRM.GetString("clientId", culture) + ": " + txtClientUserManage.Text
                                                                        + "\n" + "- " + LocRM.GetString("wh", culture) + ": " + cbWhUserManage.Text
                                                                        + " ?";
                    DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createNewUser", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int result = await Utils.callApiPostAddNewUser(username, password, whId, clientId, note);
                        switch (result)
                        {
                            case 1:
                                LoadTabUserManage();
                                break;
                            case -1:
                                //no link
                                break;

                            case 2:
                                // error
                                break;

                            default:
                                //show result
                                break;
                        }
                    }
                }
            }
        }

        private void LoadTabUserManage()
        {
            //load datagridview list user
            if (!File.Exists(@"User.xml"))
            {
                return;
            }
            xmlFile = XmlReader.Create(@"User.xml", new XmlReaderSettings());
            DataSet ds = new DataSet();
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            if (ds.Tables["Item"] == null) return;
            dataGridViewUserManage.DataSource = ds.Tables["Item"];

            dataGridViewUserManage.Columns[0].HeaderText = LocRM.GetString("accId", culture);
            dataGridViewUserManage.Columns[1].HeaderText = LocRM.GetString("username", culture);
            dataGridViewUserManage.Columns[2].HeaderText = LocRM.GetString("password", culture);
            dataGridViewUserManage.Columns[3].HeaderText = LocRM.GetString("createData", culture);
            dataGridViewUserManage.Columns[4].HeaderText = LocRM.GetString("updateData", culture);
            dataGridViewUserManage.Columns[5].HeaderText = LocRM.GetString("note", culture);
            dataGridViewUserManage.Columns[6].HeaderText = LocRM.GetString("syncStatus", culture);
            dataGridViewUserManage.Columns[7].HeaderText = LocRM.GetString("whId", culture);
            dataGridViewUserManage.Columns[8].HeaderText = LocRM.GetString("clientId", culture);

            dataGridViewUserManage.Columns[0].Visible = false;
            dataGridViewUserManage.Columns[6].Visible = false;
            foreach (DataGridViewRow row in dataGridViewUserManage.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }
        }
        #endregion

        #region BRANCH REGION
        //private void loadTabBranchManage()
        //{
        //    //load datagridview list branch
        //    xmlFile = XmlReader.Create(@"Branch.xml", new XmlReaderSettings());
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(xmlFile);
        //    xmlFile.Dispose();
        //    xmlFile.Close();
        //    dataGridViewBranchManage.DataSource = ds.Tables["Item"];
        //    //dataGridViewBranchManage.DataSource = Utils.entityWH.BRANCHes.ToList();
        //    dataGridViewBranchManage.Columns[0].HeaderText = LocRM.GetString("branchId", culture);
        //    dataGridViewBranchManage.Columns[1].HeaderText = LocRM.GetString("branchName", culture);
        //    dataGridViewBranchManage.Columns[2].HeaderText = LocRM.GetString("address", culture);
        //    dataGridViewBranchManage.Columns[3].HeaderText = LocRM.GetString("area", culture);
        //    dataGridViewBranchManage.Columns[4].HeaderText = LocRM.GetString("phone", culture);
        //    dataGridViewBranchManage.Columns[5].HeaderText = LocRM.GetString("note", culture);
        //    dataGridViewBranchManage.Columns[6].HeaderText = LocRM.GetString("syncStatus", culture);
        //    dataGridViewBranchManage.Columns[7].HeaderText = LocRM.GetString("linkWS", culture);
        //    dataGridViewBranchManage.Columns[8].HeaderText = LocRM.GetString("isHO", culture);

        //    dataGridViewBranchManage.Columns[6].Visible = false;
        //    dataGridViewBranchManage.Columns[7].Visible = false;
        //    dataGridViewBranchManage.Columns[8].Visible = false;
        //    foreach (DataGridViewRow row in dataGridViewBranchManage.Rows)
        //    {
        //        row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
        //    }
        //    //load combobox list branch
        //    //cbBranchUserManage.DataSource = Utils.entityWH.BRANCHes.ToList();
        //    // cbBranchUserManage.DisplayMember = "Name";
        //    // cbBranchUserManage.ValueMember = "Id";
        //}
        //private async void deleteBranch()
        //{
        //    string branchName = txtBranchNameBranchManage.Text;
        //    string address = txtAddressBranchManage.Text;
        //    string area = txtAreaBranchManage.Text;
        //    string phone = txtPhoneBranchManage.Text;
        //    string note = txtNoteBranchManage.Text;
        //    string linkWS = "";


        //    if (currentBranchId != null && txtBranchNameBranchManage.ReadOnly)
        //    {


        //        string message = LocRM.GetString("deleteBranch", culture) + ": " + "[" + branchName + "]"
        //                                                                + "\n" + "- " + lbAddressBranch.Text + ": " + txtAddressBranchManage.Text
        //                                                                + "\n" + "- " + lbAreaBranch.Text + ": " + txtAreaBranchManage.Text
        //                                                                + "\n" + "- " + lbPhoneBranch.Text + ": " + txtPhoneBranchManage.Text
        //                                                                + " ?";
        //        DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("deleteBranch", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //        if (dialogResult == DialogResult.Yes)
        //        {

        //            int result = await Utils.callApiPostDeleteBranch(currentBranchId);
        //            switch (result)
        //            {
        //                case 1:
        //                    loadTabBranchManage();
        //                    break;
        //                case -1:
        //                    //no link
        //                    break;

        //                case 2:
        //                    // error
        //                    break;

        //                default:
        //                    //show result
        //                    break;
        //            }
        //        }
        //    }
        //}

        //private async void updateBranch()
        //{
        //    string branchName = txtBranchNameBranchManage.Text;
        //    string address = txtAddressBranchManage.Text;
        //    string area = txtAreaBranchManage.Text;
        //    string phone = txtPhoneBranchManage.Text;
        //    string note = txtNoteBranchManage.Text;
        //    string linkWS = "http";

        //    if (!string.IsNullOrEmpty(currentBranchId) && txtBranchNameBranchManage.ReadOnly)
        //    {
        //        if (address == "" || area == "" || phone == "")
        //        {
        //            string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
        //            if (address == "") message += "\n" + "  " + "- " + lbAddressBranch.Text;
        //            if (area == "") message += "\n" + "  " + "- " + lbAreaBranch.Text;
        //            if (phone == "") message += "\n" + "  " + "- " + lbPhoneBranch.Text;
        //            MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        }
        //        else
        //        {
        //            //if (address.Equals(currentBranch.Address) && area.Equals(currentBranch.Area) && phone.Equals(currentBranch.Phone) && note.Equals(currentBranch.Note))
        //            //{
        //            //    MessageBox.Show(LocRM.GetString("noChange", culture), LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            //}
        //            //else
        //            //{
        //            string message = LocRM.GetString("updateBranch", culture) + ": " + "[" + branchName + "]";
        //            // if (!address.Equals(currentBranch.Address)) message += "\n" + "- " + lbAddressBranch.Text + ": " + txtAddressBranchManage.Text;
        //            // if (!area.Equals(currentBranch.Area)) message += "\n" + "- " + lbAreaBranch.Text + ": " + txtAreaBranchManage.Text;
        //            // if (!phone.Equals(currentBranch.Phone)) message += "\n" + "- " + lbPhoneBranch.Text + ": " + txtPhoneBranchManage.Text;
        //            // if (!note.Equals(currentBranch.Note)) message += "\n" + "- " + lbNoteBranch + ": " + txtNoteBranchManage.Text;
        //            message += " ?";
        //            DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("updateBranch", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //            if (dialogResult == DialogResult.Yes)
        //            {
        //                int result = await Utils.callApiPostUpdateBranch(currentBranchId, branchName, address, area, phone, note, linkWS);
        //                switch (result)
        //                {
        //                    case 1:
        //                        loadTabBranchManage();
        //                        break;
        //                    case -1:
        //                        //no link
        //                        break;

        //                    case 2:
        //                        // error
        //                        break;

        //                    default:
        //                        //show result
        //                        break;
        //                }

        //            }
        //            // }
        //        }
        //    }
        //}

        //private async void addNewBranch()
        //{
        //    string branchName = txtBranchNameBranchManage.Text;
        //    string address = txtAddressBranchManage.Text;
        //    string area = txtAreaBranchManage.Text;
        //    string phone = txtPhoneBranchManage.Text;
        //    string note = txtNoteBranchManage.Text;
        //    string linkWS = "http";

        //    currentBranchId = null;
        //    if (txtBranchNameBranchManage.ReadOnly)
        //    {
        //        txtBranchNameBranchManage.ReadOnly = false;
        //        txtBranchNameBranchManage.Text = "";
        //        txtAddressBranchManage.Text = "";
        //        txtAreaBranchManage.Text = "";
        //        txtPhoneBranchManage.Text = "";
        //        txtNoteBranchManage.Text = "";
        //    }
        //    else
        //    {
        //        if (branchName == "" || address == "" || area == "" || phone == "")
        //        {
        //            string message = LocRM.GetString("belowFieldCantBeEmpty", culture);
        //            if (branchName == "") message += "\n" + "  " + "- " + lbBranchName.Text;
        //            if (address == "") message += "\n" + "  " + "- " + lbAddressBranch.Text;
        //            if (area == "") message += "\n" + "  " + "- " + lbAreaBranch.Text;
        //            if (phone == "") message += "\n" + "  " + "- " + lbPhoneBranch.Text;
        //            MessageBox.Show(message, LocRM.GetString("warning", culture), MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        }
        //        else
        //        {
        //            string message = LocRM.GetString("createNewBranch", culture) + ": "
        //                                                                + "\n" + "- " + lbBranchName.Text + ": " + txtBranchNameBranchManage.Text
        //                                                                + "\n" + "- " + lbAddressBranch.Text + ": " + txtAddressBranchManage.Text
        //                                                                + "\n" + "- " + lbAreaBranch.Text + ": " + txtAreaBranchManage.Text
        //                                                                + "\n" + "- " + lbPhoneBranch.Text + ": " + txtPhoneBranchManage.Text
        //                                                                + " ?";
        //            DialogResult dialogResult = MessageBox.Show(message, LocRM.GetString("createNewBranch", culture), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //            if (dialogResult == DialogResult.Yes)
        //            {
        //                int result = await Utils.callApiPostAddNewBranch(branchName, address, area, phone, note, linkWS);
        //                switch (result)
        //                {
        //                    case 1:
        //                        loadTabBranchManage();
        //                        break;
        //                    case -1:
        //                        //no link
        //                        break;

        //                    case 2:
        //                        // error
        //                        break;

        //                    default:
        //                        //show result
        //                        break;
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion

        #region UTILS REGION
        private void loadComboBoxIsHO(ref ComboBox cb)
        {
            cb.DataSource = listRole.ToList();
            cb.DisplayMember = "Key";
            cb.ValueMember = "Value";
        }
        private void loadComboBoxWH(ref ComboBox cb)
        {
            if (File.Exists(@"Warehouse.xml"))
            {
                xmlFile2 = XmlReader.Create(@"Warehouse.xml", new XmlReaderSettings());
                DataSet dsWH = new DataSet();
                dsWH.ReadXml(xmlFile2);
                cb.DataSource = dsWH.Tables["Item"]; ;
                cb.DisplayMember = "Name";
                cb.ValueMember = "Id";
                xmlFile2.Dispose();
                xmlFile2.Close();
            }
            else
            {

            }
        }
        private void loadComboBoxArea(ref ComboBox cb)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = listArea.ToList();
            cb.DataSource = bs;
        }

        private void loadComboBoxProduct(ref ComboBox cb)
        {
            cb.DataSource = listTypeProduct.ToList();
            cb.DisplayMember = "Key";
            cb.ValueMember = "Value";
        }

        private async void focusDefaulTab()
        {
            tabMain.SelectedTab = tbUserManage;
            loadComboBoxWH(ref cbWhUserManage);
            int result = await Utils.callApiGetUser();
            switch (result)
            {
                case 1: //success case
                    LoadTabUserManage();
                    break;
                case -1: //null url
                    MessageBox.Show(LocRM.GetString("errorNoGetUserLink", culture), LocRM.GetString("callApiError", culture), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case -2: // loi reponse null

                    break;
                case -3: //result not a number

                    break;
            }
        }

        public void updateLanguage(String language)
        {
            culture = CultureInfo.CreateSpecificCulture(language);
            LocRM = new ResourceManager("WHManagement01.Language", typeof(LoginForm).Assembly);
            //Branch manage Tab
            //tabBranchManage.Text = LocRM.GetString("branchManagement", culture);
            //lbTitleBranchManage.Text = LocRM.GetString("branchManagement", culture);
            //lbBranchName.Text = LocRM.GetString("branchName", culture);
            //lbAreaBranch.Text = LocRM.GetString("area", culture);
            //lbAddressBranch.Text = LocRM.GetString("address", culture);
            //lbPhoneBranch.Text = LocRM.GetString("phone", culture);
            //lbNoteBranch.Text = LocRM.GetString("note", culture);

            //Warehouse manage Tab
            tabWHManage.Text = LocRM.GetString("whManage", culture);
            lbTitleWHManage.Text = LocRM.GetString("whManage", culture);
            lbNameWHManage.Text = LocRM.GetString("whName", culture);
            lbWhIdManage.Text = LocRM.GetString("whId", culture);
            lbAddressWHManage.Text = LocRM.GetString("address", culture);
            lbPhoneWHManage.Text = LocRM.GetString("phone", culture);
            lbNoteWHManage.Text = LocRM.GetString("note", culture);
            lbPalletWHManage.Text = LocRM.GetString("quantityPallet", culture);
            lbAreaWHManage.Text = LocRM.GetString("area", culture);
            lbLinkWSWHManage.Text = LocRM.GetString("linkWS", culture);
            lbIsHOWhManage.Text = LocRM.GetString("typeAccount", culture);

            //Truck Manage Tab
            tbTruckManage.Text = LocRM.GetString("truckManage", culture);
            lbTruckManage.Text = LocRM.GetString("truckManage", culture);
            lbLicensePlateTruckManage.Text = LocRM.GetString("numberPlate", culture);
            //lbDriverTruckManage.Text = LocRM.GetString("manageCard", culture);
            lbStatusTruckManage.Text = LocRM.GetString("truckDes", culture);
            lbWhTruckManage.Text = LocRM.GetString("wh", culture);
            lbDriverTruckManage.Text = LocRM.GetString("driverName", culture);
            lbOwnerTruckManage.Text = LocRM.GetString("truckOwner", culture);
            lbIdCardTruckManage.Text = LocRM.GetString("idCard", culture);
            lbAddressTruck.Text = LocRM.GetString("address", culture);
            lbPhoneTruck.Text = LocRM.GetString("phone", culture);
            lbNoteTruck.Text = LocRM.GetString("note", culture);
            lbBeginDateTruckManage.Text = LocRM.GetString("beginDate", culture);
            lbEndDateTruckManage.Text = LocRM.GetString("endDate", culture);
            lbStatusTruckManage.Text = LocRM.GetString("truckStatus", culture);

            //Palet Manage Tab
            lbGoodsManage.Text = LocRM.GetString("goodsManageWH", culture);
            tabPaletManage.Text = LocRM.GetString("goodsManageWH", culture);
            lbSerialPaletManage.Text = LocRM.GetString("serial", culture);
            lbWhPaletManage.Text = LocRM.GetString("wh", culture);
            lbStatusPaletManage.Text = LocRM.GetString("status", culture);
            //lbBranchGoods.Text = LocRM.GetString("branch", culture);
            //lbImportCodeGoods.Text = LocRM.GetString("importCode", culture);
            // lbExportCodeGoods.Text = LocRM.GetString("exportCode", culture);
            // lbStatusGoods.Text = LocRM.GetString("status", culture);
            // lbNoteGoods.Text = LocRM.GetString("note", culture);
            lbBeginDatePaletManage.Text = LocRM.GetString("beginDate", culture);
            lbEndDatePaletManage.Text = LocRM.GetString("endDate", culture);


            //Import Management tab
            tabImportManagement.Text = LocRM.GetString("importManagement", culture);

            //Export Management tab
            tabExportManagement.Text = LocRM.GetString("exportManagement", culture);

            //User Management tab
            lbTitleUserManage.Text = LocRM.GetString("userManagement", culture);
            tbUserManage.Text = LocRM.GetString("userManagement", culture);
            lbUsernameUserManage.Text = LocRM.GetString("username", culture);
            lbPasswordUserManage.Text = LocRM.GetString("password", culture);
            lbWHUserManage.Text = LocRM.GetString("wh", culture);
            lbClientIdUserManage.Text = LocRM.GetString("clientId", culture);
            lbNoteUserManage.Text = LocRM.GetString("note", culture);
            lbCreateDateUserManage.Text = LocRM.GetString("createData", culture);
            lbUpdateDateUserManage.Text = LocRM.GetString("updateData", culture);
        }
        #endregion




    }
}
