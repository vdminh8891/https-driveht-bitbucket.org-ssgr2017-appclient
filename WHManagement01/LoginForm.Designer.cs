﻿namespace WHManagement01
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLoginForm = new System.Windows.Forms.Label();
            this.lbClientId = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtClientId = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lbWHLogin = new System.Windows.Forms.Label();
            this.txtWHIdLogin = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbLoginForm
            // 
            this.lbLoginForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoginForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbLoginForm.Location = new System.Drawing.Point(72, 34);
            this.lbLoginForm.Name = "lbLoginForm";
            this.lbLoginForm.Size = new System.Drawing.Size(164, 20);
            this.lbLoginForm.TabIndex = 0;
            this.lbLoginForm.Text = "Đăng Nhập Hệ Thống";
            this.lbLoginForm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbClientId
            // 
            this.lbClientId.AutoSize = true;
            this.lbClientId.Location = new System.Drawing.Point(11, 110);
            this.lbClientId.Name = "lbClientId";
            this.lbClientId.Size = new System.Drawing.Size(64, 13);
            this.lbClientId.TabIndex = 2;
            this.lbClientId.Text = "ID Máy trạm";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Location = new System.Drawing.Point(11, 146);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(81, 13);
            this.lbUsername.TabIndex = 3;
            this.lbUsername.Text = "Tên đăng nhập";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Location = new System.Drawing.Point(11, 184);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(52, 13);
            this.lbPassword.TabIndex = 4;
            this.lbPassword.Text = "Mật khẩu";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(61, 220);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 11;
            this.btnLogin.Text = "Đăng nhập";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(159, 220);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtClientId
            // 
            this.txtClientId.Location = new System.Drawing.Point(94, 107);
            this.txtClientId.Name = "txtClientId";
            this.txtClientId.Size = new System.Drawing.Size(173, 20);
            this.txtClientId.TabIndex = 8;
            this.txtClientId.Text = "1";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(94, 144);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(173, 20);
            this.txtUsername.TabIndex = 9;
            this.txtUsername.Text = "hoangt";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(94, 179);
            this.txtPassword.MaxLength = 5;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(173, 20);
            this.txtPassword.TabIndex = 10;
            this.txtPassword.Text = "hoangt";
            // 
            // lbWHLogin
            // 
            this.lbWHLogin.AutoSize = true;
            this.lbWHLogin.Location = new System.Drawing.Point(11, 78);
            this.lbWHLogin.Name = "lbWHLogin";
            this.lbWHLogin.Size = new System.Drawing.Size(64, 13);
            this.lbWHLogin.TabIndex = 1;
            this.lbWHLogin.Text = "Mã nhà kho";
            // 
            // txtWHIdLogin
            // 
            this.txtWHIdLogin.Location = new System.Drawing.Point(94, 75);
            this.txtWHIdLogin.Name = "txtWHIdLogin";
            this.txtWHIdLogin.Size = new System.Drawing.Size(173, 20);
            this.txtWHIdLogin.TabIndex = 7;
            this.txtWHIdLogin.Text = "1";
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txtWHIdLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.txtClientId);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.lbClientId);
            this.Controls.Add(this.lbWHLogin);
            this.Controls.Add(this.lbLoginForm);
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbLoginForm;
        private System.Windows.Forms.Label lbClientId;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtClientId;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lbWHLogin;
        private System.Windows.Forms.TextBox txtWHIdLogin;
    }
}

