﻿namespace WHManagement01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pnOption = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabBranchManage = new System.Windows.Forms.TabPage();
            this.dataGridViewBranchManage = new System.Windows.Forms.DataGridView();
            this.pnBranchManage = new System.Windows.Forms.Panel();
            this.txtNoteBranchManage = new System.Windows.Forms.TextBox();
            this.txtPhoneBranchManage = new System.Windows.Forms.TextBox();
            this.txtAddressBranchManage = new System.Windows.Forms.TextBox();
            this.lbNoteBranch = new System.Windows.Forms.Label();
            this.lbPhoneBranch = new System.Windows.Forms.Label();
            this.lbAddressBranch = new System.Windows.Forms.Label();
            this.txtAreaBranchManage = new System.Windows.Forms.TextBox();
            this.txtBranchNameBranchManage = new System.Windows.Forms.TextBox();
            this.lbAreaBranch = new System.Windows.Forms.Label();
            this.lbBranchName = new System.Windows.Forms.Label();
            this.lbTitleBranchManage = new System.Windows.Forms.Label();
            this.tabWHManage = new System.Windows.Forms.TabPage();
            this.dataGridViewWHManage = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbIsHOWhManage = new System.Windows.Forms.ComboBox();
            this.lbIsHOWhManage = new System.Windows.Forms.Label();
            this.cbAreaWHManage = new System.Windows.Forms.ComboBox();
            this.txtLinkWSWHManage = new System.Windows.Forms.TextBox();
            this.lbLinkWSWHManage = new System.Windows.Forms.Label();
            this.lbAreaWHManage = new System.Windows.Forms.Label();
            this.txtWhIdWhManage = new System.Windows.Forms.TextBox();
            this.txtPalletWHManage = new System.Windows.Forms.TextBox();
            this.lbPalletWHManage = new System.Windows.Forms.Label();
            this.txtNoteWHManage = new System.Windows.Forms.TextBox();
            this.txtPhoneWHManage = new System.Windows.Forms.TextBox();
            this.txtAddressWHManage = new System.Windows.Forms.TextBox();
            this.lbNoteWHManage = new System.Windows.Forms.Label();
            this.lbPhoneWHManage = new System.Windows.Forms.Label();
            this.lbAddressWHManage = new System.Windows.Forms.Label();
            this.txtNameWHManage = new System.Windows.Forms.TextBox();
            this.lbWhIdManage = new System.Windows.Forms.Label();
            this.lbNameWHManage = new System.Windows.Forms.Label();
            this.lbTitleWHManage = new System.Windows.Forms.Label();
            this.tbTruckManage = new System.Windows.Forms.TabPage();
            this.dataGridViewTruckManage = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtIdCardTruckManage = new System.Windows.Forms.TextBox();
            this.lbIdCardTruckManage = new System.Windows.Forms.Label();
            this.txtStatusTruckManage = new System.Windows.Forms.TextBox();
            this.lbStatusTruckManage = new System.Windows.Forms.Label();
            this.dtpEndDateTruckManage = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginDateTruckManage = new System.Windows.Forms.DateTimePicker();
            this.lbEndDateTruckManage = new System.Windows.Forms.Label();
            this.lbBeginDateTruckManage = new System.Windows.Forms.Label();
            this.txtNoteTruckManage = new System.Windows.Forms.TextBox();
            this.lbNoteTruck = new System.Windows.Forms.Label();
            this.txtPhoneTruckManage = new System.Windows.Forms.TextBox();
            this.txtAddressTruckManage = new System.Windows.Forms.TextBox();
            this.txtOwnerTruckManage = new System.Windows.Forms.TextBox();
            this.lbPhoneTruck = new System.Windows.Forms.Label();
            this.lbAddressTruck = new System.Windows.Forms.Label();
            this.lbOwnerTruckManage = new System.Windows.Forms.Label();
            this.cbWhTruckManage = new System.Windows.Forms.ComboBox();
            this.txtDriverTruckManage = new System.Windows.Forms.TextBox();
            this.txtLicensePlateTruckManages = new System.Windows.Forms.TextBox();
            this.lbWhTruckManage = new System.Windows.Forms.Label();
            this.lbDriverTruckManage = new System.Windows.Forms.Label();
            this.lbLicensePlateTruckManage = new System.Windows.Forms.Label();
            this.lbTruckManage = new System.Windows.Forms.Label();
            this.tabPaletManage = new System.Windows.Forms.TabPage();
            this.dataGridViewPaletManage = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbWhPaletManage = new System.Windows.Forms.ComboBox();
            this.dtpEndDatePaletManage = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginDatePaletManage = new System.Windows.Forms.DateTimePicker();
            this.lbEndDatePaletManage = new System.Windows.Forms.Label();
            this.lbBeginDatePaletManage = new System.Windows.Forms.Label();
            this.txtStatusPaletManage = new System.Windows.Forms.TextBox();
            this.txtSerialPaletManage = new System.Windows.Forms.TextBox();
            this.lbStatusPaletManage = new System.Windows.Forms.Label();
            this.lbWhPaletManage = new System.Windows.Forms.Label();
            this.lbSerialPaletManage = new System.Windows.Forms.Label();
            this.lbGoodsManage = new System.Windows.Forms.Label();
            this.tabImportManagement = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.subTabImportManage = new System.Windows.Forms.TabControl();
            this.tabImportImportManage = new System.Windows.Forms.TabPage();
            this.label33 = new System.Windows.Forms.Label();
            this.btnUpdateImport = new System.Windows.Forms.Button();
            this.btnAddNewImport = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtUserIdImportManage = new System.Windows.Forms.TextBox();
            this.cbWhCurrentImportManage = new System.Windows.Forms.ComboBox();
            this.lbCurrentWhImportManage = new System.Windows.Forms.Label();
            this.txtUsernameImportManage = new System.Windows.Forms.TextBox();
            this.lbCreateUserImportManage = new System.Windows.Forms.Label();
            this.txtNoteImportManage = new System.Windows.Forms.TextBox();
            this.lbNoteImportManage = new System.Windows.Forms.Label();
            this.lbQuantityPaletImportManage = new System.Windows.Forms.Label();
            this.cbProduceImportManage = new System.Windows.Forms.ComboBox();
            this.txtQuantityImportManage = new System.Windows.Forms.TextBox();
            this.dtpUpdateDateImportManage = new System.Windows.Forms.DateTimePicker();
            this.lbUpdateDateImportManage = new System.Windows.Forms.Label();
            this.cbWhExImportManage = new System.Windows.Forms.ComboBox();
            this.lbSourceWhImportManage = new System.Windows.Forms.Label();
            this.dtpCreateDateImportManage = new System.Windows.Forms.DateTimePicker();
            this.lbTypeProductImportManage = new System.Windows.Forms.Label();
            this.lbCreateDateImportManage = new System.Windows.Forms.Label();
            this.txtSerialImportManage = new System.Windows.Forms.TextBox();
            this.lbSerialImportImportManage = new System.Windows.Forms.Label();
            this.lbNameImportManage = new System.Windows.Forms.Label();
            this.txtNameImportManage = new System.Windows.Forms.TextBox();
            this.tabDetailImportImportManage = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnUpdateDetailImportPalet = new System.Windows.Forms.Button();
            this.btnAddNewDetailImportPalet = new System.Windows.Forms.Button();
            this.dataGridViewDetailImportAndPalet = new System.Windows.Forms.DataGridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtNameBelongImport = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtStatusPlateImportManage = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.dtpEndDatePaletImportManage = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.dtpBeginDatePaletImportManage = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.cbWhDetailTabImportManage = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNoteDetailImportImportManage = new System.Windows.Forms.TextBox();
            this.dtpCreateDateSubTabImportManage = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLincensePlateImportManage = new System.Windows.Forms.TextBox();
            this.lbPlateDetailImportImportManage = new System.Windows.Forms.Label();
            this.lbSerialDetailImportImportManage = new System.Windows.Forms.Label();
            this.txtSerialPaletImportManage = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dataGridViewImportImportManage = new System.Windows.Forms.DataGridView();
            this.tabExportManagement = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabInfoExport = new System.Windows.Forms.TabPage();
            this.label46 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnUpdateExport = new System.Windows.Forms.Button();
            this.btnAddNewExport = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.cbImWhExportManage = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCreateUserExportManage = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtNoteExportManage = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cbTypeProductExportManage = new System.Windows.Forms.ComboBox();
            this.txtQuantityPaletExportManage = new System.Windows.Forms.TextBox();
            this.dtpUpdateDateExportManage = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.cbCurrentWhExportManage = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dtpCreateDateExportManage = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSerialExportManage = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lbNameExportManage = new System.Windows.Forms.Label();
            this.txtNameExportExportManage = new System.Windows.Forms.TextBox();
            this.tabInfoDetailExport = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnUpdateDetailExport = new System.Windows.Forms.Button();
            this.btnAddNewDetailExport = new System.Windows.Forms.Button();
            this.dataGridViewDetailExportAndPalet = new System.Windows.Forms.DataGridView();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtNameBelongExport = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtStatusPaletExport = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.dtpEndDatePaletExport = new System.Windows.Forms.DateTimePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.dtpBeginDatePaletExport = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.cbCurentWhPaletExport = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtNoteSubTabExport = new System.Windows.Forms.TextBox();
            this.dtpCreateDateSubTabExport = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtLicensePlateExportManage = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSerialPaletExportManage = new System.Windows.Forms.TextBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dataGridViewPaletExportManage = new System.Windows.Forms.DataGridView();
            this.btnSearchPaletExportManage = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.dataGridViewListExport = new System.Windows.Forms.DataGridView();
            this.tbUserManage = new System.Windows.Forms.TabPage();
            this.dataGridViewUserManage = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtpUpdateDate = new System.Windows.Forms.DateTimePicker();
            this.dtpCreateDate = new System.Windows.Forms.DateTimePicker();
            this.lbUpdateDateUserManage = new System.Windows.Forms.Label();
            this.lbCreateDateUserManage = new System.Windows.Forms.Label();
            this.txtNoteUserManage = new System.Windows.Forms.TextBox();
            this.txtClientUserManage = new System.Windows.Forms.TextBox();
            this.lbNoteUserManage = new System.Windows.Forms.Label();
            this.lbClientIdUserManage = new System.Windows.Forms.Label();
            this.cbWhUserManage = new System.Windows.Forms.ComboBox();
            this.txtPasswordUserManage = new System.Windows.Forms.TextBox();
            this.txtUsernameUserManage = new System.Windows.Forms.TextBox();
            this.lbWHUserManage = new System.Windows.Forms.Label();
            this.lbPasswordUserManage = new System.Windows.Forms.Label();
            this.lbUsernameUserManage = new System.Windows.Forms.Label();
            this.lbTitleUserManage = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.pnOption.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabBranchManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBranchManage)).BeginInit();
            this.pnBranchManage.SuspendLayout();
            this.tabWHManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWHManage)).BeginInit();
            this.panel4.SuspendLayout();
            this.tbTruckManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTruckManage)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPaletManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaletManage)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabImportManagement.SuspendLayout();
            this.panel6.SuspendLayout();
            this.subTabImportManage.SuspendLayout();
            this.tabImportImportManage.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabDetailImportImportManage.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailImportAndPalet)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewImportImportManage)).BeginInit();
            this.tabExportManagement.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabInfoExport.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabInfoDetailExport.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailExportAndPalet)).BeginInit();
            this.panel14.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaletExportManage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListExport)).BeginInit();
            this.tbUserManage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserManage)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // pnOption
            // 
            this.pnOption.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnOption.Controls.Add(this.label1);
            this.pnOption.Controls.Add(this.btnDelete);
            this.pnOption.Controls.Add(this.btnSave);
            this.pnOption.Controls.Add(this.btnNew);
            this.pnOption.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnOption.Location = new System.Drawing.Point(0, 464);
            this.pnOption.Margin = new System.Windows.Forms.Padding(0);
            this.pnOption.Name = "pnOption";
            this.pnOption.Size = new System.Drawing.Size(947, 75);
            this.pnOption.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.DarkCyan;
            this.label1.Location = new System.Drawing.Point(868, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Version 2.0";
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(518, 8);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 60);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(418, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 60);
            this.btnSave.TabIndex = 1;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.Location = new System.Drawing.Point(316, 8);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(60, 60);
            this.btnNew.TabIndex = 0;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabBranchManage);
            this.tabMain.Controls.Add(this.tabWHManage);
            this.tabMain.Controls.Add(this.tbTruckManage);
            this.tabMain.Controls.Add(this.tabPaletManage);
            this.tabMain.Controls.Add(this.tabImportManagement);
            this.tabMain.Controls.Add(this.tabExportManagement);
            this.tabMain.Controls.Add(this.tbUserManage);
            this.tabMain.Location = new System.Drawing.Point(0, 1);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(947, 465);
            this.tabMain.TabIndex = 4;
            this.tabMain.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabMain_Selected);
            // 
            // tabBranchManage
            // 
            this.tabBranchManage.Controls.Add(this.dataGridViewBranchManage);
            this.tabBranchManage.Controls.Add(this.pnBranchManage);
            this.tabBranchManage.Location = new System.Drawing.Point(4, 22);
            this.tabBranchManage.Name = "tabBranchManage";
            this.tabBranchManage.Padding = new System.Windows.Forms.Padding(3);
            this.tabBranchManage.Size = new System.Drawing.Size(939, 439);
            this.tabBranchManage.TabIndex = 0;
            this.tabBranchManage.Text = "Quản lý chi nhánh";
            this.tabBranchManage.UseVisualStyleBackColor = true;
            // 
            // dataGridViewBranchManage
            // 
            this.dataGridViewBranchManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBranchManage.Location = new System.Drawing.Point(-4, 148);
            this.dataGridViewBranchManage.Name = "dataGridViewBranchManage";
            this.dataGridViewBranchManage.ReadOnly = true;
            this.dataGridViewBranchManage.Size = new System.Drawing.Size(947, 295);
            this.dataGridViewBranchManage.TabIndex = 4;
            this.dataGridViewBranchManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBranchManage_CellClick);
            // 
            // pnBranchManage
            // 
            this.pnBranchManage.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnBranchManage.Controls.Add(this.txtNoteBranchManage);
            this.pnBranchManage.Controls.Add(this.txtPhoneBranchManage);
            this.pnBranchManage.Controls.Add(this.txtAddressBranchManage);
            this.pnBranchManage.Controls.Add(this.lbNoteBranch);
            this.pnBranchManage.Controls.Add(this.lbPhoneBranch);
            this.pnBranchManage.Controls.Add(this.lbAddressBranch);
            this.pnBranchManage.Controls.Add(this.txtAreaBranchManage);
            this.pnBranchManage.Controls.Add(this.txtBranchNameBranchManage);
            this.pnBranchManage.Controls.Add(this.lbAreaBranch);
            this.pnBranchManage.Controls.Add(this.lbBranchName);
            this.pnBranchManage.Controls.Add(this.lbTitleBranchManage);
            this.pnBranchManage.Location = new System.Drawing.Point(0, 0);
            this.pnBranchManage.Name = "pnBranchManage";
            this.pnBranchManage.Size = new System.Drawing.Size(947, 146);
            this.pnBranchManage.TabIndex = 3;
            // 
            // txtNoteBranchManage
            // 
            this.txtNoteBranchManage.Location = new System.Drawing.Point(373, 34);
            this.txtNoteBranchManage.Name = "txtNoteBranchManage";
            this.txtNoteBranchManage.Size = new System.Drawing.Size(167, 20);
            this.txtNoteBranchManage.TabIndex = 14;
            // 
            // txtPhoneBranchManage
            // 
            this.txtPhoneBranchManage.Location = new System.Drawing.Point(109, 119);
            this.txtPhoneBranchManage.Name = "txtPhoneBranchManage";
            this.txtPhoneBranchManage.Size = new System.Drawing.Size(170, 20);
            this.txtPhoneBranchManage.TabIndex = 13;
            // 
            // txtAddressBranchManage
            // 
            this.txtAddressBranchManage.Location = new System.Drawing.Point(109, 91);
            this.txtAddressBranchManage.Name = "txtAddressBranchManage";
            this.txtAddressBranchManage.Size = new System.Drawing.Size(170, 20);
            this.txtAddressBranchManage.TabIndex = 12;
            // 
            // lbNoteBranch
            // 
            this.lbNoteBranch.Location = new System.Drawing.Point(321, 38);
            this.lbNoteBranch.Name = "lbNoteBranch";
            this.lbNoteBranch.Size = new System.Drawing.Size(44, 13);
            this.lbNoteBranch.TabIndex = 11;
            this.lbNoteBranch.Text = "Ghi chú";
            this.lbNoteBranch.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPhoneBranch
            // 
            this.lbPhoneBranch.Location = new System.Drawing.Point(48, 123);
            this.lbPhoneBranch.Name = "lbPhoneBranch";
            this.lbPhoneBranch.Size = new System.Drawing.Size(55, 13);
            this.lbPhoneBranch.TabIndex = 10;
            this.lbPhoneBranch.Text = "Điện thoại";
            this.lbPhoneBranch.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbAddressBranch
            // 
            this.lbAddressBranch.Location = new System.Drawing.Point(63, 95);
            this.lbAddressBranch.Name = "lbAddressBranch";
            this.lbAddressBranch.Size = new System.Drawing.Size(40, 13);
            this.lbAddressBranch.TabIndex = 9;
            this.lbAddressBranch.Text = "Địa chỉ";
            this.lbAddressBranch.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAreaBranchManage
            // 
            this.txtAreaBranchManage.Location = new System.Drawing.Point(109, 63);
            this.txtAreaBranchManage.Name = "txtAreaBranchManage";
            this.txtAreaBranchManage.Size = new System.Drawing.Size(170, 20);
            this.txtAreaBranchManage.TabIndex = 7;
            // 
            // txtBranchNameBranchManage
            // 
            this.txtBranchNameBranchManage.Location = new System.Drawing.Point(109, 34);
            this.txtBranchNameBranchManage.Name = "txtBranchNameBranchManage";
            this.txtBranchNameBranchManage.Size = new System.Drawing.Size(170, 20);
            this.txtBranchNameBranchManage.TabIndex = 6;
            // 
            // lbAreaBranch
            // 
            this.lbAreaBranch.Location = new System.Drawing.Point(56, 67);
            this.lbAreaBranch.Name = "lbAreaBranch";
            this.lbAreaBranch.Size = new System.Drawing.Size(47, 13);
            this.lbAreaBranch.TabIndex = 4;
            this.lbAreaBranch.Text = "Khu vực";
            this.lbAreaBranch.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbBranchName
            // 
            this.lbBranchName.Location = new System.Drawing.Point(27, 37);
            this.lbBranchName.Name = "lbBranchName";
            this.lbBranchName.Size = new System.Drawing.Size(76, 13);
            this.lbBranchName.TabIndex = 2;
            this.lbBranchName.Text = "Tên chi nhánh";
            this.lbBranchName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTitleBranchManage
            // 
            this.lbTitleBranchManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitleBranchManage.Location = new System.Drawing.Point(13, 4);
            this.lbTitleBranchManage.Name = "lbTitleBranchManage";
            this.lbTitleBranchManage.Size = new System.Drawing.Size(110, 13);
            this.lbTitleBranchManage.TabIndex = 0;
            this.lbTitleBranchManage.Text = "Quản lý chi nhánh";
            // 
            // tabWHManage
            // 
            this.tabWHManage.Controls.Add(this.dataGridViewWHManage);
            this.tabWHManage.Controls.Add(this.panel4);
            this.tabWHManage.Location = new System.Drawing.Point(4, 22);
            this.tabWHManage.Name = "tabWHManage";
            this.tabWHManage.Size = new System.Drawing.Size(939, 439);
            this.tabWHManage.TabIndex = 6;
            this.tabWHManage.Text = "Quản lý kho hàng";
            this.tabWHManage.UseVisualStyleBackColor = true;
            // 
            // dataGridViewWHManage
            // 
            this.dataGridViewWHManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWHManage.Location = new System.Drawing.Point(-6, 146);
            this.dataGridViewWHManage.Name = "dataGridViewWHManage";
            this.dataGridViewWHManage.ReadOnly = true;
            this.dataGridViewWHManage.Size = new System.Drawing.Size(947, 295);
            this.dataGridViewWHManage.TabIndex = 6;
            this.dataGridViewWHManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewWHManage_CellClick);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel4.Controls.Add(this.cbIsHOWhManage);
            this.panel4.Controls.Add(this.lbIsHOWhManage);
            this.panel4.Controls.Add(this.cbAreaWHManage);
            this.panel4.Controls.Add(this.txtLinkWSWHManage);
            this.panel4.Controls.Add(this.lbLinkWSWHManage);
            this.panel4.Controls.Add(this.lbAreaWHManage);
            this.panel4.Controls.Add(this.txtWhIdWhManage);
            this.panel4.Controls.Add(this.txtPalletWHManage);
            this.panel4.Controls.Add(this.lbPalletWHManage);
            this.panel4.Controls.Add(this.txtNoteWHManage);
            this.panel4.Controls.Add(this.txtPhoneWHManage);
            this.panel4.Controls.Add(this.txtAddressWHManage);
            this.panel4.Controls.Add(this.lbNoteWHManage);
            this.panel4.Controls.Add(this.lbPhoneWHManage);
            this.panel4.Controls.Add(this.lbAddressWHManage);
            this.panel4.Controls.Add(this.txtNameWHManage);
            this.panel4.Controls.Add(this.lbWhIdManage);
            this.panel4.Controls.Add(this.lbNameWHManage);
            this.panel4.Controls.Add(this.lbTitleWHManage);
            this.panel4.Location = new System.Drawing.Point(-2, -2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(947, 146);
            this.panel4.TabIndex = 5;
            // 
            // cbIsHOWhManage
            // 
            this.cbIsHOWhManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIsHOWhManage.FormattingEnabled = true;
            this.cbIsHOWhManage.Location = new System.Drawing.Point(649, 118);
            this.cbIsHOWhManage.Name = "cbIsHOWhManage";
            this.cbIsHOWhManage.Size = new System.Drawing.Size(170, 21);
            this.cbIsHOWhManage.TabIndex = 28;
            // 
            // lbIsHOWhManage
            // 
            this.lbIsHOWhManage.Location = new System.Drawing.Point(557, 122);
            this.lbIsHOWhManage.Name = "lbIsHOWhManage";
            this.lbIsHOWhManage.Size = new System.Drawing.Size(87, 13);
            this.lbIsHOWhManage.TabIndex = 27;
            this.lbIsHOWhManage.Text = "Kiểu tài khoản";
            this.lbIsHOWhManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbAreaWHManage
            // 
            this.cbAreaWHManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAreaWHManage.FormattingEnabled = true;
            this.cbAreaWHManage.Location = new System.Drawing.Point(373, 91);
            this.cbAreaWHManage.Name = "cbAreaWHManage";
            this.cbAreaWHManage.Size = new System.Drawing.Size(167, 21);
            this.cbAreaWHManage.TabIndex = 22;
            // 
            // txtLinkWSWHManage
            // 
            this.txtLinkWSWHManage.Location = new System.Drawing.Point(373, 119);
            this.txtLinkWSWHManage.Name = "txtLinkWSWHManage";
            this.txtLinkWSWHManage.Size = new System.Drawing.Size(167, 20);
            this.txtLinkWSWHManage.TabIndex = 21;
            // 
            // lbLinkWSWHManage
            // 
            this.lbLinkWSWHManage.Location = new System.Drawing.Point(285, 123);
            this.lbLinkWSWHManage.Name = "lbLinkWSWHManage";
            this.lbLinkWSWHManage.Size = new System.Drawing.Size(84, 13);
            this.lbLinkWSWHManage.TabIndex = 20;
            this.lbLinkWSWHManage.Text = "Đường dẫn WS";
            this.lbLinkWSWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbAreaWHManage
            // 
            this.lbAreaWHManage.Location = new System.Drawing.Point(289, 94);
            this.lbAreaWHManage.Name = "lbAreaWHManage";
            this.lbAreaWHManage.Size = new System.Drawing.Size(80, 13);
            this.lbAreaWHManage.TabIndex = 18;
            this.lbAreaWHManage.Text = "Khu vực";
            this.lbAreaWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtWhIdWhManage
            // 
            this.txtWhIdWhManage.Location = new System.Drawing.Point(109, 62);
            this.txtWhIdWhManage.Name = "txtWhIdWhManage";
            this.txtWhIdWhManage.ReadOnly = true;
            this.txtWhIdWhManage.Size = new System.Drawing.Size(170, 20);
            this.txtWhIdWhManage.TabIndex = 17;
            // 
            // txtPalletWHManage
            // 
            this.txtPalletWHManage.Location = new System.Drawing.Point(373, 62);
            this.txtPalletWHManage.Name = "txtPalletWHManage";
            this.txtPalletWHManage.ReadOnly = true;
            this.txtPalletWHManage.Size = new System.Drawing.Size(167, 20);
            this.txtPalletWHManage.TabIndex = 16;
            this.txtPalletWHManage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalletWHManage_KeyPress);
            // 
            // lbPalletWHManage
            // 
            this.lbPalletWHManage.Location = new System.Drawing.Point(289, 67);
            this.lbPalletWHManage.Name = "lbPalletWHManage";
            this.lbPalletWHManage.Size = new System.Drawing.Size(80, 13);
            this.lbPalletWHManage.TabIndex = 15;
            this.lbPalletWHManage.Text = "Số lượng Pallet";
            this.lbPalletWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteWHManage
            // 
            this.txtNoteWHManage.Location = new System.Drawing.Point(373, 34);
            this.txtNoteWHManage.Name = "txtNoteWHManage";
            this.txtNoteWHManage.Size = new System.Drawing.Size(167, 20);
            this.txtNoteWHManage.TabIndex = 14;
            // 
            // txtPhoneWHManage
            // 
            this.txtPhoneWHManage.Location = new System.Drawing.Point(109, 119);
            this.txtPhoneWHManage.Name = "txtPhoneWHManage";
            this.txtPhoneWHManage.Size = new System.Drawing.Size(170, 20);
            this.txtPhoneWHManage.TabIndex = 13;
            // 
            // txtAddressWHManage
            // 
            this.txtAddressWHManage.Location = new System.Drawing.Point(109, 91);
            this.txtAddressWHManage.Name = "txtAddressWHManage";
            this.txtAddressWHManage.Size = new System.Drawing.Size(170, 20);
            this.txtAddressWHManage.TabIndex = 12;
            // 
            // lbNoteWHManage
            // 
            this.lbNoteWHManage.Location = new System.Drawing.Point(325, 38);
            this.lbNoteWHManage.Name = "lbNoteWHManage";
            this.lbNoteWHManage.Size = new System.Drawing.Size(44, 13);
            this.lbNoteWHManage.TabIndex = 11;
            this.lbNoteWHManage.Text = "Ghi chú";
            this.lbNoteWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPhoneWHManage
            // 
            this.lbPhoneWHManage.Location = new System.Drawing.Point(48, 123);
            this.lbPhoneWHManage.Name = "lbPhoneWHManage";
            this.lbPhoneWHManage.Size = new System.Drawing.Size(55, 13);
            this.lbPhoneWHManage.TabIndex = 10;
            this.lbPhoneWHManage.Text = "Điện thoại";
            this.lbPhoneWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbAddressWHManage
            // 
            this.lbAddressWHManage.Location = new System.Drawing.Point(63, 95);
            this.lbAddressWHManage.Name = "lbAddressWHManage";
            this.lbAddressWHManage.Size = new System.Drawing.Size(40, 13);
            this.lbAddressWHManage.TabIndex = 9;
            this.lbAddressWHManage.Text = "Địa chỉ";
            this.lbAddressWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNameWHManage
            // 
            this.txtNameWHManage.Location = new System.Drawing.Point(109, 34);
            this.txtNameWHManage.Name = "txtNameWHManage";
            this.txtNameWHManage.Size = new System.Drawing.Size(170, 20);
            this.txtNameWHManage.TabIndex = 6;
            // 
            // lbWhIdManage
            // 
            this.lbWhIdManage.Location = new System.Drawing.Point(5, 67);
            this.lbWhIdManage.Name = "lbWhIdManage";
            this.lbWhIdManage.Size = new System.Drawing.Size(98, 13);
            this.lbWhIdManage.TabIndex = 4;
            this.lbWhIdManage.Text = "Mã kho hàng";
            this.lbWhIdManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbNameWHManage
            // 
            this.lbNameWHManage.Location = new System.Drawing.Point(27, 37);
            this.lbNameWHManage.Name = "lbNameWHManage";
            this.lbNameWHManage.Size = new System.Drawing.Size(76, 13);
            this.lbNameWHManage.TabIndex = 2;
            this.lbNameWHManage.Text = "Tên kho hàng";
            this.lbNameWHManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTitleWHManage
            // 
            this.lbTitleWHManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitleWHManage.Location = new System.Drawing.Point(13, 4);
            this.lbTitleWHManage.Name = "lbTitleWHManage";
            this.lbTitleWHManage.Size = new System.Drawing.Size(110, 13);
            this.lbTitleWHManage.TabIndex = 0;
            this.lbTitleWHManage.Text = "Quản lý chi nhánh";
            // 
            // tbTruckManage
            // 
            this.tbTruckManage.Controls.Add(this.dataGridViewTruckManage);
            this.tbTruckManage.Controls.Add(this.panel1);
            this.tbTruckManage.Location = new System.Drawing.Point(4, 22);
            this.tbTruckManage.Name = "tbTruckManage";
            this.tbTruckManage.Padding = new System.Windows.Forms.Padding(3);
            this.tbTruckManage.Size = new System.Drawing.Size(939, 439);
            this.tbTruckManage.TabIndex = 1;
            this.tbTruckManage.Text = "Quản lý xe tải";
            this.tbTruckManage.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTruckManage
            // 
            this.dataGridViewTruckManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTruckManage.Location = new System.Drawing.Point(-6, 146);
            this.dataGridViewTruckManage.Name = "dataGridViewTruckManage";
            this.dataGridViewTruckManage.ReadOnly = true;
            this.dataGridViewTruckManage.Size = new System.Drawing.Size(947, 295);
            this.dataGridViewTruckManage.TabIndex = 6;
            this.dataGridViewTruckManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTruckManage_CellClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.txtIdCardTruckManage);
            this.panel1.Controls.Add(this.lbIdCardTruckManage);
            this.panel1.Controls.Add(this.txtStatusTruckManage);
            this.panel1.Controls.Add(this.lbStatusTruckManage);
            this.panel1.Controls.Add(this.dtpEndDateTruckManage);
            this.panel1.Controls.Add(this.dtpBeginDateTruckManage);
            this.panel1.Controls.Add(this.lbEndDateTruckManage);
            this.panel1.Controls.Add(this.lbBeginDateTruckManage);
            this.panel1.Controls.Add(this.txtNoteTruckManage);
            this.panel1.Controls.Add(this.lbNoteTruck);
            this.panel1.Controls.Add(this.txtPhoneTruckManage);
            this.panel1.Controls.Add(this.txtAddressTruckManage);
            this.panel1.Controls.Add(this.txtOwnerTruckManage);
            this.panel1.Controls.Add(this.lbPhoneTruck);
            this.panel1.Controls.Add(this.lbAddressTruck);
            this.panel1.Controls.Add(this.lbOwnerTruckManage);
            this.panel1.Controls.Add(this.cbWhTruckManage);
            this.panel1.Controls.Add(this.txtDriverTruckManage);
            this.panel1.Controls.Add(this.txtLicensePlateTruckManages);
            this.panel1.Controls.Add(this.lbWhTruckManage);
            this.panel1.Controls.Add(this.lbDriverTruckManage);
            this.panel1.Controls.Add(this.lbLicensePlateTruckManage);
            this.panel1.Controls.Add(this.lbTruckManage);
            this.panel1.Location = new System.Drawing.Point(-2, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(947, 146);
            this.panel1.TabIndex = 5;
            // 
            // txtIdCardTruckManage
            // 
            this.txtIdCardTruckManage.Location = new System.Drawing.Point(653, 88);
            this.txtIdCardTruckManage.Name = "txtIdCardTruckManage";
            this.txtIdCardTruckManage.Size = new System.Drawing.Size(167, 20);
            this.txtIdCardTruckManage.TabIndex = 32;
            // 
            // lbIdCardTruckManage
            // 
            this.lbIdCardTruckManage.Location = new System.Drawing.Point(560, 91);
            this.lbIdCardTruckManage.Name = "lbIdCardTruckManage";
            this.lbIdCardTruckManage.Size = new System.Drawing.Size(87, 13);
            this.lbIdCardTruckManage.TabIndex = 31;
            this.lbIdCardTruckManage.Text = "CMND";
            this.lbIdCardTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStatusTruckManage
            // 
            this.txtStatusTruckManage.Location = new System.Drawing.Point(374, 34);
            this.txtStatusTruckManage.Name = "txtStatusTruckManage";
            this.txtStatusTruckManage.Size = new System.Drawing.Size(170, 20);
            this.txtStatusTruckManage.TabIndex = 30;
            // 
            // lbStatusTruckManage
            // 
            this.lbStatusTruckManage.Location = new System.Drawing.Point(281, 37);
            this.lbStatusTruckManage.Name = "lbStatusTruckManage";
            this.lbStatusTruckManage.Size = new System.Drawing.Size(87, 13);
            this.lbStatusTruckManage.TabIndex = 29;
            this.lbStatusTruckManage.Text = "Tình trạng xe";
            this.lbStatusTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpEndDateTruckManage
            // 
            this.dtpEndDateTruckManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpEndDateTruckManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDateTruckManage.Location = new System.Drawing.Point(653, 62);
            this.dtpEndDateTruckManage.Name = "dtpEndDateTruckManage";
            this.dtpEndDateTruckManage.Size = new System.Drawing.Size(167, 20);
            this.dtpEndDateTruckManage.TabIndex = 28;
            // 
            // dtpBeginDateTruckManage
            // 
            this.dtpBeginDateTruckManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpBeginDateTruckManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginDateTruckManage.Location = new System.Drawing.Point(653, 35);
            this.dtpBeginDateTruckManage.Name = "dtpBeginDateTruckManage";
            this.dtpBeginDateTruckManage.Size = new System.Drawing.Size(167, 20);
            this.dtpBeginDateTruckManage.TabIndex = 27;
            // 
            // lbEndDateTruckManage
            // 
            this.lbEndDateTruckManage.Location = new System.Drawing.Point(564, 64);
            this.lbEndDateTruckManage.Name = "lbEndDateTruckManage";
            this.lbEndDateTruckManage.Size = new System.Drawing.Size(83, 13);
            this.lbEndDateTruckManage.TabIndex = 26;
            this.lbEndDateTruckManage.Text = "Ngày kết thúc";
            this.lbEndDateTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbBeginDateTruckManage
            // 
            this.lbBeginDateTruckManage.Location = new System.Drawing.Point(569, 38);
            this.lbBeginDateTruckManage.Name = "lbBeginDateTruckManage";
            this.lbBeginDateTruckManage.Size = new System.Drawing.Size(78, 15);
            this.lbBeginDateTruckManage.TabIndex = 25;
            this.lbBeginDateTruckManage.Text = "Ngày bắt đầu";
            this.lbBeginDateTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteTruckManage
            // 
            this.txtNoteTruckManage.Location = new System.Drawing.Point(374, 119);
            this.txtNoteTruckManage.Name = "txtNoteTruckManage";
            this.txtNoteTruckManage.Size = new System.Drawing.Size(167, 20);
            this.txtNoteTruckManage.TabIndex = 16;
            // 
            // lbNoteTruck
            // 
            this.lbNoteTruck.Location = new System.Drawing.Point(324, 123);
            this.lbNoteTruck.Name = "lbNoteTruck";
            this.lbNoteTruck.Size = new System.Drawing.Size(44, 13);
            this.lbNoteTruck.TabIndex = 15;
            this.lbNoteTruck.Text = "Ghi chú";
            this.lbNoteTruck.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPhoneTruckManage
            // 
            this.txtPhoneTruckManage.Location = new System.Drawing.Point(374, 90);
            this.txtPhoneTruckManage.Name = "txtPhoneTruckManage";
            this.txtPhoneTruckManage.Size = new System.Drawing.Size(167, 20);
            this.txtPhoneTruckManage.TabIndex = 14;
            // 
            // txtAddressTruckManage
            // 
            this.txtAddressTruckManage.Location = new System.Drawing.Point(374, 62);
            this.txtAddressTruckManage.Name = "txtAddressTruckManage";
            this.txtAddressTruckManage.Size = new System.Drawing.Size(167, 20);
            this.txtAddressTruckManage.TabIndex = 13;
            // 
            // txtOwnerTruckManage
            // 
            this.txtOwnerTruckManage.Location = new System.Drawing.Point(109, 90);
            this.txtOwnerTruckManage.Name = "txtOwnerTruckManage";
            this.txtOwnerTruckManage.Size = new System.Drawing.Size(170, 20);
            this.txtOwnerTruckManage.TabIndex = 12;
            // 
            // lbPhoneTruck
            // 
            this.lbPhoneTruck.Location = new System.Drawing.Point(313, 94);
            this.lbPhoneTruck.Name = "lbPhoneTruck";
            this.lbPhoneTruck.Size = new System.Drawing.Size(55, 13);
            this.lbPhoneTruck.TabIndex = 11;
            this.lbPhoneTruck.Text = "Điện thoại";
            this.lbPhoneTruck.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbAddressTruck
            // 
            this.lbAddressTruck.Location = new System.Drawing.Point(327, 66);
            this.lbAddressTruck.Name = "lbAddressTruck";
            this.lbAddressTruck.Size = new System.Drawing.Size(40, 13);
            this.lbAddressTruck.TabIndex = 10;
            this.lbAddressTruck.Text = "Địa chỉ";
            this.lbAddressTruck.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbOwnerTruckManage
            // 
            this.lbOwnerTruckManage.Location = new System.Drawing.Point(49, 93);
            this.lbOwnerTruckManage.Name = "lbOwnerTruckManage";
            this.lbOwnerTruckManage.Size = new System.Drawing.Size(57, 13);
            this.lbOwnerTruckManage.TabIndex = 9;
            this.lbOwnerTruckManage.Text = "Chủ xe";
            this.lbOwnerTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbWhTruckManage
            // 
            this.cbWhTruckManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhTruckManage.FormattingEnabled = true;
            this.cbWhTruckManage.Location = new System.Drawing.Point(109, 119);
            this.cbWhTruckManage.Name = "cbWhTruckManage";
            this.cbWhTruckManage.Size = new System.Drawing.Size(170, 21);
            this.cbWhTruckManage.TabIndex = 8;
            // 
            // txtDriverTruckManage
            // 
            this.txtDriverTruckManage.Location = new System.Drawing.Point(109, 62);
            this.txtDriverTruckManage.Name = "txtDriverTruckManage";
            this.txtDriverTruckManage.Size = new System.Drawing.Size(170, 20);
            this.txtDriverTruckManage.TabIndex = 6;
            // 
            // txtLicensePlateTruckManages
            // 
            this.txtLicensePlateTruckManages.Location = new System.Drawing.Point(109, 34);
            this.txtLicensePlateTruckManages.Name = "txtLicensePlateTruckManages";
            this.txtLicensePlateTruckManages.Size = new System.Drawing.Size(170, 20);
            this.txtLicensePlateTruckManages.TabIndex = 5;
            // 
            // lbWhTruckManage
            // 
            this.lbWhTruckManage.Location = new System.Drawing.Point(11, 123);
            this.lbWhTruckManage.Name = "lbWhTruckManage";
            this.lbWhTruckManage.Size = new System.Drawing.Size(92, 13);
            this.lbWhTruckManage.TabIndex = 4;
            this.lbWhTruckManage.Text = "Kho hàng";
            this.lbWhTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbDriverTruckManage
            // 
            this.lbDriverTruckManage.Location = new System.Drawing.Point(40, 65);
            this.lbDriverTruckManage.Name = "lbDriverTruckManage";
            this.lbDriverTruckManage.Size = new System.Drawing.Size(63, 13);
            this.lbDriverTruckManage.TabIndex = 2;
            this.lbDriverTruckManage.Text = "Tài xế";
            this.lbDriverTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbLicensePlateTruckManage
            // 
            this.lbLicensePlateTruckManage.Location = new System.Drawing.Point(31, 38);
            this.lbLicensePlateTruckManage.Name = "lbLicensePlateTruckManage";
            this.lbLicensePlateTruckManage.Size = new System.Drawing.Size(72, 13);
            this.lbLicensePlateTruckManage.TabIndex = 1;
            this.lbLicensePlateTruckManage.Text = "Biển số xe";
            this.lbLicensePlateTruckManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTruckManage
            // 
            this.lbTruckManage.AutoSize = true;
            this.lbTruckManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTruckManage.Location = new System.Drawing.Point(13, 4);
            this.lbTruckManage.Name = "lbTruckManage";
            this.lbTruckManage.Size = new System.Drawing.Size(85, 13);
            this.lbTruckManage.TabIndex = 0;
            this.lbTruckManage.Text = "Quản lý xe tải";
            // 
            // tabPaletManage
            // 
            this.tabPaletManage.Controls.Add(this.dataGridViewPaletManage);
            this.tabPaletManage.Controls.Add(this.panel2);
            this.tabPaletManage.Location = new System.Drawing.Point(4, 22);
            this.tabPaletManage.Name = "tabPaletManage";
            this.tabPaletManage.Size = new System.Drawing.Size(939, 439);
            this.tabPaletManage.TabIndex = 2;
            this.tabPaletManage.Text = "Quản lý hàng hóa trong kho";
            this.tabPaletManage.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPaletManage
            // 
            this.dataGridViewPaletManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPaletManage.Location = new System.Drawing.Point(-6, 146);
            this.dataGridViewPaletManage.Name = "dataGridViewPaletManage";
            this.dataGridViewPaletManage.ReadOnly = true;
            this.dataGridViewPaletManage.Size = new System.Drawing.Size(947, 295);
            this.dataGridViewPaletManage.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.cbWhPaletManage);
            this.panel2.Controls.Add(this.dtpEndDatePaletManage);
            this.panel2.Controls.Add(this.dtpBeginDatePaletManage);
            this.panel2.Controls.Add(this.lbEndDatePaletManage);
            this.panel2.Controls.Add(this.lbBeginDatePaletManage);
            this.panel2.Controls.Add(this.txtStatusPaletManage);
            this.panel2.Controls.Add(this.txtSerialPaletManage);
            this.panel2.Controls.Add(this.lbStatusPaletManage);
            this.panel2.Controls.Add(this.lbWhPaletManage);
            this.panel2.Controls.Add(this.lbSerialPaletManage);
            this.panel2.Controls.Add(this.lbGoodsManage);
            this.panel2.Location = new System.Drawing.Point(-2, -2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(947, 146);
            this.panel2.TabIndex = 7;
            // 
            // cbWhPaletManage
            // 
            this.cbWhPaletManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhPaletManage.FormattingEnabled = true;
            this.cbWhPaletManage.Location = new System.Drawing.Point(109, 62);
            this.cbWhPaletManage.Name = "cbWhPaletManage";
            this.cbWhPaletManage.Size = new System.Drawing.Size(170, 21);
            this.cbWhPaletManage.TabIndex = 21;
            // 
            // dtpEndDatePaletManage
            // 
            this.dtpEndDatePaletManage.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDatePaletManage.Location = new System.Drawing.Point(387, 62);
            this.dtpEndDatePaletManage.Name = "dtpEndDatePaletManage";
            this.dtpEndDatePaletManage.Size = new System.Drawing.Size(148, 20);
            this.dtpEndDatePaletManage.TabIndex = 20;
            // 
            // dtpBeginDatePaletManage
            // 
            this.dtpBeginDatePaletManage.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBeginDatePaletManage.Location = new System.Drawing.Point(386, 34);
            this.dtpBeginDatePaletManage.Name = "dtpBeginDatePaletManage";
            this.dtpBeginDatePaletManage.Size = new System.Drawing.Size(149, 20);
            this.dtpBeginDatePaletManage.TabIndex = 19;
            // 
            // lbEndDatePaletManage
            // 
            this.lbEndDatePaletManage.Location = new System.Drawing.Point(304, 66);
            this.lbEndDatePaletManage.Name = "lbEndDatePaletManage";
            this.lbEndDatePaletManage.Size = new System.Drawing.Size(74, 13);
            this.lbEndDatePaletManage.TabIndex = 18;
            this.lbEndDatePaletManage.Text = "Ngày kết thúc";
            this.lbEndDatePaletManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbBeginDatePaletManage
            // 
            this.lbBeginDatePaletManage.Location = new System.Drawing.Point(301, 36);
            this.lbBeginDatePaletManage.Name = "lbBeginDatePaletManage";
            this.lbBeginDatePaletManage.Size = new System.Drawing.Size(78, 15);
            this.lbBeginDatePaletManage.TabIndex = 17;
            this.lbBeginDatePaletManage.Text = "Ngày bắt đầu";
            this.lbBeginDatePaletManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStatusPaletManage
            // 
            this.txtStatusPaletManage.Location = new System.Drawing.Point(109, 93);
            this.txtStatusPaletManage.Name = "txtStatusPaletManage";
            this.txtStatusPaletManage.Size = new System.Drawing.Size(170, 20);
            this.txtStatusPaletManage.TabIndex = 7;
            // 
            // txtSerialPaletManage
            // 
            this.txtSerialPaletManage.Location = new System.Drawing.Point(109, 34);
            this.txtSerialPaletManage.Name = "txtSerialPaletManage";
            this.txtSerialPaletManage.Size = new System.Drawing.Size(170, 20);
            this.txtSerialPaletManage.TabIndex = 5;
            // 
            // lbStatusPaletManage
            // 
            this.lbStatusPaletManage.Location = new System.Drawing.Point(43, 97);
            this.lbStatusPaletManage.Name = "lbStatusPaletManage";
            this.lbStatusPaletManage.Size = new System.Drawing.Size(60, 13);
            this.lbStatusPaletManage.TabIndex = 3;
            this.lbStatusPaletManage.Text = "Tình trạng";
            this.lbStatusPaletManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbWhPaletManage
            // 
            this.lbWhPaletManage.Location = new System.Drawing.Point(40, 65);
            this.lbWhPaletManage.Name = "lbWhPaletManage";
            this.lbWhPaletManage.Size = new System.Drawing.Size(63, 13);
            this.lbWhPaletManage.TabIndex = 2;
            this.lbWhPaletManage.Text = "Nhà kho";
            this.lbWhPaletManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbSerialPaletManage
            // 
            this.lbSerialPaletManage.Location = new System.Drawing.Point(31, 38);
            this.lbSerialPaletManage.Name = "lbSerialPaletManage";
            this.lbSerialPaletManage.Size = new System.Drawing.Size(72, 13);
            this.lbSerialPaletManage.TabIndex = 1;
            this.lbSerialPaletManage.Text = "Số Seri";
            this.lbSerialPaletManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbGoodsManage
            // 
            this.lbGoodsManage.AutoSize = true;
            this.lbGoodsManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGoodsManage.Location = new System.Drawing.Point(13, 4);
            this.lbGoodsManage.Name = "lbGoodsManage";
            this.lbGoodsManage.Size = new System.Drawing.Size(165, 13);
            this.lbGoodsManage.TabIndex = 0;
            this.lbGoodsManage.Text = "Quản lý hàng hóa trong kho";
            // 
            // tabImportManagement
            // 
            this.tabImportManagement.Controls.Add(this.panel6);
            this.tabImportManagement.Controls.Add(this.panel5);
            this.tabImportManagement.Location = new System.Drawing.Point(4, 22);
            this.tabImportManagement.Name = "tabImportManagement";
            this.tabImportManagement.Size = new System.Drawing.Size(939, 439);
            this.tabImportManagement.TabIndex = 3;
            this.tabImportManagement.Text = "Quản lý nhập kho";
            this.tabImportManagement.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.subTabImportManage);
            this.panel6.Location = new System.Drawing.Point(448, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(492, 435);
            this.panel6.TabIndex = 1;
            // 
            // subTabImportManage
            // 
            this.subTabImportManage.Controls.Add(this.tabImportImportManage);
            this.subTabImportManage.Controls.Add(this.tabDetailImportImportManage);
            this.subTabImportManage.Location = new System.Drawing.Point(4, 4);
            this.subTabImportManage.Name = "subTabImportManage";
            this.subTabImportManage.SelectedIndex = 0;
            this.subTabImportManage.Size = new System.Drawing.Size(485, 428);
            this.subTabImportManage.TabIndex = 0;
            // 
            // tabImportImportManage
            // 
            this.tabImportImportManage.Controls.Add(this.label33);
            this.tabImportImportManage.Controls.Add(this.btnUpdateImport);
            this.tabImportImportManage.Controls.Add(this.btnAddNewImport);
            this.tabImportImportManage.Controls.Add(this.panel7);
            this.tabImportImportManage.Location = new System.Drawing.Point(4, 22);
            this.tabImportImportManage.Name = "tabImportImportManage";
            this.tabImportImportManage.Padding = new System.Windows.Forms.Padding(3);
            this.tabImportImportManage.Size = new System.Drawing.Size(477, 402);
            this.tabImportImportManage.TabIndex = 0;
            this.tabImportImportManage.Text = "Thông tin phiếu nhập";
            this.tabImportImportManage.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 383);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(376, 13);
            this.label33.TabIndex = 5;
            this.label33.Text = "Bạn bắt buộc phải nhập mới thông tin phiếu trước khi thêm \"Chi tiết nhập kho\"";
            // 
            // btnUpdateImport
            // 
            this.btnUpdateImport.Location = new System.Drawing.Point(250, 214);
            this.btnUpdateImport.Name = "btnUpdateImport";
            this.btnUpdateImport.Size = new System.Drawing.Size(155, 50);
            this.btnUpdateImport.TabIndex = 4;
            this.btnUpdateImport.Text = "Cập nhật phiếu nhập";
            this.btnUpdateImport.UseVisualStyleBackColor = true;
            this.btnUpdateImport.Click += new System.EventHandler(this.btnUpdateImport_Click);
            // 
            // btnAddNewImport
            // 
            this.btnAddNewImport.Location = new System.Drawing.Point(103, 214);
            this.btnAddNewImport.Name = "btnAddNewImport";
            this.btnAddNewImport.Size = new System.Drawing.Size(141, 49);
            this.btnAddNewImport.TabIndex = 3;
            this.btnAddNewImport.Text = "Thêm mới phiếu nhập";
            this.btnAddNewImport.UseVisualStyleBackColor = true;
            this.btnAddNewImport.Click += new System.EventHandler(this.btnAddNewImport_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtUserIdImportManage);
            this.panel7.Controls.Add(this.cbWhCurrentImportManage);
            this.panel7.Controls.Add(this.lbCurrentWhImportManage);
            this.panel7.Controls.Add(this.txtUsernameImportManage);
            this.panel7.Controls.Add(this.lbCreateUserImportManage);
            this.panel7.Controls.Add(this.txtNoteImportManage);
            this.panel7.Controls.Add(this.lbNoteImportManage);
            this.panel7.Controls.Add(this.lbQuantityPaletImportManage);
            this.panel7.Controls.Add(this.cbProduceImportManage);
            this.panel7.Controls.Add(this.txtQuantityImportManage);
            this.panel7.Controls.Add(this.dtpUpdateDateImportManage);
            this.panel7.Controls.Add(this.lbUpdateDateImportManage);
            this.panel7.Controls.Add(this.cbWhExImportManage);
            this.panel7.Controls.Add(this.lbSourceWhImportManage);
            this.panel7.Controls.Add(this.dtpCreateDateImportManage);
            this.panel7.Controls.Add(this.lbTypeProductImportManage);
            this.panel7.Controls.Add(this.lbCreateDateImportManage);
            this.panel7.Controls.Add(this.txtSerialImportManage);
            this.panel7.Controls.Add(this.lbSerialImportImportManage);
            this.panel7.Controls.Add(this.lbNameImportManage);
            this.panel7.Controls.Add(this.txtNameImportManage);
            this.panel7.Location = new System.Drawing.Point(7, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(464, 188);
            this.panel7.TabIndex = 0;
            // 
            // txtUserIdImportManage
            // 
            this.txtUserIdImportManage.Location = new System.Drawing.Point(308, 165);
            this.txtUserIdImportManage.Name = "txtUserIdImportManage";
            this.txtUserIdImportManage.ReadOnly = true;
            this.txtUserIdImportManage.Size = new System.Drawing.Size(144, 20);
            this.txtUserIdImportManage.TabIndex = 70;
            this.txtUserIdImportManage.Visible = false;
            // 
            // cbWhCurrentImportManage
            // 
            this.cbWhCurrentImportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhCurrentImportManage.Enabled = false;
            this.cbWhCurrentImportManage.FormattingEnabled = true;
            this.cbWhCurrentImportManage.Location = new System.Drawing.Point(79, 80);
            this.cbWhCurrentImportManage.Name = "cbWhCurrentImportManage";
            this.cbWhCurrentImportManage.Size = new System.Drawing.Size(147, 21);
            this.cbWhCurrentImportManage.TabIndex = 69;
            // 
            // lbCurrentWhImportManage
            // 
            this.lbCurrentWhImportManage.Location = new System.Drawing.Point(12, 83);
            this.lbCurrentWhImportManage.Name = "lbCurrentWhImportManage";
            this.lbCurrentWhImportManage.Size = new System.Drawing.Size(65, 13);
            this.lbCurrentWhImportManage.TabIndex = 68;
            this.lbCurrentWhImportManage.Text = "Kho nhập";
            this.lbCurrentWhImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtUsernameImportManage
            // 
            this.txtUsernameImportManage.Location = new System.Drawing.Point(308, 110);
            this.txtUsernameImportManage.Name = "txtUsernameImportManage";
            this.txtUsernameImportManage.ReadOnly = true;
            this.txtUsernameImportManage.Size = new System.Drawing.Size(143, 20);
            this.txtUsernameImportManage.TabIndex = 67;
            // 
            // lbCreateUserImportManage
            // 
            this.lbCreateUserImportManage.Location = new System.Drawing.Point(250, 112);
            this.lbCreateUserImportManage.Name = "lbCreateUserImportManage";
            this.lbCreateUserImportManage.Size = new System.Drawing.Size(54, 13);
            this.lbCreateUserImportManage.TabIndex = 66;
            this.lbCreateUserImportManage.Text = "Người tạo";
            this.lbCreateUserImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteImportManage
            // 
            this.txtNoteImportManage.Location = new System.Drawing.Point(309, 80);
            this.txtNoteImportManage.Name = "txtNoteImportManage";
            this.txtNoteImportManage.Size = new System.Drawing.Size(143, 20);
            this.txtNoteImportManage.TabIndex = 65;
            // 
            // lbNoteImportManage
            // 
            this.lbNoteImportManage.Location = new System.Drawing.Point(250, 83);
            this.lbNoteImportManage.Name = "lbNoteImportManage";
            this.lbNoteImportManage.Size = new System.Drawing.Size(54, 13);
            this.lbNoteImportManage.TabIndex = 64;
            this.lbNoteImportManage.Text = "Ghi chú";
            this.lbNoteImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbQuantityPaletImportManage
            // 
            this.lbQuantityPaletImportManage.Location = new System.Drawing.Point(248, 53);
            this.lbQuantityPaletImportManage.Name = "lbQuantityPaletImportManage";
            this.lbQuantityPaletImportManage.Size = new System.Drawing.Size(56, 13);
            this.lbQuantityPaletImportManage.TabIndex = 63;
            this.lbQuantityPaletImportManage.Text = "Số lượng";
            this.lbQuantityPaletImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbProduceImportManage
            // 
            this.cbProduceImportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProduceImportManage.FormattingEnabled = true;
            this.cbProduceImportManage.Location = new System.Drawing.Point(309, 16);
            this.cbProduceImportManage.Name = "cbProduceImportManage";
            this.cbProduceImportManage.Size = new System.Drawing.Size(142, 21);
            this.cbProduceImportManage.TabIndex = 62;
            // 
            // txtQuantityImportManage
            // 
            this.txtQuantityImportManage.Location = new System.Drawing.Point(309, 50);
            this.txtQuantityImportManage.Name = "txtQuantityImportManage";
            this.txtQuantityImportManage.ReadOnly = true;
            this.txtQuantityImportManage.Size = new System.Drawing.Size(143, 20);
            this.txtQuantityImportManage.TabIndex = 61;
            // 
            // dtpUpdateDateImportManage
            // 
            this.dtpUpdateDateImportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpUpdateDateImportManage.Enabled = false;
            this.dtpUpdateDateImportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpUpdateDateImportManage.Location = new System.Drawing.Point(309, 140);
            this.dtpUpdateDateImportManage.Name = "dtpUpdateDateImportManage";
            this.dtpUpdateDateImportManage.Size = new System.Drawing.Size(143, 20);
            this.dtpUpdateDateImportManage.TabIndex = 60;
            // 
            // lbUpdateDateImportManage
            // 
            this.lbUpdateDateImportManage.Location = new System.Drawing.Point(245, 143);
            this.lbUpdateDateImportManage.Name = "lbUpdateDateImportManage";
            this.lbUpdateDateImportManage.Size = new System.Drawing.Size(59, 13);
            this.lbUpdateDateImportManage.TabIndex = 59;
            this.lbUpdateDateImportManage.Text = "Cập nhật";
            this.lbUpdateDateImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbWhExImportManage
            // 
            this.cbWhExImportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhExImportManage.FormattingEnabled = true;
            this.cbWhExImportManage.Location = new System.Drawing.Point(80, 50);
            this.cbWhExImportManage.Name = "cbWhExImportManage";
            this.cbWhExImportManage.Size = new System.Drawing.Size(147, 21);
            this.cbWhExImportManage.TabIndex = 56;
            // 
            // lbSourceWhImportManage
            // 
            this.lbSourceWhImportManage.Location = new System.Drawing.Point(12, 54);
            this.lbSourceWhImportManage.Name = "lbSourceWhImportManage";
            this.lbSourceWhImportManage.Size = new System.Drawing.Size(65, 13);
            this.lbSourceWhImportManage.TabIndex = 55;
            this.lbSourceWhImportManage.Text = "Kho nguồn";
            this.lbSourceWhImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpCreateDateImportManage
            // 
            this.dtpCreateDateImportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpCreateDateImportManage.Enabled = false;
            this.dtpCreateDateImportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDateImportManage.Location = new System.Drawing.Point(80, 140);
            this.dtpCreateDateImportManage.Name = "dtpCreateDateImportManage";
            this.dtpCreateDateImportManage.Size = new System.Drawing.Size(148, 20);
            this.dtpCreateDateImportManage.TabIndex = 53;
            // 
            // lbTypeProductImportManage
            // 
            this.lbTypeProductImportManage.Location = new System.Drawing.Point(250, 19);
            this.lbTypeProductImportManage.Name = "lbTypeProductImportManage";
            this.lbTypeProductImportManage.Size = new System.Drawing.Size(54, 13);
            this.lbTypeProductImportManage.TabIndex = 52;
            this.lbTypeProductImportManage.Text = "Loại";
            this.lbTypeProductImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbCreateDateImportManage
            // 
            this.lbCreateDateImportManage.Location = new System.Drawing.Point(15, 143);
            this.lbCreateDateImportManage.Name = "lbCreateDateImportManage";
            this.lbCreateDateImportManage.Size = new System.Drawing.Size(62, 13);
            this.lbCreateDateImportManage.TabIndex = 51;
            this.lbCreateDateImportManage.Text = "Ngày nhập";
            this.lbCreateDateImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSerialImportManage
            // 
            this.txtSerialImportManage.Location = new System.Drawing.Point(80, 110);
            this.txtSerialImportManage.Name = "txtSerialImportManage";
            this.txtSerialImportManage.Size = new System.Drawing.Size(148, 20);
            this.txtSerialImportManage.TabIndex = 50;
            // 
            // lbSerialImportImportManage
            // 
            this.lbSerialImportImportManage.Location = new System.Drawing.Point(18, 112);
            this.lbSerialImportImportManage.Name = "lbSerialImportImportManage";
            this.lbSerialImportImportManage.Size = new System.Drawing.Size(59, 13);
            this.lbSerialImportImportManage.TabIndex = 49;
            this.lbSerialImportImportManage.Text = "Seri phiếu";
            this.lbSerialImportImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbNameImportManage
            // 
            this.lbNameImportManage.Location = new System.Drawing.Point(21, 23);
            this.lbNameImportManage.Name = "lbNameImportManage";
            this.lbNameImportManage.Size = new System.Drawing.Size(56, 13);
            this.lbNameImportManage.TabIndex = 48;
            this.lbNameImportManage.Text = "Tên phiếu";
            this.lbNameImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNameImportManage
            // 
            this.txtNameImportManage.Location = new System.Drawing.Point(79, 20);
            this.txtNameImportManage.Name = "txtNameImportManage";
            this.txtNameImportManage.Size = new System.Drawing.Size(148, 20);
            this.txtNameImportManage.TabIndex = 47;
            // 
            // tabDetailImportImportManage
            // 
            this.tabDetailImportImportManage.Controls.Add(this.panel9);
            this.tabDetailImportImportManage.Controls.Add(this.panel8);
            this.tabDetailImportImportManage.Location = new System.Drawing.Point(4, 22);
            this.tabDetailImportImportManage.Name = "tabDetailImportImportManage";
            this.tabDetailImportImportManage.Padding = new System.Windows.Forms.Padding(3);
            this.tabDetailImportImportManage.Size = new System.Drawing.Size(477, 402);
            this.tabDetailImportImportManage.TabIndex = 1;
            this.tabDetailImportImportManage.Text = "Chi tiết nhập kho";
            this.tabDetailImportImportManage.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnUpdateDetailImportPalet);
            this.panel9.Controls.Add(this.btnAddNewDetailImportPalet);
            this.panel9.Controls.Add(this.dataGridViewDetailImportAndPalet);
            this.panel9.Location = new System.Drawing.Point(4, 156);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(467, 238);
            this.panel9.TabIndex = 1;
            // 
            // btnUpdateDetailImportPalet
            // 
            this.btnUpdateDetailImportPalet.Location = new System.Drawing.Point(243, 210);
            this.btnUpdateDetailImportPalet.Name = "btnUpdateDetailImportPalet";
            this.btnUpdateDetailImportPalet.Size = new System.Drawing.Size(100, 23);
            this.btnUpdateDetailImportPalet.TabIndex = 2;
            this.btnUpdateDetailImportPalet.Text = "Cập nhật chi tiết";
            this.btnUpdateDetailImportPalet.UseVisualStyleBackColor = true;
            this.btnUpdateDetailImportPalet.Click += new System.EventHandler(this.btnUpdateDetailImportPalet_Click);
            // 
            // btnAddNewDetailImportPalet
            // 
            this.btnAddNewDetailImportPalet.Location = new System.Drawing.Point(123, 210);
            this.btnAddNewDetailImportPalet.Name = "btnAddNewDetailImportPalet";
            this.btnAddNewDetailImportPalet.Size = new System.Drawing.Size(98, 23);
            this.btnAddNewDetailImportPalet.TabIndex = 1;
            this.btnAddNewDetailImportPalet.Text = "Thêm mới chi tiết";
            this.btnAddNewDetailImportPalet.UseVisualStyleBackColor = true;
            this.btnAddNewDetailImportPalet.Click += new System.EventHandler(this.btnAddNewDetailImportPalet_Click);
            // 
            // dataGridViewDetailImportAndPalet
            // 
            this.dataGridViewDetailImportAndPalet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetailImportAndPalet.Location = new System.Drawing.Point(2, 2);
            this.dataGridViewDetailImportAndPalet.Name = "dataGridViewDetailImportAndPalet";
            this.dataGridViewDetailImportAndPalet.Size = new System.Drawing.Size(462, 192);
            this.dataGridViewDetailImportAndPalet.TabIndex = 0;
            this.dataGridViewDetailImportAndPalet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailImportAndPalet_CellClick);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtNameBelongImport);
            this.panel8.Controls.Add(this.label24);
            this.panel8.Controls.Add(this.txtStatusPlateImportManage);
            this.panel8.Controls.Add(this.label23);
            this.panel8.Controls.Add(this.dtpEndDatePaletImportManage);
            this.panel8.Controls.Add(this.label21);
            this.panel8.Controls.Add(this.dtpBeginDatePaletImportManage);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.cbWhDetailTabImportManage);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.txtNoteDetailImportImportManage);
            this.panel8.Controls.Add(this.dtpCreateDateSubTabImportManage);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.label13);
            this.panel8.Controls.Add(this.txtLincensePlateImportManage);
            this.panel8.Controls.Add(this.lbPlateDetailImportImportManage);
            this.panel8.Controls.Add(this.lbSerialDetailImportImportManage);
            this.panel8.Controls.Add(this.txtSerialPaletImportManage);
            this.panel8.Location = new System.Drawing.Point(7, 7);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(467, 145);
            this.panel8.TabIndex = 0;
            // 
            // txtNameBelongImport
            // 
            this.txtNameBelongImport.Location = new System.Drawing.Point(179, 15);
            this.txtNameBelongImport.Name = "txtNameBelongImport";
            this.txtNameBelongImport.ReadOnly = true;
            this.txtNameBelongImport.Size = new System.Drawing.Size(148, 20);
            this.txtNameBelongImport.TabIndex = 48;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(64, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(109, 13);
            this.label24.TabIndex = 47;
            this.label24.Text = "Thuộc phiếu nhập";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStatusPlateImportManage
            // 
            this.txtStatusPlateImportManage.Location = new System.Drawing.Point(310, 120);
            this.txtStatusPlateImportManage.Name = "txtStatusPlateImportManage";
            this.txtStatusPlateImportManage.Size = new System.Drawing.Size(143, 20);
            this.txtStatusPlateImportManage.TabIndex = 46;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(242, 124);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Tình trạng";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpEndDatePaletImportManage
            // 
            this.dtpEndDatePaletImportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpEndDatePaletImportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDatePaletImportManage.Location = new System.Drawing.Point(310, 95);
            this.dtpEndDatePaletImportManage.Name = "dtpEndDatePaletImportManage";
            this.dtpEndDatePaletImportManage.Size = new System.Drawing.Size(143, 20);
            this.dtpEndDatePaletImportManage.TabIndex = 44;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(227, 99);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "Ngày kết thúc";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpBeginDatePaletImportManage
            // 
            this.dtpBeginDatePaletImportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpBeginDatePaletImportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginDatePaletImportManage.Location = new System.Drawing.Point(310, 70);
            this.dtpBeginDatePaletImportManage.Name = "dtpBeginDatePaletImportManage";
            this.dtpBeginDatePaletImportManage.Size = new System.Drawing.Size(143, 20);
            this.dtpBeginDatePaletImportManage.TabIndex = 42;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(227, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "Ngày bắt đầu";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbWhDetailTabImportManage
            // 
            this.cbWhDetailTabImportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhDetailTabImportManage.FormattingEnabled = true;
            this.cbWhDetailTabImportManage.Location = new System.Drawing.Point(310, 45);
            this.cbWhDetailTabImportManage.Name = "cbWhDetailTabImportManage";
            this.cbWhDetailTabImportManage.Size = new System.Drawing.Size(143, 21);
            this.cbWhDetailTabImportManage.TabIndex = 40;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(240, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 39;
            this.label20.Text = "Kho hàng";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteDetailImportImportManage
            // 
            this.txtNoteDetailImportImportManage.Location = new System.Drawing.Point(78, 120);
            this.txtNoteDetailImportImportManage.Name = "txtNoteDetailImportImportManage";
            this.txtNoteDetailImportImportManage.Size = new System.Drawing.Size(148, 20);
            this.txtNoteDetailImportImportManage.TabIndex = 38;
            // 
            // dtpCreateDateSubTabImportManage
            // 
            this.dtpCreateDateSubTabImportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpCreateDateSubTabImportManage.Enabled = false;
            this.dtpCreateDateSubTabImportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDateSubTabImportManage.Location = new System.Drawing.Point(78, 95);
            this.dtpCreateDateSubTabImportManage.Name = "dtpCreateDateSubTabImportManage";
            this.dtpCreateDateSubTabImportManage.Size = new System.Drawing.Size(148, 20);
            this.dtpCreateDateSubTabImportManage.TabIndex = 37;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(10, 124);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Ghi chú";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(8, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Ngày nhập";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtLincensePlateImportManage
            // 
            this.txtLincensePlateImportManage.Location = new System.Drawing.Point(78, 70);
            this.txtLincensePlateImportManage.Name = "txtLincensePlateImportManage";
            this.txtLincensePlateImportManage.Size = new System.Drawing.Size(148, 20);
            this.txtLincensePlateImportManage.TabIndex = 11;
            // 
            // lbPlateDetailImportImportManage
            // 
            this.lbPlateDetailImportImportManage.Location = new System.Drawing.Point(8, 74);
            this.lbPlateDetailImportImportManage.Name = "lbPlateDetailImportImportManage";
            this.lbPlateDetailImportImportManage.Size = new System.Drawing.Size(59, 13);
            this.lbPlateDetailImportImportManage.TabIndex = 10;
            this.lbPlateDetailImportImportManage.Text = "Biển số xe";
            this.lbPlateDetailImportImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbSerialDetailImportImportManage
            // 
            this.lbSerialDetailImportImportManage.Location = new System.Drawing.Point(8, 49);
            this.lbSerialDetailImportImportManage.Name = "lbSerialDetailImportImportManage";
            this.lbSerialDetailImportImportManage.Size = new System.Drawing.Size(56, 13);
            this.lbSerialDetailImportImportManage.TabIndex = 9;
            this.lbSerialDetailImportImportManage.Text = "Số seri";
            this.lbSerialDetailImportImportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSerialPaletImportManage
            // 
            this.txtSerialPaletImportManage.Location = new System.Drawing.Point(77, 45);
            this.txtSerialPaletImportManage.Name = "txtSerialPaletImportManage";
            this.txtSerialPaletImportManage.Size = new System.Drawing.Size(148, 20);
            this.txtSerialPaletImportManage.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGridViewImportImportManage);
            this.panel5.Location = new System.Drawing.Point(0, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(442, 435);
            this.panel5.TabIndex = 0;
            // 
            // dataGridViewImportImportManage
            // 
            this.dataGridViewImportImportManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewImportImportManage.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewImportImportManage.Name = "dataGridViewImportImportManage";
            this.dataGridViewImportImportManage.Size = new System.Drawing.Size(435, 428);
            this.dataGridViewImportImportManage.TabIndex = 0;
            this.dataGridViewImportImportManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewImportImportManage_CellClick);
            // 
            // tabExportManagement
            // 
            this.tabExportManagement.Controls.Add(this.panel11);
            this.tabExportManagement.Controls.Add(this.panel10);
            this.tabExportManagement.Location = new System.Drawing.Point(4, 22);
            this.tabExportManagement.Name = "tabExportManagement";
            this.tabExportManagement.Size = new System.Drawing.Size(939, 439);
            this.tabExportManagement.TabIndex = 4;
            this.tabExportManagement.Text = "Quản lý xuất kho";
            this.tabExportManagement.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.tabControl1);
            this.panel11.Location = new System.Drawing.Point(448, 4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(492, 435);
            this.panel11.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabInfoExport);
            this.tabControl1.Controls.Add(this.tabInfoDetailExport);
            this.tabControl1.Location = new System.Drawing.Point(4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(485, 428);
            this.tabControl1.TabIndex = 0;
            // 
            // tabInfoExport
            // 
            this.tabInfoExport.Controls.Add(this.label46);
            this.tabInfoExport.Controls.Add(this.label11);
            this.tabInfoExport.Controls.Add(this.btnUpdateExport);
            this.tabInfoExport.Controls.Add(this.btnAddNewExport);
            this.tabInfoExport.Controls.Add(this.panel12);
            this.tabInfoExport.Location = new System.Drawing.Point(4, 22);
            this.tabInfoExport.Name = "tabInfoExport";
            this.tabInfoExport.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfoExport.Size = new System.Drawing.Size(477, 402);
            this.tabInfoExport.TabIndex = 0;
            this.tabInfoExport.Text = "Thông tin phiếu xuất";
            this.tabInfoExport.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 363);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(372, 13);
            this.label46.TabIndex = 6;
            this.label46.Text = "Bạn bắt buộc phải nhập mới thông tin phiếu trước khi thêm \"Chi tiết xuất kho\"";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 383);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(360, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Bạn bắt buộc phải chọn phiếu xuất mới có thể chọn hàng hóa để xuất kho";
            // 
            // btnUpdateExport
            // 
            this.btnUpdateExport.Location = new System.Drawing.Point(250, 214);
            this.btnUpdateExport.Name = "btnUpdateExport";
            this.btnUpdateExport.Size = new System.Drawing.Size(155, 50);
            this.btnUpdateExport.TabIndex = 4;
            this.btnUpdateExport.Text = "Cập nhật phiếu xuất";
            this.btnUpdateExport.UseVisualStyleBackColor = true;
            this.btnUpdateExport.Click += new System.EventHandler(this.btnUpdateExport_Click);
            // 
            // btnAddNewExport
            // 
            this.btnAddNewExport.Location = new System.Drawing.Point(103, 214);
            this.btnAddNewExport.Name = "btnAddNewExport";
            this.btnAddNewExport.Size = new System.Drawing.Size(141, 49);
            this.btnAddNewExport.TabIndex = 3;
            this.btnAddNewExport.Text = "Thêm mới phiếu xuất";
            this.btnAddNewExport.UseVisualStyleBackColor = true;
            this.btnAddNewExport.Click += new System.EventHandler(this.btnAddNewExport_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.cbImWhExportManage);
            this.panel12.Controls.Add(this.label12);
            this.panel12.Controls.Add(this.txtCreateUserExportManage);
            this.panel12.Controls.Add(this.label25);
            this.panel12.Controls.Add(this.txtNoteExportManage);
            this.panel12.Controls.Add(this.label26);
            this.panel12.Controls.Add(this.label27);
            this.panel12.Controls.Add(this.cbTypeProductExportManage);
            this.panel12.Controls.Add(this.txtQuantityPaletExportManage);
            this.panel12.Controls.Add(this.dtpUpdateDateExportManage);
            this.panel12.Controls.Add(this.label28);
            this.panel12.Controls.Add(this.cbCurrentWhExportManage);
            this.panel12.Controls.Add(this.label29);
            this.panel12.Controls.Add(this.dtpCreateDateExportManage);
            this.panel12.Controls.Add(this.label30);
            this.panel12.Controls.Add(this.label31);
            this.panel12.Controls.Add(this.txtSerialExportManage);
            this.panel12.Controls.Add(this.label32);
            this.panel12.Controls.Add(this.lbNameExportManage);
            this.panel12.Controls.Add(this.txtNameExportExportManage);
            this.panel12.Location = new System.Drawing.Point(7, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(464, 188);
            this.panel12.TabIndex = 0;
            // 
            // cbImWhExportManage
            // 
            this.cbImWhExportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImWhExportManage.Enabled = false;
            this.cbImWhExportManage.FormattingEnabled = true;
            this.cbImWhExportManage.Location = new System.Drawing.Point(79, 80);
            this.cbImWhExportManage.Name = "cbImWhExportManage";
            this.cbImWhExportManage.Size = new System.Drawing.Size(147, 21);
            this.cbImWhExportManage.TabIndex = 69;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(12, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 68;
            this.label12.Text = "Kho nhập";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCreateUserExportManage
            // 
            this.txtCreateUserExportManage.Location = new System.Drawing.Point(308, 110);
            this.txtCreateUserExportManage.Name = "txtCreateUserExportManage";
            this.txtCreateUserExportManage.ReadOnly = true;
            this.txtCreateUserExportManage.Size = new System.Drawing.Size(143, 20);
            this.txtCreateUserExportManage.TabIndex = 67;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(250, 112);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 13);
            this.label25.TabIndex = 66;
            this.label25.Text = "Người tạo";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteExportManage
            // 
            this.txtNoteExportManage.Location = new System.Drawing.Point(309, 80);
            this.txtNoteExportManage.Name = "txtNoteExportManage";
            this.txtNoteExportManage.Size = new System.Drawing.Size(143, 20);
            this.txtNoteExportManage.TabIndex = 65;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(250, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 64;
            this.label26.Text = "Ghi chú";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(248, 53);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 63;
            this.label27.Text = "Số lượng";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbTypeProductExportManage
            // 
            this.cbTypeProductExportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeProductExportManage.FormattingEnabled = true;
            this.cbTypeProductExportManage.Location = new System.Drawing.Point(309, 16);
            this.cbTypeProductExportManage.Name = "cbTypeProductExportManage";
            this.cbTypeProductExportManage.Size = new System.Drawing.Size(142, 21);
            this.cbTypeProductExportManage.TabIndex = 62;
            // 
            // txtQuantityPaletExportManage
            // 
            this.txtQuantityPaletExportManage.Location = new System.Drawing.Point(309, 50);
            this.txtQuantityPaletExportManage.Name = "txtQuantityPaletExportManage";
            this.txtQuantityPaletExportManage.ReadOnly = true;
            this.txtQuantityPaletExportManage.Size = new System.Drawing.Size(143, 20);
            this.txtQuantityPaletExportManage.TabIndex = 61;
            // 
            // dtpUpdateDateExportManage
            // 
            this.dtpUpdateDateExportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpUpdateDateExportManage.Enabled = false;
            this.dtpUpdateDateExportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpUpdateDateExportManage.Location = new System.Drawing.Point(309, 140);
            this.dtpUpdateDateExportManage.Name = "dtpUpdateDateExportManage";
            this.dtpUpdateDateExportManage.Size = new System.Drawing.Size(143, 20);
            this.dtpUpdateDateExportManage.TabIndex = 60;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(245, 143);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(59, 13);
            this.label28.TabIndex = 59;
            this.label28.Text = "Cập nhật";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbCurrentWhExportManage
            // 
            this.cbCurrentWhExportManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCurrentWhExportManage.FormattingEnabled = true;
            this.cbCurrentWhExportManage.Location = new System.Drawing.Point(80, 50);
            this.cbCurrentWhExportManage.Name = "cbCurrentWhExportManage";
            this.cbCurrentWhExportManage.Size = new System.Drawing.Size(147, 21);
            this.cbCurrentWhExportManage.TabIndex = 56;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(12, 54);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 55;
            this.label29.Text = "Kho xuất";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpCreateDateExportManage
            // 
            this.dtpCreateDateExportManage.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpCreateDateExportManage.Enabled = false;
            this.dtpCreateDateExportManage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDateExportManage.Location = new System.Drawing.Point(80, 140);
            this.dtpCreateDateExportManage.Name = "dtpCreateDateExportManage";
            this.dtpCreateDateExportManage.Size = new System.Drawing.Size(148, 20);
            this.dtpCreateDateExportManage.TabIndex = 53;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(250, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(54, 13);
            this.label30.TabIndex = 52;
            this.label30.Text = "Loại";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(15, 143);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(62, 13);
            this.label31.TabIndex = 51;
            this.label31.Text = "Ngày xuất";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSerialExportManage
            // 
            this.txtSerialExportManage.Location = new System.Drawing.Point(80, 110);
            this.txtSerialExportManage.Name = "txtSerialExportManage";
            this.txtSerialExportManage.Size = new System.Drawing.Size(148, 20);
            this.txtSerialExportManage.TabIndex = 50;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(18, 112);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 13);
            this.label32.TabIndex = 49;
            this.label32.Text = "Seri phiếu";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbNameExportManage
            // 
            this.lbNameExportManage.Location = new System.Drawing.Point(21, 23);
            this.lbNameExportManage.Name = "lbNameExportManage";
            this.lbNameExportManage.Size = new System.Drawing.Size(56, 13);
            this.lbNameExportManage.TabIndex = 48;
            this.lbNameExportManage.Text = "Tên phiếu";
            this.lbNameExportManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNameExportExportManage
            // 
            this.txtNameExportExportManage.Location = new System.Drawing.Point(79, 20);
            this.txtNameExportExportManage.Name = "txtNameExportExportManage";
            this.txtNameExportExportManage.Size = new System.Drawing.Size(148, 20);
            this.txtNameExportExportManage.TabIndex = 47;
            // 
            // tabInfoDetailExport
            // 
            this.tabInfoDetailExport.Controls.Add(this.panel13);
            this.tabInfoDetailExport.Controls.Add(this.panel14);
            this.tabInfoDetailExport.Location = new System.Drawing.Point(4, 22);
            this.tabInfoDetailExport.Name = "tabInfoDetailExport";
            this.tabInfoDetailExport.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfoDetailExport.Size = new System.Drawing.Size(477, 402);
            this.tabInfoDetailExport.TabIndex = 1;
            this.tabInfoDetailExport.Text = "Chi tiết xuất kho";
            this.tabInfoDetailExport.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.btnUpdateDetailExport);
            this.panel13.Controls.Add(this.btnAddNewDetailExport);
            this.panel13.Controls.Add(this.dataGridViewDetailExportAndPalet);
            this.panel13.Location = new System.Drawing.Point(4, 156);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(467, 238);
            this.panel13.TabIndex = 1;
            // 
            // btnUpdateDetailExport
            // 
            this.btnUpdateDetailExport.Location = new System.Drawing.Point(243, 210);
            this.btnUpdateDetailExport.Name = "btnUpdateDetailExport";
            this.btnUpdateDetailExport.Size = new System.Drawing.Size(100, 23);
            this.btnUpdateDetailExport.TabIndex = 2;
            this.btnUpdateDetailExport.Text = "Cập nhật chi tiết";
            this.btnUpdateDetailExport.UseVisualStyleBackColor = true;
            this.btnUpdateDetailExport.Click += new System.EventHandler(this.btnUpdateDetailExport_Click);
            // 
            // btnAddNewDetailExport
            // 
            this.btnAddNewDetailExport.Location = new System.Drawing.Point(123, 210);
            this.btnAddNewDetailExport.Name = "btnAddNewDetailExport";
            this.btnAddNewDetailExport.Size = new System.Drawing.Size(98, 23);
            this.btnAddNewDetailExport.TabIndex = 1;
            this.btnAddNewDetailExport.Text = "Thêm mới chi tiết";
            this.btnAddNewDetailExport.UseVisualStyleBackColor = true;
            this.btnAddNewDetailExport.Click += new System.EventHandler(this.btnAddNewDetailExport_Click);
            // 
            // dataGridViewDetailExportAndPalet
            // 
            this.dataGridViewDetailExportAndPalet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetailExportAndPalet.Location = new System.Drawing.Point(2, 2);
            this.dataGridViewDetailExportAndPalet.Name = "dataGridViewDetailExportAndPalet";
            this.dataGridViewDetailExportAndPalet.Size = new System.Drawing.Size(462, 192);
            this.dataGridViewDetailExportAndPalet.TabIndex = 0;
            this.dataGridViewDetailExportAndPalet.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetailExportAndPalet_CellClick);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.txtNameBelongExport);
            this.panel14.Controls.Add(this.label35);
            this.panel14.Controls.Add(this.txtStatusPaletExport);
            this.panel14.Controls.Add(this.label36);
            this.panel14.Controls.Add(this.dtpEndDatePaletExport);
            this.panel14.Controls.Add(this.label37);
            this.panel14.Controls.Add(this.dtpBeginDatePaletExport);
            this.panel14.Controls.Add(this.label38);
            this.panel14.Controls.Add(this.cbCurentWhPaletExport);
            this.panel14.Controls.Add(this.label39);
            this.panel14.Controls.Add(this.txtNoteSubTabExport);
            this.panel14.Controls.Add(this.dtpCreateDateSubTabExport);
            this.panel14.Controls.Add(this.label40);
            this.panel14.Controls.Add(this.label41);
            this.panel14.Controls.Add(this.txtLicensePlateExportManage);
            this.panel14.Controls.Add(this.label42);
            this.panel14.Controls.Add(this.label43);
            this.panel14.Controls.Add(this.txtSerialPaletExportManage);
            this.panel14.Location = new System.Drawing.Point(7, 7);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(467, 145);
            this.panel14.TabIndex = 0;
            // 
            // txtNameBelongExport
            // 
            this.txtNameBelongExport.Location = new System.Drawing.Point(179, 15);
            this.txtNameBelongExport.Name = "txtNameBelongExport";
            this.txtNameBelongExport.ReadOnly = true;
            this.txtNameBelongExport.Size = new System.Drawing.Size(148, 20);
            this.txtNameBelongExport.TabIndex = 48;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(64, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(109, 13);
            this.label35.TabIndex = 47;
            this.label35.Text = "Thuộc phiếu xuất";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStatusPaletExport
            // 
            this.txtStatusPaletExport.Location = new System.Drawing.Point(310, 120);
            this.txtStatusPaletExport.Name = "txtStatusPaletExport";
            this.txtStatusPaletExport.Size = new System.Drawing.Size(143, 20);
            this.txtStatusPaletExport.TabIndex = 46;
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(242, 124);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(64, 13);
            this.label36.TabIndex = 45;
            this.label36.Text = "Tình trạng";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpEndDatePaletExport
            // 
            this.dtpEndDatePaletExport.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpEndDatePaletExport.Enabled = false;
            this.dtpEndDatePaletExport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDatePaletExport.Location = new System.Drawing.Point(310, 95);
            this.dtpEndDatePaletExport.Name = "dtpEndDatePaletExport";
            this.dtpEndDatePaletExport.Size = new System.Drawing.Size(143, 20);
            this.dtpEndDatePaletExport.TabIndex = 44;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(227, 99);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(80, 13);
            this.label37.TabIndex = 43;
            this.label37.Text = "Ngày kết thúc";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dtpBeginDatePaletExport
            // 
            this.dtpBeginDatePaletExport.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpBeginDatePaletExport.Enabled = false;
            this.dtpBeginDatePaletExport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginDatePaletExport.Location = new System.Drawing.Point(310, 70);
            this.dtpBeginDatePaletExport.Name = "dtpBeginDatePaletExport";
            this.dtpBeginDatePaletExport.Size = new System.Drawing.Size(143, 20);
            this.dtpBeginDatePaletExport.TabIndex = 42;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(227, 74);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(80, 13);
            this.label38.TabIndex = 41;
            this.label38.Text = "Ngày bắt đầu";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbCurentWhPaletExport
            // 
            this.cbCurentWhPaletExport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCurentWhPaletExport.Enabled = false;
            this.cbCurentWhPaletExport.FormattingEnabled = true;
            this.cbCurentWhPaletExport.Location = new System.Drawing.Point(310, 45);
            this.cbCurentWhPaletExport.Name = "cbCurentWhPaletExport";
            this.cbCurentWhPaletExport.Size = new System.Drawing.Size(143, 21);
            this.cbCurentWhPaletExport.TabIndex = 40;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(240, 49);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 13);
            this.label39.TabIndex = 39;
            this.label39.Text = "Kho hàng";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteSubTabExport
            // 
            this.txtNoteSubTabExport.Location = new System.Drawing.Point(78, 120);
            this.txtNoteSubTabExport.Name = "txtNoteSubTabExport";
            this.txtNoteSubTabExport.Size = new System.Drawing.Size(148, 20);
            this.txtNoteSubTabExport.TabIndex = 38;
            // 
            // dtpCreateDateSubTabExport
            // 
            this.dtpCreateDateSubTabExport.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpCreateDateSubTabExport.Enabled = false;
            this.dtpCreateDateSubTabExport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDateSubTabExport.Location = new System.Drawing.Point(78, 95);
            this.dtpCreateDateSubTabExport.Name = "dtpCreateDateSubTabExport";
            this.dtpCreateDateSubTabExport.Size = new System.Drawing.Size(148, 20);
            this.dtpCreateDateSubTabExport.TabIndex = 37;
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(10, 124);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(59, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Ghi chú";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(8, 99);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(62, 13);
            this.label41.TabIndex = 12;
            this.label41.Text = "Ngày nhập";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtLicensePlateExportManage
            // 
            this.txtLicensePlateExportManage.Location = new System.Drawing.Point(78, 70);
            this.txtLicensePlateExportManage.Name = "txtLicensePlateExportManage";
            this.txtLicensePlateExportManage.Size = new System.Drawing.Size(148, 20);
            this.txtLicensePlateExportManage.TabIndex = 11;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(8, 74);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(59, 13);
            this.label42.TabIndex = 10;
            this.label42.Text = "Biển số xe";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(8, 49);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 13);
            this.label43.TabIndex = 9;
            this.label43.Text = "Số seri";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSerialPaletExportManage
            // 
            this.txtSerialPaletExportManage.Location = new System.Drawing.Point(77, 45);
            this.txtSerialPaletExportManage.Name = "txtSerialPaletExportManage";
            this.txtSerialPaletExportManage.ReadOnly = true;
            this.txtSerialPaletExportManage.Size = new System.Drawing.Size(148, 20);
            this.txtSerialPaletExportManage.TabIndex = 2;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.dataGridViewPaletExportManage);
            this.panel10.Controls.Add(this.btnSearchPaletExportManage);
            this.panel10.Controls.Add(this.label45);
            this.panel10.Controls.Add(this.textBox19);
            this.panel10.Controls.Add(this.label44);
            this.panel10.Controls.Add(this.dataGridViewListExport);
            this.panel10.Location = new System.Drawing.Point(0, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(442, 435);
            this.panel10.TabIndex = 1;
            // 
            // dataGridViewPaletExportManage
            // 
            this.dataGridViewPaletExportManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPaletExportManage.Location = new System.Drawing.Point(4, 294);
            this.dataGridViewPaletExportManage.Name = "dataGridViewPaletExportManage";
            this.dataGridViewPaletExportManage.Size = new System.Drawing.Size(435, 138);
            this.dataGridViewPaletExportManage.TabIndex = 5;
            this.dataGridViewPaletExportManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPaletExportManage_CellClick);
            // 
            // btnSearchPaletExportManage
            // 
            this.btnSearchPaletExportManage.Location = new System.Drawing.Point(118, 268);
            this.btnSearchPaletExportManage.Name = "btnSearchPaletExportManage";
            this.btnSearchPaletExportManage.Size = new System.Drawing.Size(75, 23);
            this.btnSearchPaletExportManage.TabIndex = 4;
            this.btnSearchPaletExportManage.Text = "Tìm kiếm";
            this.btnSearchPaletExportManage.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(8, 252);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(134, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Danh sách hàng trong kho";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(10, 270);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 20);
            this.textBox19.TabIndex = 2;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(9, 4);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(111, 13);
            this.label44.TabIndex = 1;
            this.label44.Text = "Danh sách phiếu xuất";
            // 
            // dataGridViewListExport
            // 
            this.dataGridViewListExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListExport.Location = new System.Drawing.Point(4, 24);
            this.dataGridViewListExport.Name = "dataGridViewListExport";
            this.dataGridViewListExport.Size = new System.Drawing.Size(435, 216);
            this.dataGridViewListExport.TabIndex = 0;
            this.dataGridViewListExport.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewListExport_CellClick);
            // 
            // tbUserManage
            // 
            this.tbUserManage.Controls.Add(this.dataGridViewUserManage);
            this.tbUserManage.Controls.Add(this.panel3);
            this.tbUserManage.Location = new System.Drawing.Point(4, 22);
            this.tbUserManage.Name = "tbUserManage";
            this.tbUserManage.Size = new System.Drawing.Size(939, 439);
            this.tbUserManage.TabIndex = 5;
            this.tbUserManage.Text = "Quản lý tài khoản";
            this.tbUserManage.UseVisualStyleBackColor = true;
            // 
            // dataGridViewUserManage
            // 
            this.dataGridViewUserManage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUserManage.Location = new System.Drawing.Point(-6, 146);
            this.dataGridViewUserManage.Name = "dataGridViewUserManage";
            this.dataGridViewUserManage.ReadOnly = true;
            this.dataGridViewUserManage.Size = new System.Drawing.Size(947, 295);
            this.dataGridViewUserManage.TabIndex = 10;
            this.dataGridViewUserManage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUserManage_CellClick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Controls.Add(this.dtpUpdateDate);
            this.panel3.Controls.Add(this.dtpCreateDate);
            this.panel3.Controls.Add(this.lbUpdateDateUserManage);
            this.panel3.Controls.Add(this.lbCreateDateUserManage);
            this.panel3.Controls.Add(this.txtNoteUserManage);
            this.panel3.Controls.Add(this.txtClientUserManage);
            this.panel3.Controls.Add(this.lbNoteUserManage);
            this.panel3.Controls.Add(this.lbClientIdUserManage);
            this.panel3.Controls.Add(this.cbWhUserManage);
            this.panel3.Controls.Add(this.txtPasswordUserManage);
            this.panel3.Controls.Add(this.txtUsernameUserManage);
            this.panel3.Controls.Add(this.lbWHUserManage);
            this.panel3.Controls.Add(this.lbPasswordUserManage);
            this.panel3.Controls.Add(this.lbUsernameUserManage);
            this.panel3.Controls.Add(this.lbTitleUserManage);
            this.panel3.Location = new System.Drawing.Point(-2, -2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(947, 146);
            this.panel3.TabIndex = 9;
            // 
            // dtpUpdateDate
            // 
            this.dtpUpdateDate.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpUpdateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpUpdateDate.Location = new System.Drawing.Point(374, 93);
            this.dtpUpdateDate.Name = "dtpUpdateDate";
            this.dtpUpdateDate.Size = new System.Drawing.Size(167, 20);
            this.dtpUpdateDate.TabIndex = 24;
            // 
            // dtpCreateDate
            // 
            this.dtpCreateDate.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCreateDate.Location = new System.Drawing.Point(374, 62);
            this.dtpCreateDate.Name = "dtpCreateDate";
            this.dtpCreateDate.Size = new System.Drawing.Size(167, 20);
            this.dtpCreateDate.TabIndex = 23;
            // 
            // lbUpdateDateUserManage
            // 
            this.lbUpdateDateUserManage.Location = new System.Drawing.Point(285, 96);
            this.lbUpdateDateUserManage.Name = "lbUpdateDateUserManage";
            this.lbUpdateDateUserManage.Size = new System.Drawing.Size(83, 13);
            this.lbUpdateDateUserManage.TabIndex = 22;
            this.lbUpdateDateUserManage.Text = "Ngày sửa cuối";
            this.lbUpdateDateUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbCreateDateUserManage
            // 
            this.lbCreateDateUserManage.Location = new System.Drawing.Point(290, 65);
            this.lbCreateDateUserManage.Name = "lbCreateDateUserManage";
            this.lbCreateDateUserManage.Size = new System.Drawing.Size(78, 15);
            this.lbCreateDateUserManage.TabIndex = 21;
            this.lbCreateDateUserManage.Text = "Ngày tạo";
            this.lbCreateDateUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNoteUserManage
            // 
            this.txtNoteUserManage.Location = new System.Drawing.Point(374, 34);
            this.txtNoteUserManage.Name = "txtNoteUserManage";
            this.txtNoteUserManage.Size = new System.Drawing.Size(167, 20);
            this.txtNoteUserManage.TabIndex = 13;
            // 
            // txtClientUserManage
            // 
            this.txtClientUserManage.Location = new System.Drawing.Point(109, 93);
            this.txtClientUserManage.Name = "txtClientUserManage";
            this.txtClientUserManage.Size = new System.Drawing.Size(170, 20);
            this.txtClientUserManage.TabIndex = 12;
            // 
            // lbNoteUserManage
            // 
            this.lbNoteUserManage.Location = new System.Drawing.Point(294, 38);
            this.lbNoteUserManage.Name = "lbNoteUserManage";
            this.lbNoteUserManage.Size = new System.Drawing.Size(74, 13);
            this.lbNoteUserManage.TabIndex = 10;
            this.lbNoteUserManage.Text = "Ghi chú";
            this.lbNoteUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbClientIdUserManage
            // 
            this.lbClientIdUserManage.Location = new System.Drawing.Point(25, 96);
            this.lbClientIdUserManage.Name = "lbClientIdUserManage";
            this.lbClientIdUserManage.Size = new System.Drawing.Size(78, 15);
            this.lbClientIdUserManage.TabIndex = 9;
            this.lbClientIdUserManage.Text = "Id Máy trạm";
            this.lbClientIdUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbWhUserManage
            // 
            this.cbWhUserManage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWhUserManage.FormattingEnabled = true;
            this.cbWhUserManage.Location = new System.Drawing.Point(109, 119);
            this.cbWhUserManage.Name = "cbWhUserManage";
            this.cbWhUserManage.Size = new System.Drawing.Size(170, 21);
            this.cbWhUserManage.TabIndex = 8;
            // 
            // txtPasswordUserManage
            // 
            this.txtPasswordUserManage.Location = new System.Drawing.Point(109, 62);
            this.txtPasswordUserManage.Name = "txtPasswordUserManage";
            this.txtPasswordUserManage.Size = new System.Drawing.Size(170, 20);
            this.txtPasswordUserManage.TabIndex = 6;
            // 
            // txtUsernameUserManage
            // 
            this.txtUsernameUserManage.Location = new System.Drawing.Point(109, 34);
            this.txtUsernameUserManage.Name = "txtUsernameUserManage";
            this.txtUsernameUserManage.Size = new System.Drawing.Size(170, 20);
            this.txtUsernameUserManage.TabIndex = 5;
            // 
            // lbWHUserManage
            // 
            this.lbWHUserManage.Location = new System.Drawing.Point(11, 123);
            this.lbWHUserManage.Name = "lbWHUserManage";
            this.lbWHUserManage.Size = new System.Drawing.Size(92, 13);
            this.lbWHUserManage.TabIndex = 4;
            this.lbWHUserManage.Text = "Nhà kho";
            this.lbWHUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbPasswordUserManage
            // 
            this.lbPasswordUserManage.Location = new System.Drawing.Point(40, 65);
            this.lbPasswordUserManage.Name = "lbPasswordUserManage";
            this.lbPasswordUserManage.Size = new System.Drawing.Size(63, 13);
            this.lbPasswordUserManage.TabIndex = 2;
            this.lbPasswordUserManage.Text = "Mật khẩu";
            this.lbPasswordUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbUsernameUserManage
            // 
            this.lbUsernameUserManage.Location = new System.Drawing.Point(16, 37);
            this.lbUsernameUserManage.Name = "lbUsernameUserManage";
            this.lbUsernameUserManage.Size = new System.Drawing.Size(87, 13);
            this.lbUsernameUserManage.TabIndex = 1;
            this.lbUsernameUserManage.Text = "Tên đăng nhập";
            this.lbUsernameUserManage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbTitleUserManage
            // 
            this.lbTitleUserManage.AutoSize = true;
            this.lbTitleUserManage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitleUserManage.Location = new System.Drawing.Point(13, 4);
            this.lbTitleUserManage.Name = "lbTitleUserManage";
            this.lbTitleUserManage.Size = new System.Drawing.Size(107, 13);
            this.lbTitleUserManage.TabIndex = 0;
            this.lbTitleUserManage.Text = "Quản lý tài khoản";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(73, 48);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 21);
            this.comboBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(74, 14);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(143, 20);
            this.textBox2.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(0, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 8;
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 10;
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 11;
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(72, 86);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(143, 20);
            this.dateTimePicker1.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 25;
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(72, 153);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(143, 21);
            this.comboBox2.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(9, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 27;
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(72, 122);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(143, 20);
            this.textBox1.TabIndex = 28;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(308, 14);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(143, 20);
            this.textBox3.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(228, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 30;
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(308, 48);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(143, 20);
            this.textBox4.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(228, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 32;
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(308, 85);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(143, 20);
            this.textBox5.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(228, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 34;
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(221, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 35;
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(308, 122);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(143, 20);
            this.dateTimePicker2.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(17, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 11;
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(91, 16);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(143, 21);
            this.comboBox3.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(17, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 13;
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(17, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 14;
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(91, 51);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(143, 20);
            this.textBox9.TabIndex = 15;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(91, 77);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(143, 20);
            this.textBox10.TabIndex = 16;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(238, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 37;
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(321, 16);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(143, 20);
            this.dateTimePicker4.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(238, 54);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 39;
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker5.Location = new System.Drawing.Point(321, 51);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(143, 20);
            this.dateTimePicker5.TabIndex = 40;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(4, 4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(460, 222);
            this.dataGridView2.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 539);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.pnOption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.pnOption.ResumeLayout(false);
            this.pnOption.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabBranchManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBranchManage)).EndInit();
            this.pnBranchManage.ResumeLayout(false);
            this.pnBranchManage.PerformLayout();
            this.tabWHManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWHManage)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tbTruckManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTruckManage)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPaletManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaletManage)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabImportManagement.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.subTabImportManage.ResumeLayout(false);
            this.tabImportImportManage.ResumeLayout(false);
            this.tabImportImportManage.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabDetailImportImportManage.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailImportAndPalet)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewImportImportManage)).EndInit();
            this.tabExportManagement.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabInfoExport.ResumeLayout(false);
            this.tabInfoExport.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.tabInfoDetailExport.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetailExportAndPalet)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPaletExportManage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListExport)).EndInit();
            this.tbUserManage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserManage)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnOption;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabBranchManage;
        private System.Windows.Forms.TabPage tbTruckManage;
        private System.Windows.Forms.DataGridView dataGridViewBranchManage;
        private System.Windows.Forms.Panel pnBranchManage;
        private System.Windows.Forms.TextBox txtNoteBranchManage;
        private System.Windows.Forms.TextBox txtPhoneBranchManage;
        private System.Windows.Forms.TextBox txtAddressBranchManage;
        private System.Windows.Forms.Label lbNoteBranch;
        private System.Windows.Forms.Label lbPhoneBranch;
        private System.Windows.Forms.Label lbAddressBranch;
        private System.Windows.Forms.TextBox txtAreaBranchManage;
        private System.Windows.Forms.TextBox txtBranchNameBranchManage;
        private System.Windows.Forms.Label lbAreaBranch;
        private System.Windows.Forms.Label lbBranchName;
        private System.Windows.Forms.Label lbTitleBranchManage;
        private System.Windows.Forms.DataGridView dataGridViewTruckManage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNoteTruckManage;
        private System.Windows.Forms.Label lbNoteTruck;
        private System.Windows.Forms.TextBox txtPhoneTruckManage;
        private System.Windows.Forms.TextBox txtAddressTruckManage;
        private System.Windows.Forms.Label lbPhoneTruck;
        private System.Windows.Forms.Label lbAddressTruck;
        private System.Windows.Forms.ComboBox cbWhTruckManage;
        private System.Windows.Forms.TextBox txtDriverTruckManage;
        private System.Windows.Forms.TextBox txtLicensePlateTruckManages;
        private System.Windows.Forms.Label lbWhTruckManage;
        private System.Windows.Forms.Label lbDriverTruckManage;
        private System.Windows.Forms.Label lbLicensePlateTruckManage;
        private System.Windows.Forms.Label lbTruckManage;
        private System.Windows.Forms.TabPage tabPaletManage;
        private System.Windows.Forms.DataGridView dataGridViewPaletManage;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtpEndDatePaletManage;
        private System.Windows.Forms.DateTimePicker dtpBeginDatePaletManage;
        private System.Windows.Forms.Label lbEndDatePaletManage;
        private System.Windows.Forms.Label lbBeginDatePaletManage;
        private System.Windows.Forms.TextBox txtStatusPaletManage;
        private System.Windows.Forms.TextBox txtSerialPaletManage;
        private System.Windows.Forms.Label lbStatusPaletManage;
        private System.Windows.Forms.Label lbWhPaletManage;
        private System.Windows.Forms.Label lbSerialPaletManage;
        private System.Windows.Forms.Label lbGoodsManage;
        private System.Windows.Forms.TabPage tabImportManagement;
        private System.Windows.Forms.TabPage tabExportManagement;
        private System.Windows.Forms.TabPage tbUserManage;
        private System.Windows.Forms.DataGridView dataGridViewUserManage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtNoteUserManage;
        private System.Windows.Forms.TextBox txtClientUserManage;
        private System.Windows.Forms.Label lbNoteUserManage;
        private System.Windows.Forms.Label lbClientIdUserManage;
        private System.Windows.Forms.ComboBox cbWhUserManage;
        private System.Windows.Forms.TextBox txtPasswordUserManage;
        private System.Windows.Forms.TextBox txtUsernameUserManage;
        private System.Windows.Forms.Label lbWHUserManage;
        private System.Windows.Forms.Label lbPasswordUserManage;
        private System.Windows.Forms.Label lbUsernameUserManage;
        private System.Windows.Forms.Label lbTitleUserManage;
        private System.Windows.Forms.TabPage tabWHManage;
        private System.Windows.Forms.DataGridView dataGridViewWHManage;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtPalletWHManage;
        private System.Windows.Forms.Label lbPalletWHManage;
        private System.Windows.Forms.TextBox txtNoteWHManage;
        private System.Windows.Forms.TextBox txtPhoneWHManage;
        private System.Windows.Forms.TextBox txtAddressWHManage;
        private System.Windows.Forms.Label lbNoteWHManage;
        private System.Windows.Forms.Label lbPhoneWHManage;
        private System.Windows.Forms.Label lbAddressWHManage;
        private System.Windows.Forms.TextBox txtNameWHManage;
        private System.Windows.Forms.Label lbWhIdManage;
        private System.Windows.Forms.Label lbNameWHManage;
        private System.Windows.Forms.Label lbTitleWHManage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWhIdWhManage;
        private System.Windows.Forms.DateTimePicker dtpUpdateDate;
        private System.Windows.Forms.DateTimePicker dtpCreateDate;
        private System.Windows.Forms.Label lbUpdateDateUserManage;
        private System.Windows.Forms.Label lbCreateDateUserManage;
        private System.Windows.Forms.TextBox txtLinkWSWHManage;
        private System.Windows.Forms.Label lbLinkWSWHManage;
        private System.Windows.Forms.Label lbAreaWHManage;
        private System.Windows.Forms.ComboBox cbAreaWHManage;
        private System.Windows.Forms.DateTimePicker dtpEndDateTruckManage;
        private System.Windows.Forms.DateTimePicker dtpBeginDateTruckManage;
        private System.Windows.Forms.Label lbEndDateTruckManage;
        private System.Windows.Forms.Label lbBeginDateTruckManage;
        private System.Windows.Forms.TextBox txtIdCardTruckManage;
        private System.Windows.Forms.Label lbIdCardTruckManage;
        private System.Windows.Forms.TextBox txtStatusTruckManage;
        private System.Windows.Forms.Label lbStatusTruckManage;
        private System.Windows.Forms.TextBox txtOwnerTruckManage;
        private System.Windows.Forms.Label lbOwnerTruckManage;
        private System.Windows.Forms.ComboBox cbWhPaletManage;
        private System.Windows.Forms.ComboBox cbIsHOWhManage;
        private System.Windows.Forms.Label lbIsHOWhManage;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabControl subTabImportManage;
        private System.Windows.Forms.TabPage tabDetailImportImportManage;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dataGridViewImportImportManage;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnUpdateDetailImportPalet;
        private System.Windows.Forms.Button btnAddNewDetailImportPalet;
        private System.Windows.Forms.DataGridView dataGridViewDetailImportAndPalet;
        private System.Windows.Forms.TextBox txtNoteDetailImportImportManage;
        private System.Windows.Forms.DateTimePicker dtpCreateDateSubTabImportManage;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLincensePlateImportManage;
        private System.Windows.Forms.Label lbPlateDetailImportImportManage;
        private System.Windows.Forms.Label lbSerialDetailImportImportManage;
        private System.Windows.Forms.TextBox txtSerialPaletImportManage;
        private System.Windows.Forms.TabPage tabImportImportManage;
        private System.Windows.Forms.Button btnUpdateImport;
        private System.Windows.Forms.Button btnAddNewImport;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtStatusPlateImportManage;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker dtpEndDatePaletImportManage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dtpBeginDatePaletImportManage;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbWhDetailTabImportManage;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox txtUsernameImportManage;
        private System.Windows.Forms.Label lbCreateUserImportManage;
        private System.Windows.Forms.TextBox txtNoteImportManage;
        private System.Windows.Forms.Label lbNoteImportManage;
        private System.Windows.Forms.Label lbQuantityPaletImportManage;
        private System.Windows.Forms.ComboBox cbProduceImportManage;
        private System.Windows.Forms.TextBox txtQuantityImportManage;
        private System.Windows.Forms.DateTimePicker dtpUpdateDateImportManage;
        private System.Windows.Forms.Label lbUpdateDateImportManage;
        private System.Windows.Forms.ComboBox cbWhExImportManage;
        private System.Windows.Forms.Label lbSourceWhImportManage;
        private System.Windows.Forms.DateTimePicker dtpCreateDateImportManage;
        private System.Windows.Forms.Label lbTypeProductImportManage;
        private System.Windows.Forms.Label lbCreateDateImportManage;
        private System.Windows.Forms.TextBox txtSerialImportManage;
        private System.Windows.Forms.Label lbSerialImportImportManage;
        private System.Windows.Forms.Label lbNameImportManage;
        private System.Windows.Forms.TextBox txtNameImportManage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cbWhCurrentImportManage;
        private System.Windows.Forms.Label lbCurrentWhImportManage;
        private System.Windows.Forms.TextBox txtUserIdImportManage;
        private System.Windows.Forms.TextBox txtNameBelongImport;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabInfoExport;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnUpdateExport;
        private System.Windows.Forms.Button btnAddNewExport;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox cbImWhExportManage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCreateUserExportManage;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtNoteExportManage;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cbTypeProductExportManage;
        private System.Windows.Forms.TextBox txtQuantityPaletExportManage;
        private System.Windows.Forms.DateTimePicker dtpUpdateDateExportManage;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbCurrentWhExportManage;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dtpCreateDateExportManage;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtSerialExportManage;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lbNameExportManage;
        private System.Windows.Forms.TextBox txtNameExportExportManage;
        private System.Windows.Forms.TabPage tabInfoDetailExport;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnUpdateDetailExport;
        private System.Windows.Forms.Button btnAddNewDetailExport;
        private System.Windows.Forms.DataGridView dataGridViewDetailExportAndPalet;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox txtNameBelongExport;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtStatusPaletExport;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker dtpEndDatePaletExport;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DateTimePicker dtpBeginDatePaletExport;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbCurentWhPaletExport;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtNoteSubTabExport;
        private System.Windows.Forms.DateTimePicker dtpCreateDateSubTabExport;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtLicensePlateExportManage;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtSerialPaletExportManage;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.DataGridView dataGridViewPaletExportManage;
        private System.Windows.Forms.Button btnSearchPaletExportManage;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DataGridView dataGridViewListExport;
        private System.Windows.Forms.Label label46;
    }
}