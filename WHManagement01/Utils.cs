﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data.Entity.Validation;
using System.Xml.Linq;
using System.Xml;

namespace WHManagement01
{
    static class Utils
    {
        #region VARIABLE REGION
        //private static string connectionString = ConfigurationManager.ConnectionStrings["WHManagement01.Properties.Settings.WHDBConnectionString"].ConnectionString;
        private static readonly HttpClient client = new HttpClient();
        private static String defaultDomain = "http://115.79.7.102:8082/";

        //login
        private static String loginDefaulUrl = defaultDomain + "api/user/login";

        //detail export
        private static String listDetailExportDefaulUrl = defaultDomain + "api/detailexport";
        private static String insertDetailExportDefaulUrl = defaultDomain + "api/detailexport/insertdetailexport";
        private static String updateDetailExportDefaulUrl = defaultDomain + "api/detailexport/updateexport";
        private static String deleteDetailExportDefaulUrl = defaultDomain + "api/detailexport/deleteexport";

        //export
        private static String listExportDefaulUrl = defaultDomain + "api/export";
        private static String insertExportDefaulUrl = defaultDomain + "api/export/insertexport";
        private static String updateExportDefaulUrl = defaultDomain + "api/export/updateexport";
        private static String deleteExportDefaulUrl = defaultDomain + "api/export/deleteexport";

        //detail import
        private static String listDetailImportDefaulUrl = defaultDomain + "api/detailimport";
        private static String insertDetailImportDefaulUrl = defaultDomain + "api/detailimport/insertdetailimport";
        private static String updateDetailImportDefaulUrl = defaultDomain + "api/detailimport/updateimport";
        private static String deleteDetailImportDefaulUrl = defaultDomain + "api/detailimport/deleteimport";

        //import
        private static String listImportDefaulUrl = defaultDomain + "api/import";
        private static String insertImportDefaulUrl = defaultDomain + "api/import/insertimport";
        private static String updateImportDefaulUrl = defaultDomain + "api/import/updateimport";
        private static String deleteImportDefaulUrl = defaultDomain + "api/import/deleteimport";

        //user
        private static String listUserDefaulUrl = defaultDomain + "api/user";
        private static String insertUserDefaulUrl = defaultDomain + "api/user/insertuser";
        private static String updateUserDefaulUrl = defaultDomain + "api/user/updateuser";
        private static String deleteUserDefaulUrl = defaultDomain + "api/user/deleteuser";

        //branch
        //private static String listBranchDefaulUrll = defaultDomain + "api/branch";
        //private static String insertBranchDefaulUrl = defaultDomain + "api/branch//insertbranch";
        //private static String updateBranchDefaulUrl = defaultDomain + "api/branch/updatebranch";
        //private static String deleteBranchDefaulUrl = defaultDomain + "api/branch/deletebranch";

        //warehouse
        private static String listWHDefaulUrl = defaultDomain + "api/warehouse";
        private static String insertWHDefaulUrl = defaultDomain + "api/warehouse/insertwarehouse";
        private static String updateWHDefaulUrl = defaultDomain + "api/warehouse/updatewarehouse";
        private static String deleteWHDefaulUrl = defaultDomain + "api/warehouse/deletewarehouse";

        //Truck
        private static String listTruckDefaulUrl = defaultDomain + "api/truck";
        private static String insertTruckDefaulUrl = defaultDomain + "api/truck/inserttruck";
        private static String updateTruckDefaulUrl = defaultDomain + "api/truck/updatetruck";
        private static String deleteTruckDefaulUrl = defaultDomain + "api/truck/deletetruck";

        //Palet
        private static String listPaletDefaulUrl = defaultDomain + "api/palet";
        private static String insertPaletDefaulUrl = defaultDomain + "api/palet/insertpalet";
        private static String updatePaletDefaulUrl = defaultDomain + "api/palet/updatepalet";
        private static String deletePaletDefaulUrl = defaultDomain + "api/palet/deletepalet";

        public static Boolean IS_HEAD_OFFICE = false;
        //public static String CURRENT_ID_USER = "";
        public static String CURRENT_USERNAME = "";
        public static String CURRENT_ID_WAREHOUSE = "";
        //public static String CURRENT_LINK_WS = "";

        public static string idNewDetailExport = "";
        public static string idNewDetailImport = "";
        public static string idNewPalet = "";
        #endregion

        #region READ WRITE CONFIG FILE REGION


        //public static DataSet getListDetailImportLocalByWhId(string idwarehouse)
        //{

        //}

        private static void changeAllApiLink(string newDomain)
        {
            if (!string.IsNullOrEmpty(newDomain))
            {
                //detail export
                listDetailExportDefaulUrl = newDomain + "api/detailexport";
                insertDetailExportDefaulUrl = newDomain + "api/detailexport/insertexport";
                updateDetailExportDefaulUrl = newDomain + "api/detailexport/updateexport";
                deleteDetailExportDefaulUrl = newDomain + "api/detailexport/deleteexport";

                //export
                listExportDefaulUrl = newDomain + "api/export";
                insertExportDefaulUrl = newDomain + "api/export/insertexport";
                updateExportDefaulUrl = newDomain + "api/export/updateexport";
                deleteExportDefaulUrl = newDomain + "api/export/deleteexport";

                //detail import
                listDetailImportDefaulUrl = newDomain + "api/detailimport";
                insertDetailImportDefaulUrl = newDomain + "api/detailimport/insertimport";
                updateDetailImportDefaulUrl = newDomain + "api/detailimport/updateimport";
                deleteDetailImportDefaulUrl = newDomain + "api/detailimport/deleteimport";

                //import
                listImportDefaulUrl = newDomain + "api/import";
                insertImportDefaulUrl = newDomain + "api/import/insertimport";
                updateImportDefaulUrl = newDomain + "api/import/updateimport";
                deleteImportDefaulUrl = newDomain + "api/import/deleteimport";

                //user
                listUserDefaulUrl = newDomain + "api/user";
                insertUserDefaulUrl = newDomain + "api/user/insertuser";
                updateUserDefaulUrl = newDomain + "api/user/updateuser";
                deleteUserDefaulUrl = newDomain + "api/user/deleteuser";

                //branch
                //listBranchDefaulUrll = newDomain + "api/branch";
                //insertBranchDefaulUrl = newDomain + "api/branch//insertbranch";
                //updateBranchDefaulUrl = newDomain + "api/branch/updatebranch";
                //deleteBranchDefaulUrl = newDomain + "api/branch/deletebranch";

                //warehouse
                listWHDefaulUrl = newDomain + "api/warehouse";
                insertWHDefaulUrl = newDomain + "api/warehouse/insertwarehouse";
                updateWHDefaulUrl = newDomain + "api/warehouse/updatewarehouse";
                deleteWHDefaulUrl = newDomain + "api/warehouse/deletewarehouse";

                //truck
                listTruckDefaulUrl = defaultDomain + "api/truck";
                insertTruckDefaulUrl = defaultDomain + "api/truck/inserttruck";
                updateTruckDefaulUrl = defaultDomain + "api/truck/updatetruck";
                deleteTruckDefaulUrl = defaultDomain + "api/truck/deletetruck";

                //palet
                listPaletDefaulUrl = defaultDomain + "api/palet";
                insertPaletDefaulUrl = defaultDomain + "api/palet/insertpalet";
                updatePaletDefaulUrl = defaultDomain + "api/palet/updatepalet";
                deletePaletDefaulUrl = defaultDomain + "api/palet/deletepalet";
            }
            initDefaultUrl(); //if newDomain empty, use default domain
        }

        private static string getUrlFromConfigFile(string field)
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;
            if (settings[field] != null)
            {
                return settings[field].Value;
            }
            else
            {
                return "";
            }
        }

        private static void setUrlToConfigFile(string field, string value)
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;

            if (settings[field] == null)
            {
                settings.Add(field, value);
            }
            else
            {
                settings.Remove(field);
                settings.Add(field, value);
            }

            configSettings.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static FileStream writer;

        public static string findWhNameByWhId(string id)
        {
            string result = "";
            if (File.Exists(@"Warehouse.xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@"Warehouse.xml");
                XmlElement root = doc.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("Id");
                foreach (XmlNode node in nodes)
                {
                    if (node.Value.Equals(id))
                    {
                        return node.ParentNode.SelectSingleNode("Name").Value.ToString();
                    }
                }
            }
            return result;
        }

        public static void initDefaultUrl()
        {
            setUrlToConfigFile("loginDefaulUrl", loginDefaulUrl);

            //detail export
            setUrlToConfigFile("listDetailExportDefaulUrl", listDetailExportDefaulUrl);
            setUrlToConfigFile("insertDetailExportDefaulUrl", insertDetailExportDefaulUrl);
            setUrlToConfigFile("updateDetailExportDefaulUrl", updateDetailExportDefaulUrl);
            setUrlToConfigFile("deleteDetailExportDefaulUrl", deleteDetailExportDefaulUrl);

            //export
            setUrlToConfigFile("listExportDefaulUrl", listExportDefaulUrl);
            setUrlToConfigFile("insertExportDefaulUrl", insertExportDefaulUrl);
            setUrlToConfigFile("updateExportDefaulUrl", updateExportDefaulUrl);
            setUrlToConfigFile("deleteExportDefaulUrl", deleteExportDefaulUrl);

            //detail import
            setUrlToConfigFile("listDetailImportDefaulUrl", listDetailImportDefaulUrl);
            setUrlToConfigFile("insertDetailImportDefaulUrl", insertDetailImportDefaulUrl);
            setUrlToConfigFile("updateDetailImportDefaulUrl", updateDetailImportDefaulUrl);
            setUrlToConfigFile("deleteDetailImportDefaulUrl", deleteDetailImportDefaulUrl);

            //import
            setUrlToConfigFile("listImportDefaulUrl", listImportDefaulUrl);
            setUrlToConfigFile("insertImportDefaulUrl", insertImportDefaulUrl);
            setUrlToConfigFile("updateImportDefaulUrl", updateImportDefaulUrl);
            setUrlToConfigFile("deleteImportDefaulUrl", deleteImportDefaulUrl);

            //palet
            setUrlToConfigFile("listPaletDefaulUrl", listPaletDefaulUrl);
            setUrlToConfigFile("insertPaletDefaulUrl", insertPaletDefaulUrl);
            setUrlToConfigFile("updatePaletDefaulUrl", updatePaletDefaulUrl);
            setUrlToConfigFile("deletePaletDefaulUrl", deletePaletDefaulUrl);

            //user
            setUrlToConfigFile("listUserDefaulUrl", listUserDefaulUrl);
            setUrlToConfigFile("insertUserDefaulUrl", insertUserDefaulUrl);
            setUrlToConfigFile("updateUserDefaulUrl", updateUserDefaulUrl);
            setUrlToConfigFile("deleteUserDefaulUrl", deleteUserDefaulUrl);

            //warehouse
            setUrlToConfigFile("listWHDefaulUrl", listWHDefaulUrl);
            setUrlToConfigFile("insertWHDefaulUrl", insertWHDefaulUrl);
            setUrlToConfigFile("updateWHDefaulUrl", updateWHDefaulUrl);
            setUrlToConfigFile("deleteWHDefaulUrl", deleteWHDefaulUrl);

            //truck
            setUrlToConfigFile("listTruckDefaulUrl", listTruckDefaulUrl);
            setUrlToConfigFile("insertTruckDefaulUrl", insertTruckDefaulUrl);
            setUrlToConfigFile("updateTruckDefaulUrl", updateTruckDefaulUrl);
            setUrlToConfigFile("deleteTruckDefaulUrl", deleteTruckDefaulUrl);
        }

        public static void changeLanguage(string language)
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;
            if (settings["Language"] == null)
            {
                settings.Add("Language", language);
            }
            else
            {
                settings["Language"].Value = language;
            }
            configSettings.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static String getLanguage()
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;
            if (settings["Language"] == null)
            {
                settings.Add("Language", "vi");
            }
            return settings["Language"].Value;
        }
        #endregion

        #region EXPORT REGION
        public static DataTable getListDetailExportLocalByExportId(string exportId)
        {
            DataSet ds = new DataSet();
            DataTable rs = new DataTable();
            if (!File.Exists(@"DetailExportAndPalet.xml"))
            {
                return null;
            }
            XmlReader xmlFile = XmlReader.Create(@"DetailExportAndPalet.xml", new XmlReaderSettings());
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            foreach (DataRow dr in ds.Tables["Item"].Rows)
            {
                if (dr["IdExport"].ToString().Equals(exportId))
                {
                    rs.ImportRow(dr);
                }
            }
            if (rs.Rows.Count == 0) return null;
            return rs;
        }
        private static void syncDetailExportAndPaletTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"DetailExportAndPalet.xml"))
                {
                    File.Delete(@"DetailExportAndPalet.xml");
                }
                writer = new FileStream(@"DetailExportAndPalet.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("IdPalet", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdExport", (string)obj["idexport"]);
                        xmlWriter.WriteElementString("SerialPalet", (string)obj["serialpalet"]);
                        xmlWriter.WriteElementString("LicensePlate", (string)obj["licenseplate"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                        // xmlWriter.WriteElementString("SyncStatus", "1");

                        xmlWriter.WriteElementString("IdDetailExport", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("BeginDate", (string)obj["begindate"]);
                        xmlWriter.WriteElementString("EndDate", (string)obj["enddate"]);
                        xmlWriter.WriteElementString("Status", (string)obj["status"]);
                        //xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        private static void syncExportTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"Export.xml"))
                {
                    File.Delete(@"Export.xml");
                }
                writer = new FileStream(@"Export.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("IdWareHouseEx", (string)obj["idwarehouseex"]);
                        xmlWriter.WriteElementString("Name", (string)obj["name"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("Produce", (string)obj["produce"]);
                        xmlWriter.WriteElementString("Quantity", (string)obj["quantity"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteElementString("IdUser", (string)obj["iduser"]);
                        xmlWriter.WriteElementString("UpdateDate", (string)obj["updatedate"]);
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }
        public static async Task<int> callApiPostUpdateExport(string exportId, string whId, string whExId, string name, string date, string serial, string produce, string quantity, string note, string username, string updateDate)
        {
            string url = getUrlFromConfigFile("updateExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", exportId },
               { "IdWareHouse", whId },
               { "IdWareHouseEx", whExId },
               { "Name", name },
               { "Date", date },
               { "Serial",  serial },
               { "Produce",  produce },
               { "Quantity",  quantity },
               { "Notes",  note },
               { "UserName",  username },
               { "UpdateDate",  updateDate }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteExport(string exportId)
        {
            string url = getUrlFromConfigFile("deleteExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", exportId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewExport(string whId, string whExId, string name, string date, string serial, string produce, string quantity, string note, string username, string updateDate)
        {
            string url = getUrlFromConfigFile("insertExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "IdWareHouse", whId },
               { "IdWareHouseEx", whExId },
               { "Name", name },
               { "Date", date },
               { "Serial",  serial },
               { "Produce",  produce },
               { "Quantity",  quantity },
               { "Notes",  note },
               //{ "SyncStatus",  "1" },
               { "UserName",  username },
               { "UpdateDate",  updateDate }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetExport()
        {
            string url = getUrlFromConfigFile("listExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncExportTable((JArray)jobject["root"]["item"]);
                        syncDetailExportAndPaletTable((JArray)jobject["root"]["item2"]);
                        syncPaletTableForExport((JArray)jobject["root"]["item3"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region IMPORT REGION
        public static DataTable getListDetailImportLocalByImportId(string importId)
        {
            DataSet ds = new DataSet();
            DataTable rs = new DataTable();
            if (!File.Exists(@"DetailImportAndPalet.xml"))
            {
                return null;
            }
            XmlReader xmlFile = XmlReader.Create(@"DetailImportAndPalet.xml", new XmlReaderSettings());
            ds.ReadXml(xmlFile);
            xmlFile.Dispose();
            xmlFile.Close();
            foreach (DataRow dr in ds.Tables["Item"].Rows)
            {
                if (dr["IdImport"].ToString().Equals(importId))
                {
                    rs.ImportRow(dr);
                }
            }
            if (rs.Rows.Count == 0) return null;
            return rs;
        }
        private static void syncDetailImportAndPaletTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"DetailImportAndPalet.xml"))
                {
                    File.Delete(@"DetailImportAndPalet.xml");
                }
                writer = new FileStream(@"DetailImportAndPalet.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("IdPalet", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdImport", (string)obj["idimport"]);
                        xmlWriter.WriteElementString("SerialPalet", (string)obj["serialpalet"]);
                        xmlWriter.WriteElementString("LicensePlate", (string)obj["licenseplate"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                       // xmlWriter.WriteElementString("SyncStatus", "1");

                        xmlWriter.WriteElementString("IdDetailImport", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("BeginDate", (string)obj["begindate"]);
                        xmlWriter.WriteElementString("EndDate", (string)obj["enddate"]);
                        xmlWriter.WriteElementString("Status", (string)obj["status"]);
                        //xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }
        private static void syncImportTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"Import.xml"))
                {
                    File.Delete(@"Import.xml");
                }
                writer = new FileStream(@"Import.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("IdWareHouseEx", (string)obj["idwarehouseex"]);
                        xmlWriter.WriteElementString("Name", (string)obj["name"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("Produce", (string)obj["produce"]);
                        xmlWriter.WriteElementString("Quantity", (string)obj["quantity"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteElementString("UpdateDate", (string)obj["updatedate"]);
                        xmlWriter.WriteElementString("UserName", (string)obj["username"]);
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }
        public static async Task<int> callApiPostUpdateImport(string importId, string whId, string whExId, string name, string date, string serial, string produce, string quantity, string note, string username, string updateDate)
        {
            string url = getUrlFromConfigFile("updateImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", importId },
               { "IdWareHouse", whId },
               { "IdWareHouseEx", whExId },
               { "Name", name },
               { "Date", date },
               { "Serial",  serial },
               { "Produce",  produce },
               { "Quantity",  quantity },
               { "Notes",  note },
               { "UserName",  username },
               { "UpdateDate",  updateDate }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteImport(string importId)
        {
            string url = getUrlFromConfigFile("deleteImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", importId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewImport(string whId, string whExId, string name, string date, string serial, string produce, string quantity, string note, string username, string updateDate)
        {
            string url = getUrlFromConfigFile("insertImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "IdWareHouse", whId },
               { "IdWareHouseEx", whExId },
               { "Name", name },
               { "Date", date },
               { "Serial",  serial },
               { "Produce",  produce },
               { "Quantity",  quantity },
               { "Notes",  note },
               //{ "SyncStatus",  "1" },
               { "UserName",  username },
               { "UpdateDate",  updateDate }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetImport()
        {
            string url = getUrlFromConfigFile("listImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncImportTable((JArray)jobject["root"]["item"]);
                        syncDetailImportAndPaletTable((JArray)jobject["root"]["item2"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region DETAIL EXPORT REGION
        private static void syncDetailExportTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"DetailExport.xml"))
                {
                    File.Delete(@"DetailExport.xml");
                }
                writer = new FileStream(@"DetailExport.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdExport", (string)obj["idexport"]);
                        xmlWriter.WriteElementString("IdPalet", (string)obj["idpalet"]);
                        xmlWriter.WriteElementString("IdTruck", (string)obj["idtruck"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Note", (string)obj["note"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostUpdateDetailExport(string detailExportId, string exportId, string serialPalet, string licensePlate, string date, string note)
        {
            string url = getUrlFromConfigFile("updateDetailExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", detailExportId },
               { "IdExport", exportId },
               { "SerialPalet", serialPalet },
               { "LicensePlate", licensePlate },
               { "Date", date },
               { "Notes",  note }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteDetailExport(string detailExportId)
        {
            string url = getUrlFromConfigFile("deleteDetailExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", detailExportId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewDetailExport(string exportId, string serialPalet, string licensePlate, string date, string note)
        {
            string url = getUrlFromConfigFile("insertDetailExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "IdExport", exportId },
               { "SerialPalet", serialPalet },
               { "LicensePlate", licensePlate },
               { "Date", date },
               { "Notes",  note }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
                idNewDetailExport = (string)objJson["root"]["iddetailexport"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailExportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetDetailExport()
        {
            string url = getUrlFromConfigFile("listDetailExportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncDetailExportTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region DETAIL IMPORT REGION
        private static void syncDetailImportTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"DetailImport.xml"))
                {
                    File.Delete(@"DetailImport.xml");
                }
                writer = new FileStream(@"DetailImport.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdImport", (string)obj["idimport"]);
                        xmlWriter.WriteElementString("SerialPallet", (string)obj["serialpallet"]);
                        xmlWriter.WriteElementString("LicensePlate", (string)obj["licenseplate"]);
                        xmlWriter.WriteElementString("Date", (string)obj["date"]);
                        xmlWriter.WriteElementString("Note", (string)obj["note"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostUpdateDetailImport(string detailImportId, string importId, string serialPalet, string licensePlate, string date, string note)
        {
            string url = getUrlFromConfigFile("updateDetailImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", detailImportId },
               { "IdImport", importId },
               { "SerialPalet", serialPalet },
               { "LicensePlate", licensePlate },
               { "Date", date },
               { "Notes",  note }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteDetailImport(string detailImportId)
        {
            string url = getUrlFromConfigFile("deleteDetailImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", detailImportId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewDetailImport(string importId, string serialPalet, string licensePlate, string date, string note)
        {
            string url = getUrlFromConfigFile("insertDetailImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "IdImport", importId },
               { "SerialPalet", serialPalet },
               { "LicensePlate", licensePlate },
               { "Date", date },
               { "Notes",  note }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
                idNewDetailImport = (string)objJson["root"]["iddetailimport"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncDetailImportTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetDetailImport()
        {
            string url = getUrlFromConfigFile("listDetailImportDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncDetailImportTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region PALET REGION
        private static void syncPaletTableForExport(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"PaletForExport.xml"))
                {
                    File.Delete(@"PaletForExport.xml");
                }
                writer = new FileStream(@"PaletForExport.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("BeginDate", (string)obj["begindate"]);
                        xmlWriter.WriteElementString("EndDate", (string)obj["enddate"]);
                        xmlWriter.WriteElementString("Status", (string)obj["status"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }
        private static void syncPaletTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"Palet.xml"))
                {
                    File.Delete(@"Palet.xml");
                }
                writer = new FileStream(@"Palet.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("Serial", (string)obj["serial"]);
                        xmlWriter.WriteElementString("BeginDate", (string)obj["begindate"]);
                        xmlWriter.WriteElementString("EndDate", (string)obj["enddate"]);
                        xmlWriter.WriteElementString("Status", (string)obj["status"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostUpdatePalet(string paletId, string whId, string serial, string beginDate, string endDate, string status)
        {
            string url = getUrlFromConfigFile("updatePaletDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", paletId },
               { "IdWareHouse", whId },
               { "Serial", serial },
               { "BeginDate", beginDate },
               { "EndDate", endDate },
               { "Status",  status}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncPaletTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeletePalet(string paletId)
        {
            string url = getUrlFromConfigFile("deletePaletDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", paletId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncPaletTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewPalet(string whId, string serial, string beginDate, string endDate, string status)
        {
            string url = getUrlFromConfigFile("insertPaletDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "IdWareHouse", whId },
               { "Serial", serial },
               { "BeginDate", beginDate },
               { "EndDate", endDate },
               { "Status",  status}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
                idNewPalet = (string)objJson["root"]["idpalet"];

            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncPaletTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetPalet()
        {
            string url = getUrlFromConfigFile("listPaletDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncPaletTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region TRUCK REGION
        private static void syncTruckTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"Truck.xml"))
                {
                    File.Delete(@"Truck.xml");
                }
                writer = new FileStream(@"Truck.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("LicensePlate", (string)obj["licenseplate"]);
                        xmlWriter.WriteElementString("IdWareHouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("BeginDate", (string)obj["begindate"]);
                        xmlWriter.WriteElementString("EndDate", (string)obj["enddate"]);
                        xmlWriter.WriteElementString("Status", (string)obj["status"]);
                        xmlWriter.WriteElementString("Owner", (string)obj["owner"]);
                        xmlWriter.WriteElementString("Address", (string)obj["address"]);
                        xmlWriter.WriteElementString("Phone", (string)obj["phone"]);
                        xmlWriter.WriteElementString("IdentityCard", (string)obj["identitycard"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteElementString("Driver", (string)obj["driver"]);
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostUpdateTruck(string truckId, string licensePlate, string idWh, string beginDate, string endDate, string status, string owner, string address, string phone, string identityCard, string note, string driver)
        {
            string url = getUrlFromConfigFile("updateTruckDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", truckId },
               { "LicensePlate", licensePlate },
               { "IdWareHouse", idWh },
               { "BeginDate", beginDate },
               { "EndDate", endDate },
               { "Status",  status},
               { "Owner",  owner},
               { "Address",  address},
               { "Phone",  phone},
               { "IdentityCard",  identityCard},
               { "Notes",  note},
               { "Driver",  driver}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncTruckTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteTruck(string truckId)
        {
            string url = getUrlFromConfigFile("deleteTruckDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", truckId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncTruckTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewTruck(string licensePlate, string idWh, string beginDate, string endDate, string status, string owner, string address, string phone, string identityCard, string note, string driver)
        {
            string url = getUrlFromConfigFile("insertTruckDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "LicensePlate", licensePlate },
               { "IdWareHouse", idWh },
               { "BeginDate", beginDate },
               { "EndDate", endDate },
               { "Status",  status},
               { "Owner",  owner},
               { "Address",  address},
               { "Phone",  phone},
               { "IdentityCard",  identityCard},
               { "Notes",  note},
               { "Driver",  driver}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncTruckTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetTruck()
        {
            string url = getUrlFromConfigFile("listTruckDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncTruckTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region WAREHOUSE REGION
        private static void syncWarehouseTable(JArray array)
        {
            if (array.Count > 1)
            {
                //var all = from c in entityWH.WAREHOUSEs select c;
                //entityWH.WAREHOUSEs.RemoveRange(all);
                //entityWH.SaveChanges();
                //JObject obj;
                //for (var i = 0; i < array.Count; i++)
                //{
                //    try
                //    {
                //        obj = new JObject();
                //        obj = (JObject)array[i];
                //        WAREHOUSE newItem = new WAREHOUSE();
                //        newItem.Id = (string)obj["id"];
                //        newItem.IdBranch = (string)obj["idbranch"];
                //        newItem.Name = (string)obj["name"];
                //        newItem.Address = (string)obj["address"];
                //        newItem.Phone = (string)obj["phone"];
                //        newItem.Note = (string)obj["notes"];
                //        newItem.SyncStatus = 1;
                //        newItem.QuantityPalet = (int)obj["quantitypalet"];
                //        entityWH.WAREHOUSEs.Add(newItem);
                //        entityWH.SaveChanges();
                //    }
                //    catch (DbEntityValidationException e)
                //    {
                //        foreach (var eve in e.EntityValidationErrors)
                //        {
                //            Console.WriteLine("loi loi loi \"{0}\" in state \"{1}\" has the following validation errors:",
                //                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //            foreach (var ve in eve.ValidationErrors)
                //            {
                //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //                    ve.PropertyName, ve.ErrorMessage);
                //            }
                //        }
                //        throw;
                //    }

                //}

                if (File.Exists(@"Warehouse.xml"))
                {
                    File.Delete(@"Warehouse.xml");
                }

                writer = new FileStream(@"Warehouse.xml", FileMode.OpenOrCreate);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("Name", (string)obj["name"]);
                        xmlWriter.WriteElementString("Address", (string)obj["address"]);
                        xmlWriter.WriteElementString("Phone", (string)obj["phone"]);
                        xmlWriter.WriteElementString("Note", (string)obj["notes"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteElementString("QuantityPalet", (string)obj["quantitypalet"]);
                        xmlWriter.WriteElementString("Area", (string)obj["area"]);
                        xmlWriter.WriteElementString("LinkWS", (string)obj["linkws"]);
                        xmlWriter.WriteElementString("IsHeadOffice", (string)obj["isheadoffice"]);

                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();

                }
                writer.Flush();
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostDeleteWarehouse(string whId)
        {
            string url = getUrlFromConfigFile("deleteWHDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", whId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncWarehouseTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewWarehouse(string whName, string address, string phone, string note, string quantityPallet, string area, string linkws, string isHeadOffice)
        {
            string url = getUrlFromConfigFile("insertWHDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Name", whName },
               { "Address", address },
               { "Phone", phone },
               { "Notes", note},
               { "QuantityPalet", quantityPallet},
               { "Area", area},
               { "LinkWS", linkws},
               { "IsHeadOffice",  isHeadOffice}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncWarehouseTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostUpdateWH(string whId, string name, string address, string phone, string note, string quantityPallet, string area, string linkws, string isHeadOffice)
        {
            string url = getUrlFromConfigFile("updateWHDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", whId },
               { "Name", name },
               { "Address", address },
               { "Phone", phone },
               { "Notes", note },
               { "QuantityPalet", quantityPallet},
               { "Area", area},
               { "LinkWS", linkws},
               { "IsHeadOffice",  isHeadOffice}
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncWarehouseTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetWarehouse()
        {
            string url = getUrlFromConfigFile("listWHDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncWarehouseTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }

            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region USER REGION
        private static void syncUserTable(JArray array)
        {
            if (array.Count > 0)
            {
                if (writer != null)
                {
                    writer.Dispose();
                    writer.Close();
                }
                if (File.Exists(@"User.xml"))
                {
                    File.Delete(@"User.xml");
                }
                writer = new FileStream(@"User.xml", FileMode.Create, FileAccess.Write, FileShare.None);
                JObject obj;
                using (XmlWriter xmlWriter = XmlWriter.Create(writer))
                {
                    xmlWriter.WriteStartElement("Table");
                    for (var i = 0; i < array.Count; i++)
                    {
                        obj = new JObject();
                        obj = (JObject)array[i];
                        xmlWriter.WriteStartElement("Item");
                        xmlWriter.WriteElementString("Id", (string)obj["id"]);
                        xmlWriter.WriteElementString("UserName", (string)obj["username"]);
                        xmlWriter.WriteElementString("Password", (string)obj["password"]);
                        xmlWriter.WriteElementString("CreateData", (string)obj["createdate"]);
                        xmlWriter.WriteElementString("UpdateData", (string)obj["updatedate"]);
                        xmlWriter.WriteElementString("Notes", (string)obj["notes"]);
                        xmlWriter.WriteElementString("SyncStatus", "1");
                        xmlWriter.WriteElementString("IdWarehouse", (string)obj["idwarehouse"]);
                        xmlWriter.WriteElementString("IdClient", (string)obj["idclient"]);
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Dispose();
                    xmlWriter.Close();
                }
                writer.Dispose();
                writer.Close();
            }
        }

        public static async Task<int> callApiPostUpdateUser(string userId, string username, string password, string whId, string clientId, string note)
        {
            string url = getUrlFromConfigFile("updateUserDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", userId },
               { "UserName", username },
               { "Password", password },
               { "IdWarehouse", whId },
               { "IdClient", clientId },
               { "Notes",  note},
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncUserTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostDeleteUser(string userId)
        {
            string url = getUrlFromConfigFile("deleteUserDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "Id", userId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncUserTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiPostAddNewUser(string username, string password, string whId, string clientId, string note)
        {
            string url = getUrlFromConfigFile("insertUserDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "UserName", username },
               { "Password", password },
               { "whId", whId },
               { "ClientId", clientId },
               { "Notes",  note},
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                //loi url, post ....
            }

            string resultString = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                int resultNumber = -3;
                if (int.TryParse(resultString, out resultNumber))
                {
                    if (resultNumber == 1) //success case
                    {
                        syncUserTable((JArray)objJson["root"]["item"]);
                        return resultNumber;
                    }
                    else
                    {
                        return resultNumber;
                    }
                }
                else
                {
                    return resultNumber; //result tra ve ko phai number
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                return -2;
            }
        }

        public static async Task<int> callApiGetUser()
        {
            string url = getUrlFromConfigFile("listUserDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1; //url null
            }
            var request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = WebRequestMethods.Http.Get;
            string text;
            var response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                JObject jobject;
                try
                {
                    jobject = JObject.Parse(text);
                }
                catch
                {
                    jobject = null;
                }
                if (jobject == null)
                {
                    return -2;
                }
                string result = (string)jobject["root"]["result"];
                int resultNum = -2;
                if (int.TryParse(result, out resultNum))
                {
                    if (resultNum == 1) //success case
                    {
                        syncUserTable((JArray)jobject["root"]["item"]);
                        return 1;
                    }
                    else
                    {
                        return resultNum; //have json return, still error
                    }
                }
                else
                {
                    return -3; //result not a number
                }
            }
            else
            {
                return -2; //response null
            }
        }
        #endregion

        #region BRANCH REGION
        //private static void syncBranchTable(JArray array)
        //{
        //    if (array.Count > 1)
        //    {
        //        if (writer != null)
        //        {
        //            writer.Dispose();
        //            writer.Close();
        //        }
        //        if (File.Exists(@"Branch.xml"))
        //        {
        //            File.Delete(@"Branch.xml");
        //        }

        //        writer = new FileStream(@"Branch.xml", FileMode.OpenOrCreate);
        //        JObject obj;
        //        using (XmlWriter xmlWriter = XmlWriter.Create(writer))
        //        {
        //            xmlWriter.WriteStartElement("Table");
        //            for (var i = 0; i < array.Count; i++)
        //            {
        //                obj = new JObject();
        //                obj = (JObject)array[i];
        //                xmlWriter.WriteStartElement("Item");
        //                xmlWriter.WriteElementString("Id", (string)obj["id"]);
        //                xmlWriter.WriteElementString("Name", (string)obj["name"]);
        //                xmlWriter.WriteElementString("Address", (string)obj["address"]);
        //                xmlWriter.WriteElementString("Area", (string)obj["area"]);
        //                xmlWriter.WriteElementString("Phone", (string)obj["phone"]);
        //                xmlWriter.WriteElementString("Note", (string)obj["notes"]);
        //                xmlWriter.WriteElementString("SyncStatus", "1");
        //                xmlWriter.WriteElementString("LinkWS", (string)obj["linkws"]);
        //                xmlWriter.WriteElementString("IsHeadOffice", (string)obj["isheadoffice"]);
        //                xmlWriter.WriteEndElement();
        //            }
        //            xmlWriter.WriteEndDocument();
        //            xmlWriter.Dispose();
        //            xmlWriter.Close();

        //        }
        //        writer.Dispose();
        //        writer.Close();
        //    }
        //}

        //public static async Task<int> callApiPostDeleteBranch(string branchId)
        //{
        //    string url = getUrlFromConfigFile("DeleteBranchUrl");
        //    if (string.IsNullOrEmpty(url))
        //    {
        //        return -1;
        //    }
        //    var values = new Dictionary<string, string>
        //    {
        //       { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
        //       { "Id", branchId }
        //    };
        //    var content = new FormUrlEncodedContent(values);
        //    var responseString = "";
        //    try
        //    {
        //        var response = await client.PostAsync(url, content);
        //        responseString = await response.Content.ReadAsStringAsync();
        //    }
        //    catch (Exception e)
        //    {
        //        //loi url, post ....
        //    }

        //    string resultString = "";
        //    JObject objJson = null;
        //    try
        //    {
        //        objJson = JObject.Parse(responseString);
        //        resultString = (string)objJson["root"]["result"];
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    if (!string.IsNullOrEmpty(resultString))
        //    {
        //        int resultNumber = -3;
        //        if (int.TryParse(resultString, out resultNumber))
        //        {
        //            if (resultNumber == 1) //success case
        //            {
        //                syncBranchTable((JArray)objJson["root"]["item"]);
        //                return resultNumber;
        //            }
        //            else
        //            {
        //                return resultNumber;
        //            }
        //        }
        //        else
        //        {
        //            return resultNumber; //result tra ve ko phai number
        //        }
        //    }
        //    else
        //    {
        //        //json ko tra ve
        //        //json tra ve loi
        //        //timeout
        //        //disconnect
        //        //call login local !
        //        return -2;
        //    }
        //}

        //public static async Task<int> callApiPostAddNewBranch(string branchname, string address, string area, string phone, string note, string linkws)
        //{
        //    string url = getUrlFromConfigFile("InsertBranchUrl");
        //    if (string.IsNullOrEmpty(url))
        //    {
        //        return -1;
        //    }
        //    var values = new Dictionary<string, string>
        //    {
        //       { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
        //       { "Name", branchname },
        //       { "Address", address },
        //       { "Area", area },
        //       { "Phone", phone },
        //       { "Notes", note},
        //       { "LinkWS", "1"},
        //       { "IsHeadOffice", "1"}

        //    };
        //    var content = new FormUrlEncodedContent(values);
        //    var responseString = "";
        //    try
        //    {
        //        var response = await client.PostAsync(url, content);
        //        responseString = await response.Content.ReadAsStringAsync();
        //    }
        //    catch (Exception e)
        //    {
        //        //loi url, post ....
        //    }

        //    string resultString = "";
        //    JObject objJson = null;
        //    try
        //    {
        //        objJson = JObject.Parse(responseString);
        //        resultString = (string)objJson["root"]["result"];
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    if (!string.IsNullOrEmpty(resultString))
        //    {
        //        int resultNumber = -3;
        //        if (int.TryParse(resultString, out resultNumber))
        //        {
        //            if (resultNumber == 1) //success case
        //            {
        //                syncBranchTable((JArray)objJson["root"]["item"]);
        //                return resultNumber;
        //            }
        //            else
        //            {
        //                return resultNumber;
        //            }
        //        }
        //        else
        //        {
        //            return resultNumber; //result tra ve ko phai number
        //        }
        //    }
        //    else
        //    {
        //        //json ko tra ve
        //        //json tra ve loi
        //        //timeout
        //        //disconnect
        //        //call login local !
        //        return -2;
        //    }
        //}

        //public static async Task<int> callApiPostUpdateBranch(string branchId, string branchname, string address, string area, string phone, string note, string linkws)
        //{
        //    string url = getUrlFromConfigFile("UpdateBranchUrl");
        //    if (string.IsNullOrEmpty(url))
        //    {
        //        return -1;
        //    }
        //    var values = new Dictionary<string, string>
        //    {
        //       { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
        //       { "Id", branchId },
        //       { "Name", branchname },
        //       { "Address", address },
        //       { "Area", area },
        //       { "Phone", phone },
        //       { "Notes", note },
        //       { "LinkWS", linkws},
        //       { "IsHeadOffice", "1"}
        //    };
        //    var content = new FormUrlEncodedContent(values);
        //    var responseString = "";
        //    try
        //    {
        //        var response = await client.PostAsync(url, content);
        //        responseString = await response.Content.ReadAsStringAsync();
        //    }
        //    catch (Exception e)
        //    {
        //        //loi url, post ....
        //    }

        //    string resultString = "";
        //    JObject objJson = null;
        //    try
        //    {
        //        objJson = JObject.Parse(responseString);
        //        resultString = (string)objJson["root"]["result"];
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    if (!string.IsNullOrEmpty(resultString))
        //    {
        //        int resultNumber = -3;
        //        if (int.TryParse(resultString, out resultNumber))
        //        {
        //            if (resultNumber == 1) //success case
        //            {
        //                syncBranchTable((JArray)objJson["root"]["item"]);
        //                return resultNumber;
        //            }
        //            else
        //            {
        //                return resultNumber;
        //            }
        //        }
        //        else
        //        {
        //            return resultNumber; //result tra ve ko phai number
        //        }
        //    }
        //    else
        //    {
        //        //json ko tra ve
        //        //json tra ve loi
        //        //timeout
        //        //disconnect
        //        //call login local !
        //        return -2;
        //    }
        //}

        //public static async Task<int> callApiGetBranch()
        //{
        //    string url = getUrlFromConfigFile("ListBranchUrl");
        //    if (string.IsNullOrEmpty(url))
        //    {
        //        return -1; //url null
        //    }
        //    var request = WebRequest.Create(url);
        //    request.ContentType = "application/json; charset=utf-8";
        //    request.Method = WebRequestMethods.Http.Get;
        //    string text;
        //    var response = (HttpWebResponse)request.GetResponse();
        //    if (response != null)
        //    {
        //        using (var sr = new StreamReader(response.GetResponseStream()))
        //        {
        //            text = sr.ReadToEnd();
        //        }
        //        JObject jobject;
        //        try
        //        {
        //            jobject = JObject.Parse(text);
        //        }
        //        catch
        //        {
        //            jobject = null;
        //        }
        //        if (jobject == null)
        //        {
        //            return -2;
        //        }
        //        string result = (string)jobject["root"]["result"];
        //        int resultNum = -2;
        //        if (int.TryParse(result, out resultNum))
        //        {
        //            if (resultNum == 1) //success case
        //            {
        //                syncBranchTable((JArray)jobject["root"]["item"]);
        //                return 1;
        //            }
        //            else
        //            {
        //                return resultNum; //have json return, still error
        //            }
        //        }
        //        else
        //        {
        //            return -3; //result not a number
        //        }

        //    }
        //    else
        //    {
        //        return -2; //response null
        //    }
        //}
        #endregion

        #region LOGIN REGION
        public static async Task<int> callApiPostLogin(string username, string password, string whId, string clientId)
        {
            IS_HEAD_OFFICE = false;
            //CURRENT_ID_USER = "";
            CURRENT_USERNAME = "";
            CURRENT_ID_WAREHOUSE = "";
            string url = getUrlFromConfigFile("loginDefaulUrl");
            if (string.IsNullOrEmpty(url))
            {
                return -1;
            }
            var values = new Dictionary<string, string>
            {
               { "contentType", "application/x-www-form-urlencoded;charset=utf-8" },
               { "UserName", username },
               { "Password", password },
               { "IdWareHouse", whId },
               { "IdClient", clientId }
            };
            var content = new FormUrlEncodedContent(values);
            var responseString = "";
            try
            {
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message.ToString());
            }

            string resultString = "";
            string isHO = "";
            string linkWSSuccess = "";
            JObject objJson = null;
            try
            {
                objJson = JObject.Parse(responseString);
                resultString = (string)objJson["root"]["result"];
                isHO = (string)objJson["root"]["isheadoffice"];
                linkWSSuccess = (string)objJson["root"]["linkws"];
            }
            catch (Exception e)
            {

            }
            if (!string.IsNullOrEmpty(resultString))
            {
                if (resultString.Equals("1"))
                { //login success 
                    saveLastLoginSuccess(whId, clientId, username, linkWSSuccess); //save config for next login
                    IS_HEAD_OFFICE = isHO == "1" ? true : false;
                    CURRENT_ID_WAREHOUSE = (string)objJson["root"]["idwarehouse"];
                    CURRENT_USERNAME = (string)objJson["root"]["username"];

                    changeAllApiLink(linkWSSuccess);

                    //sync data
                    syncUserTable((JArray)objJson["root"]["item"]);
                    syncWarehouseTable((JArray)objJson["root"]["item2"]);

                    return int.Parse(resultString);
                }
                else
                {
                    //json co tra ve, khac 1
                    return int.Parse(resultString);
                }
            }
            else
            {
                //json ko tra ve
                //json tra ve loi
                //timeout
                //disconnect
                //call login local !
                //getLastLoginSuccess
                //var userCurrent = entityWH.USERs.FirstOrDefault(u => u.UserName.Equals(username) && u.Password.Equals(password) 
                //                                                                                 && u.IdBranch.Equals(branchId) 
                //                                                                                 && u.IdClient.Equals(clientId));
                //if (userCurrent == null)
                //{
                //    return 2;
                //}
                //else
                //{
                //    saveLastLoginSuccess(branchId, clientId, username);
                //    //login local success
                //    //save config for next login
                //    return 1;
                //}
                return 2;
            }
        }

        private static void saveLastLoginSuccess(string whId, string clientId, string username, string linkWSSuccess)
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;
            if (settings["WHIdLastLogin"] == null)
            {
                settings.Add("WHIdLastLogin", whId);
            }
            else
            {
                settings.Remove("WHIdLastLogin");
                settings.Add("WHIdLastLogin", whId);
            }
            if (settings["ClientIdLastLogin"] == null)
            {
                settings.Add("ClientIdLastLogin", clientId);
            }
            else
            {
                settings.Remove("ClientIdLastLogin");
                settings.Add("ClientIdLastLogin", clientId);
            }
            if (settings["UsernameLastLogin"] == null)
            {
                settings.Add("UsernameLastLogin", username);
            }
            else
            {
                settings.Remove("UsernameLastLogin");
                settings.Add("UsernameLastLogin", username);
            }
            if (settings["LinkWSLastLogin"] == null)
            {
                settings.Add("LinkWSLastLogin", linkWSSuccess);
            }
            else
            {
                if (!string.IsNullOrEmpty(linkWSSuccess))
                {
                    settings.Remove("LinkWSLastLogin");
                    settings.Add("LinkWSLastLogin", linkWSSuccess);
                }
            }
            configSettings.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static void getLastLoginSuccess(out string branchId, out string clientId, out string username)
        {
            var configSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configSettings.AppSettings.Settings;
            branchId = "";
            clientId = "";
            username = "";
            string linkWs = "";
            if (settings["WHIdLastLogin"] != null)
            {
                branchId = settings["WHIdLastLogin"].Value;
            }

            if (settings["ClientIdLastLogin"] != null)
            {
                clientId = settings["ClientIdLastLogin"].Value;
            }

            if (settings["UsernameLastLogin"] != null)
            {
                username = settings["UsernameLastLogin"].Value;
            }

            if (settings["LinkWSLastLogin"] != null)
            {
                linkWs = settings["LinkWSLastLogin"].Value;
            }
            changeAllApiLink(linkWs);

        }
        #endregion
    }
}
